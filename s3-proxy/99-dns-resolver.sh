#!/bin/sh

# shellcheck disable=SC2312  # this is as ugly as it looks
echo "resolver $(grep nameserver /etc/resolv.conf | cut -d' ' -f2 | xargs);" > /etc/nginx/conf.d/resolver.conf
