"""UMB message sender."""

from cki_lib import misc
import sentry_sdk

from . import main

misc.sentry_init(sentry_sdk)
main()
