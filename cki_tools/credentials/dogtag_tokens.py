"""Dogtag certificate management."""

import typing
from urllib import parse

from cki_lib import misc
from cki_lib.session import get_session
from cryptography import x509
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.x509.oid import NameOID

from . import secrets
from . import token

SESSION = get_session(__name__, headers={'Accept': 'application/json'}, raise_for_status=True)


@token.register_token('dogtag_certificate')
class DogtagCertificate(token.Token):
    """Dogtag certificates."""

    clean_active_versions = 1

    def _create_token(self, token_version: str, meta: dict[str, typing.Any]) -> dict[str, str]:
        """Create Dogtag certificates."""
        uid = next(p.split('=', 1)[1] for p in meta['SubjectDN'].split(',')
                   if p.lower().startswith('uid='))
        password = secrets.secret(next(
            k for k, v in secrets.read_secrets_file().items()
            if misc.get_nested_key(v, 'meta/token_type') == 'ldap_password'
            and misc.get_nested_key(v, 'meta/uid') == uid))
        private_key = rsa.generate_private_key(65537, 4096)
        csr = x509.CertificateSigningRequestBuilder().subject_name(x509.Name([
            x509.NameAttribute(NameOID.USER_ID, uid),
        ])).sign(private_key, hashes.SHA256()).public_bytes(
            serialization.Encoding.PEM,
        ).decode('ascii')
        certrequest_post_url = parse.urljoin(meta['server_url'], 'ca/rest/certrequests')
        certrequest_url = SESSION.post(certrequest_post_url, json={
            'ProfileID': 'caDirAppUserCert',
            'Input': [{'Attribute': [
                {'name': 'cert_request_type', 'Value': 'pkcs10'},
                {'name': 'cert_request', 'Value': csr},
            ]}],
            'Attributes': {'Attribute': [
                {'name': 'uid', 'value': uid},
                {'name': 'pwd', 'value': password},
            ]},
        }).json()['entries'][0]['requestURL']

        meta['id'] = SESSION.get(certrequest_url).json()['certId']
        self._update_token(token_version, meta)

        return {
            'private_key': private_key.private_bytes(
                serialization.Encoding.PEM,
                serialization.PrivateFormat.PKCS8,
                serialization.NoEncryption(),
            ).decode('ascii').strip(),
            'certificate': SESSION.get(
                parse.urljoin(meta['server_url'], f'ca/rest/certs/{meta["id"]}'),
            ).json()['Encoded'].strip(),
        }

    def _destroy_token(self, token_version: str, meta: dict[str, typing.Any]) -> None:
        """Destroy a token version."""
        # there is no API call to revoke a Dogtag certificate

    def _update_token(self, token_version: str, meta: dict[str, typing.Any]) -> None:
        """Update a token version."""
        cert = SESSION.get(parse.urljoin(meta['server_url'], f'ca/rest/certs/{meta["id"]}')).json()
        meta.update({
            'id': cert['id'],
            'created_at': misc.datetime_fromisoformat_tz_utc(cert['NotBefore']).isoformat(),
            'expires_at': misc.datetime_fromisoformat_tz_utc(cert['NotAfter']).isoformat(),
            'active': cert['Status'] == 'VALID',
            'IssuerDN': cert['IssuerDN'],
            'SubjectDN': cert['SubjectDN'],
        })

    def _validate_token(self, token_version: str, meta: dict[str, typing.Any]) -> None:
        """Validate a token version."""
        token_secret = secrets.secret(f'{self.full_token_name(token_version)}:')
        private_key = serialization.load_pem_private_key(
            token_secret['private_key'].encode('ascii'), None)
        cert = x509.load_pem_x509_certificate(token_secret['certificate'].encode('ascii'))
        if private_key.public_key().public_bytes(
            serialization.Encoding.PEM,
            serialization.PublicFormat.SubjectPublicKeyInfo,
        ) != cert.public_key().public_bytes(
            serialization.Encoding.PEM,
            serialization.PublicFormat.SubjectPublicKeyInfo,
        ):
            raise ValueError('Certificate does not match private key')
