"""AWS credential management."""

import functools
import typing
from typing import cast

import boto3
from cki_lib import logger
from cki_lib import misc

from . import secrets
from . import token

LOGGER = logger.get_logger(__name__)

BOTO_SESSION = boto3.Session()


@functools.cache
def _current_account() -> str:
    return cast(str, BOTO_SESSION.client('sts').get_caller_identity()['Account'])


@functools.cache
def _iam() -> boto3.client:
    return BOTO_SESSION.client('iam')


@token.register_token('aws_secret_access_key')
class AwsSecretAccessKey(token.Token):
    """AWS secret access key."""

    clean_active_versions = 1

    def _create_token(self, token_version: str, meta: dict[str, typing.Any]) -> str:
        """Create an AWS key."""
        user_name = meta['user_name']
        access_key = _iam().create_access_key(UserName=user_name)['AccessKey']
        meta.update({
            'access_key_id': access_key['AccessKeyId'],
            'arn': f'arn:aws:iam::{meta["account"]}:user/{user_name}',
            'created_at': misc.ensure_tz_utc(access_key['CreateDate']).isoformat(),
        })
        return cast(str, access_key['SecretAccessKey'])

    def _destroy_token(self, token_version: str, meta: dict[str, typing.Any]) -> None:
        """Destroy an AWS key."""
        _iam().delete_access_key(UserName=meta['user_name'], AccessKeyId=meta['access_key_id'])

    def _update_token(self, token_version: str, meta: dict[str, typing.Any]) -> None:
        """Update the secret meta information about AWS tokens."""
        token_name = self.full_token_name(token_version)

        if endpoint_url := meta.get('endpoint_url'):
            LOGGER.debug('Token meta update for %s not supported', endpoint_url)
            return
        if not meta.get('account') or not meta.get('arn'):
            response = BOTO_SESSION.client(
                'sts',
                aws_access_key_id=meta['access_key_id'],
                aws_secret_access_key=secrets.secret(f'{token_name}'),
            ).get_caller_identity()
            meta.update({
                'account': response['Account'],
                'arn': response['Arn'],
            })
        if meta['account'] != _current_account():
            LOGGER.debug('Token account %s != %s', meta['account'], _current_account())
            return
        if not meta.get('user_name'):
            response = _iam().get_access_key_last_used(AccessKeyId=meta['access_key_id'])
            meta['user_name'] = response['UserName']
        response = next(
            r for r in _iam().list_access_keys(UserName=meta['user_name'])['AccessKeyMetadata']
            if r['AccessKeyId'] == meta['access_key_id']
        )
        meta.update({
            'active': response['Status'] == 'Active',
            'created_at': misc.ensure_tz_utc(response['CreateDate']).isoformat(),
        })
