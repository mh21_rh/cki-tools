"""LDAP credential management."""

import typing

from . import ldap_helpers
from . import secrets
from . import token


@token.register_token('ldap_keytab')
class LdapKeytab(token.Token):
    """LDAP keytab."""

    def _update_token(self, token_version: str, meta: dict[str, typing.Any]) -> None:
        """Update a token version."""
        meta.update(ldap_helpers.ldap_search(f'ldap://{meta["ldap_server"]}', meta['dn']))


@token.register_token('ldap_password')
class LdapPassword(token.Token):
    """LDAP password."""

    def _update_token(self, token_version: str, meta: dict[str, typing.Any]) -> None:
        """Update a token version."""
        meta.update(ldap_helpers.ldap_search(f'ldap://{meta["ldap_server"]}', meta['dn']))

    def _validate_token(self, token_version: str, meta: dict[str, typing.Any]) -> None:
        """Validate a token version."""
        token_secret = secrets.secret(self.full_token_name(token_version))
        ldap_helpers.ldap_login(f'ldap://{meta["ldap_server"]}', meta['dn'], token_secret)
