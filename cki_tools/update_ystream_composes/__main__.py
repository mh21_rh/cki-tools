"""Manage y-stream composes."""
import sys

from cki_lib import misc
import sentry_sdk

from . import main

misc.sentry_init(sentry_sdk)
sys.exit(main())
