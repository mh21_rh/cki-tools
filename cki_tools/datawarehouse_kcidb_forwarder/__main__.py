"""Forward KCIDB data to KernelCI."""
from cki_lib import misc
import sentry_sdk

from . import main

misc.sentry_init(sentry_sdk)
main()
