"""Tests for HV secrets processing."""

import collections
import contextlib
import os
import tempfile
import typing
from unittest import mock

from cki_lib.inttests import cluster
from cki_lib.inttests.vault import HashiCorpVaultServer

from cki_tools.credentials import secrets


@cluster.skip_without_requirements()
class TestHashiCorpVaultSecrets(HashiCorpVaultServer):
    """Tests for HV secrets processing."""

    @staticmethod
    @contextlib.contextmanager
    def _setup_secrets(data: dict[str, typing.Any]) -> collections.abc.Iterator[None]:
        with (tempfile.TemporaryDirectory() as directory,
              mock.patch.dict(os.environ, {'CKI_SECRETS_FILE': f'{directory}/secrets.yml'})):
            secrets.write_secrets_file(data)
            yield

    def test_smoke(self) -> None:
        """Basic smoke tests."""
        with self._setup_secrets({'foo': {'backend': 'hv'}}):
            secrets.edit('foo', 'bar')
            self.assertEqual(secrets.secret('foo'), 'bar')
            self.assertEqual(secrets.vault_list(), ['foo'])
