"""Tests for autoscaler image."""
from collections import abc
import contextlib
import json
import os

from cki_lib import messagequeue
from cki_lib import misc
from cki_lib.inttests import cluster
from cki_lib.inttests.rabbitmq import RabbitMQServer
from cki_lib.logger import get_logger
from kubernetes import watch

LOGGER = get_logger(__name__)


@cluster.skip_without_requirements()
class TestAutoscaler(RabbitMQServer):
    """Tests for autoscaler image."""

    @classmethod
    def setUpClass(cls) -> None:
        """Set up the service."""
        super().setUpClass()
        cls.enterClassContext(cls.k8s_namespace('image-under-test'))
        cls.enterClassContext(cls._service())

    def test_smoke(self) -> None:
        """Basic smoke tests."""
        queue = messagequeue.MessageQueue()
        for _ in range(16):
            queue.send_message({'pooh': 'bear'}, 'cki.queue.webhooks.workload')

        for event in self.dynamic_client.resources.get(kind='Deployment').watch(
                timeout=10, watcher=watch.Watch(), namespace='image-under-test'):
            if event['object'].metadata.name == 'workload' and event['object'].spec.replicas == 4:
                break
        else:
            self.fail('workload not scaled correctly')

    @classmethod
    @contextlib.contextmanager
    def _service(cls) -> abc.Iterator[None]:
        LOGGER.info('Starting image-under-test')
        service_id = 'image-under-test'
        now = misc.now_tz_utc()

        cls.k8s_deployment(namespace=service_id, name='workload', setup_at=now, container={
            'name': 'default',
            'image': 'quay.io/cki/base',
            'command': ['sleep', 'infinity'],
        })
        if not cls.k8s_wait(namespace=service_id, name='workload', setup_at=now):
            raise Exception('dummy workload did not start up')

        with messagequeue.MessageQueue().connect() as channel:
            channel.queue_declare('cki.queue.webhooks.workload')

        cls.k8s_apply(namespace=service_id, body={
            'apiVersion': 'rbac.authorization.k8s.io/v1', 'kind': 'Role',
            'metadata': {'name': service_id},
            'rules': [
                {'apiGroups': ['apps'], 'resources': ['deployments'], 'verbs': ['get']},
                {'apiGroups': ['apps'], 'resources': ['deployments/scale'], 'verbs': ['patch']},
            ],
        })
        cls.k8s_apply(namespace=service_id, body={
            'apiVersion': 'rbac.authorization.k8s.io/v1', 'kind': 'RoleBinding',
            'metadata': {'name': service_id},
            'roleRef': {'kind': 'Role', 'name': service_id},
            'subjects': [{'kind': 'ServiceAccount', 'name': service_id}],
        })

        cls.k8s_deployment(namespace=service_id, name=service_id, setup_at=now, container={
            'name': 'default',
            'image': 'quay.io/cki/autoscaler',
            'startupProbe': cls.k8s_startup_probe(8765),
            'env': [
                {'name': 'AUTOSCALER_CONFIG', 'value': json.dumps({
                    '.schema': 'cki_tools.autoscaler/schema.yml',
                    'workload': {
                        'queue_prefix': 'cki.queue.webhooks.',
                        'messages_per_replica': 5,
                        'max_scale_up_step': 1,
                        'max_scale_down_step': 1,
                        'replicas_min': 1,
                        'replicas_max': 5,
                        'message_baseline': 0,
                    }})},
                {'name': 'REFRESH_PERIOD', 'value': '1'},
                {'name': 'RABBITMQ_HOST', 'value': 'rabbitmq.rabbitmq.svc.cluster.local'},
                {'name': 'RABBITMQ_PORT', 'value': '5672'},
                {'name': 'RABBITMQ_VIRTUAL_HOST', 'value': os.environ['RABBITMQ_VIRTUAL_HOST']},
                {'name': 'RABBITMQ_USER', 'value': os.environ['RABBITMQ_USER']},
                {'name': 'RABBITMQ_PASSWORD', 'value': os.environ['RABBITMQ_PASSWORD']},
                {'name': 'CKI_DEPLOYMENT_ENVIRONMENT', 'value': 'production'},
            ],
        }, image_under_test=True)
        cls.k8s_service(namespace=service_id, name=service_id)
        if not cls.k8s_wait(namespace=service_id, name=service_id, setup_at=now):
            raise Exception(f'{service_id} did not start up')

        yield
