"""Tests for message-trigger image."""
from collections import abc
import contextlib
import json
import os
import time
from urllib import parse

from cki_lib import messagequeue
from cki_lib import misc
from cki_lib.inttests import cluster
from cki_lib.inttests.rabbitmq import RabbitMQServer
from cki_lib.inttests.remote_responses import RemoteResponsesServer
from cki_lib.logger import get_logger

LOGGER = get_logger(__name__)


@cluster.skip_without_requirements()
class TestMessageTrigger(RabbitMQServer, RemoteResponsesServer):
    """Tests for message-trigger image."""

    url: str

    @classmethod
    def setUpClass(cls) -> None:
        """Set up the service."""
        super().setUpClass()
        cls.url = os.environ['REMOTE_RESPONSES_URL']
        cls.enterClassContext(cls.k8s_namespace('image-under-test'))
        cls.enterClassContext(cls._service())

    def test_smoke(self) -> None:
        """Basic smoke tests."""
        cases = (
            ('not matching', {'foo': 'bar'}, None),
            ('matching', {'a': {'path': 'value'}}, {'ref': 'main', 'variables': [
                {'key': 'var1', 'value': 'value'},
            ]}),
        )

        queue = messagequeue.MessageQueue()
        for description, data, expected in cases:
            with self.subTest(description):
                self.responses.reset()
                self.responses.add('GET', f'{self.url}/api/v4/projects/project',
                                   json={'id': 1, 'web_url': f'{self.url}/project'})
                post = self.responses.add('POST', f'{self.url}/api/v4/projects/1/pipeline', json={
                    'web_url': f'{self.url}/project/-/pipelines/2',
                })

                queue.send_message(data, '#', exchange='cki.exchange.webhooks')
                time.sleep(1)

                if not expected:
                    self.assertEqual(post.call_count, 0)
                else:
                    self.assertEqual(post.call_count, 1)
                    self.assertEqual(post.calls[0].request.headers.get(
                        'PRIVATE-TOKEN'), 'secret-token')
                    self.assertEqual(json.loads(post.calls[0].request.body), expected)

    def test_retry(self) -> None:
        """Test retry behavior."""
        data = {'a': {'path': 'value'}}

        self.responses.add('GET', f'{self.url}/api/v4/projects/project',
                           json={'id': 1, 'web_url': f'{self.url}/project'})
        self.responses.add('POST', f'{self.url}/api/v4/projects/1/pipeline', status=500)
        self.responses.add('POST', f'{self.url}/api/v4/projects/1/pipeline', status=500)
        post = self.responses.add('POST', f'{self.url}/api/v4/projects/1/pipeline', json={
            'web_url': f'{self.url}/project/-/pipelines/2',
        })

        messagequeue.MessageQueue().send_message(data, '#', exchange='cki.exchange.webhooks')
        time.sleep(3)

        self.assertEqual(post.call_count, 1)

    @classmethod
    @contextlib.contextmanager
    def _service(cls) -> abc.Iterator[None]:
        LOGGER.info('Starting image-under-test')
        service_id = 'image-under-test'
        now = misc.now_tz_utc()
        cls.k8s_deployment(namespace=service_id, name=service_id, setup_at=now, container={
            'name': 'default',
            'image': 'quay.io/cki/message-trigger',
            'startupProbe': cls.k8s_startup_probe(8765),
            'env': [
                {'name': 'MESSAGE_TRIGGER_CONFIG', 'value': json.dumps({
                    '.schema': 'cki_tools.message_trigger/schema.yml',
                    '.default': {'project_url': f'{cls.url}/project'},
                    'one': {
                        'conditions': {'a': {'jmespath': 'body.a.path', 'regex': '(?P<group>.+)'}},
                        'variables': {'var1': '{group}'},
                    },
                })},
                {'name': 'GITLAB_TOKENS', 'value': json.dumps({
                    parse.urlsplit(cls.url).netloc: 'GITLAB_TOKEN',
                })},
                {'name': 'GITLAB_TOKEN', 'value': 'secret-token'},
                {'name': 'RABBITMQ_HOST', 'value': 'rabbitmq.rabbitmq.svc.cluster.local'},
                {'name': 'RABBITMQ_PORT', 'value': '5672'},
                {'name': 'RABBITMQ_VIRTUAL_HOST', 'value': os.environ['RABBITMQ_VIRTUAL_HOST']},
                {'name': 'RABBITMQ_USER', 'value': os.environ['RABBITMQ_USER']},
                {'name': 'RABBITMQ_PASSWORD', 'value': os.environ['RABBITMQ_PASSWORD']},
                {'name': 'CKI_DEPLOYMENT_ENVIRONMENT', 'value': 'production'},
                {'name': 'MESSAGE_TRIGGER_ROUTING_KEYS', 'value': '#'},
                {'name': 'MESSAGE_TRIGGER_QUEUE', 'value': 'cki.queue.webhooks.message-trigger'},
            ],
        }, image_under_test=True)
        cls.k8s_service(namespace=service_id, name=service_id)
        if not cls.k8s_wait(namespace=service_id, name=service_id, setup_at=now):
            raise Exception(f'{service_id} did not start up')

        yield
