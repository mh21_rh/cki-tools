---
include:
  - {project: cki-project/cki-lib, ref: production, file: .gitlab/ci_templates/cki-common.yml}
  - {project: cki-project/cki-lib, ref: production, file: .gitlab/ci_templates/cki-sast.yml}

shellcheck:
  extends: .shellcheck

yamllint:
  extends: .yamllint

linting:
  extends: .cki_tox_lint

tests:
  extends: .cki_tox_test

inttests:
  extends: .inttests

tests-pipeline:
  extends: .tox
  variables:
    TOX_ARGS: -epipeline
  before_script:
    - curl --config "${CKI_CURL_CONFIG_FILE}"
      https://gitlab.com/cki-project/pipeline-definition/-/raw/production/constraints.txt
      > constraints.txt
  coverage: null  # disable

webhook-receiver-lambda:
  extends: .cki_tools
  image:
    name: public.ecr.aws/lambda/python:3.11
    entrypoint: ['']
  before_script:
    - yum -y install git-core unzip zip
  script:
    - cki/cki_tools/webhook_receiver/build_lambda.sh
  artifacts:
    paths:
      - webhook_receiver_lambda.zip

.deploy_webhook_receiver:
  image: !reference [.cki_tools, image]
  variables:
    IMAGE_NAME: webhook-receiver
    CHANGES: cki/cki_tools/webhook_receiver
    SUPPORTS_STAGING: 'true'
  script:
    - aws s3 cp webhook_receiver_lambda.zip "s3://${S3_BUCKET:?}/${S3_PATH:?}${DEPLOY_TAG:?}/"
  dependencies:
    - webhook-receiver-lambda

.images:
  parallel:
    matrix:
      - IMAGE_NAME: amqp-bridge
        CHANGES: cki/cki_tools/amqp_bridge
        SUPPORTS_STAGING: 'true'
        SMOKE_TEST_COMMANDS: python3 -m cki.cki_tools.amqp_bridge --help
      - IMAGE_NAME: autoscaler
        CHANGES: cki_tools/autoscaler
        SUPPORTS_STAGING: 'true'
        SMOKE_TEST_COMMANDS: python3 -m cki_tools.autoscaler --help
      - IMAGE_NAME: koji-trigger
        CHANGES: cki_tools/koji_trigger
        SMOKE_TEST_COMMANDS: python3 -m cki_tools.koji_trigger --help
      - IMAGE_NAME: cki-tools
        TAG_BRANCH: 'true'
        DEPLOY_GIT_TAG: v1.0.0
        IMAGE_ARCHES: "amd64 arm64"
        SMOKE_TEST_COMMANDS_VARIABLE: SMOKE_TEST_COMMANDS_CKI_TOOLS
      - IMAGE_NAME: datawarehouse-kcidb-forwarder
        CHANGES: cki/kcidb
        SMOKE_TEST_COMMANDS: python3 -m cki_tools.datawarehouse_kcidb_forwarder --help
      - IMAGE_NAME: datawarehouse-triager
        CHANGES: cki/triager
        SMOKE_TEST_COMMANDS: python3 -m cki.triager --help
      - IMAGE_NAME: datawarehouse-submitter
        CHANGES: cki_tools/datawarehouse_submitter
        SUPPORTS_STAGING: 'true'
        SMOKE_TEST_COMMANDS: python3 -m cki_tools.datawarehouse_submitter --help
      - IMAGE_NAME: gating-reporter
        CHANGES: cki_tools/gating_reporter
        SMOKE_TEST_COMMANDS: python3 -m cki_tools.gating_reporter --help
      - IMAGE_NAME: message-trigger
        CHANGES: cki_tools/message_trigger
        SMOKE_TEST_COMMANDS: python3 -m cki_tools.message_trigger.main --help
        SUPPORTS_STAGING: 'true'
      - IMAGE_NAME: monitoring-event-exporter
        CHANGES: cki_tools/monitoring_event_exporter
        SMOKE_TEST_COMMANDS: python3 -m cki_tools.monitoring_event_exporter --help
      - IMAGE_NAME: pipeline-herder
        CHANGES: cki_tools/pipeline_herder
        SUPPORTS_STAGING: 'true'
        SMOKE_TEST_COMMANDS: python3 -m cki_tools.pipeline_herder.main --help
      - IMAGE_NAME: s3-proxy
        CHANGES: s3-proxy
      - IMAGE_NAME: service-metrics
        CHANGES: cki/cki_tools/service_metrics
        SMOKE_TEST_COMMANDS: python3 -m cki.cki_tools.service_metrics --help
      - IMAGE_NAME: slack-bot
        CHANGES: cki_tools/slack_bot
        SMOKE_TEST_COMMANDS: |
          python3 -m cki_tools.slack_bot.webhook
          python3 -m cki_tools.slack_bot.amqp --help
      - IMAGE_NAME: umb-messenger
        CHANGES: cki_tools/umb_messenger
        SMOKE_TEST_COMMANDS: python3 -m cki_tools.umb_messenger --help
      - IMAGE_NAME: webhook-receiver
        CHANGES: cki/cki_tools/webhook_receiver
        SMOKE_TEST_COMMANDS: python3 -m cki.cki_tools.webhook_receiver.flask

build:
  extends: [.build, .images]
  variables:
    # https://gitlab.com/gitlab-org/gitlab/-/issues/362262 🤦
    SMOKE_TEST_COMMANDS_CKI_TOOLS: |
      cki_aws_login --help
      cki_edit_secret --help
      cki_gitlab_yaml_shellcheck --help
      cki_secret --help
      cki_secrets_login --help
      cki_secrets_logout --help
      cki_secrets_validate --help
      cki_variable --help
      python3 -m cki.beaker_tools.broken_machines --help
      python3 -m cki.cki_tools.datawarehouse.issue_maintenance --help
      python3 -m cki.cki_tools.get_kernel_headers --help
      python3 -m cki.cki_tools.gitlab_ci_bot --help
      python3 -m cki.cki_tools.gitlab_sso_login --help
      python3 -m cki.cki_tools.orphan_hunter_ec2 --help
      python3 -m cki.cki_tools.orphan_hunter --help
      python3 -m cki.cki_tools.repo_manager --help
      python3 -m cki.cki_tools.retrigger --help
      python3 -m cki.cki_tools.select_kpet_tree --help
      python3 -m cki.cki_tools.sync_files --help
      python3 -m cki.deployment_tools.gitlab_codeowners_config --help
      python3 -m cki.deployment_tools.gitlab_runner_config --help
      python3 -m cki.deployment_tools.render --help
      python3 -m cki.kcidb.adjust_dumpfiles --help
      python3 -m cki.kcidb.beaker_to_kcidb --help
      python3 -m cki.kcidb.edit --help
      python3 -m cki.kcidb.get_test_summary --help
      python3 -m cki.kcidb.parser --help
      python3 -m cki_tools.credentials.manager --help
      python3 -m cki_tools.credentials.metrics --help
      python3 -m cki_tools.credentials.secrets --help
      python3 -m cki_tools.gitlab_repo_config --help
      python3 -m cki_tools.gitlab_yaml_shellcheck --help
      python3 -m cki_tools.gitrepo_trigger --help
      python3 -m cki_tools.grafana --help
      python3 -m cki_tools.monitoring_event_exporter --help
      python3 -m cki_tools.pipeline_data_gen_tree_doc --help
      python3 -m cki_tools.sync_polarion_id --help
      python3 -m cki_tools.tmt_generate_test_plans_doc --help
      python3 -m cki_tools.update_ystream_composes --help

prod:
  extends: [.deploy_production, .images]

stage:
  extends: [.deploy_staging, .images]

prod-webhook-receiver:
  extends: [.deploy_production, .deploy_webhook_receiver]

stage-webhook-receiver:
  extends: [.deploy_staging, .deploy_webhook_receiver]

documentation-mr:
  extends: .trigger_dependency_mr
  trigger: {project: cki-project/documentation, branch: production}
  variables:
    cki_tools_mr: $CI_MERGE_REQUEST_IID
  rules:
    - if: $CI_MERGE_REQUEST_ID
      changes:
        - "**/*.md"

code_navigation:
  extends: .cki_tools
  script:
    - tox -e scip
  rules:
    - if: $CI_PIPELINE_SOURCE =~ /^(web|push)$/
      allow_failure: true
    - when: manual
      allow_failure: true
  artifacts:
    reports:
      lsif: dump.lsif
