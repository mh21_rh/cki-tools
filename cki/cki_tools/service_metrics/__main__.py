"""Expose CKI service metrics."""
from cki_lib import misc
import sentry_sdk

from . import run

if __name__ == '__main__':
    misc.sentry_init(sentry_sdk)
    run()
