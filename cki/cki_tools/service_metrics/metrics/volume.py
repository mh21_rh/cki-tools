"""Disk metrics."""
import os
import shutil

from cki_lib import yaml
from cki_lib.cronjob import CronJob
import prometheus_client

VOLUMES_LIST = yaml.load(contents=os.environ.get('VOLUMES_LIST', '[]'))


class VolumeMetrics(CronJob):
    # pylint: disable=too-few-public-methods
    """Calculate Volume metrics."""

    schedule = '*/5 * * * *'

    metric_usage = prometheus_client.Gauge(
        'cki_volume_usage',
        'The number of used bytes on a volume',
        ['name']
    )

    def run(self, **_):
        """Update the metrics."""
        for volume in VOLUMES_LIST:
            usage = shutil.disk_usage(volume['path'])
            self.metric_usage.labels(volume['name']).set(usage.used)
