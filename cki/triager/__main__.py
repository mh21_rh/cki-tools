"""Datawarehouse failure triager."""

from cki_lib import misc
import sentry_sdk

from .triager import main

misc.sentry_init(sentry_sdk)
main()
