---
title: cki.beaker_tools.broken_machines
linkTitle: broken_machines
description: Beaker broken machines updater
---

This script queries the Beaker database and gets a list of broken
machines from [Beaker](https://beaker-project.org).

The produced list of machines is used when submitting testing
jobs so we avoid infrastructure failures which we'd encounter
by running on these machines.

| Environment variable         | Secret | Required | Description                                                                          |
|------------------------------|--------|----------|--------------------------------------------------------------------------------------|
| `MARIADB_HOST`               | no     | yes      | Beaker DB host name                                                                  |
| `MARIADB_PORT`               | no     | yes      | Beaker DB port                                                                       |
| `MARIADB_USER`               | no     | yes      | Beaker DB user name with readonly access rights                                      |
| `MARIADB_PASSWORD`           | yes    | yes      | Beaker DB password                                                                   |
| `MARIADB_DATABASE`           | no     | yes      | Beaker DB name                                                                      |
| `BUCKET_CONFIG_NAME`         | no     | yes      | Name of environment variable with S3 bucket spec to use as a backing store           |
| `CKI_DEPLOYMENT_ENVIRONMENT` | no     | no       | Define the deployment environment (production/staging)                               |
| `SENTRY_SDN`                 | yes    | no       | Sentry SDN                                                                           |
| `LIST_PATH`                  | no     | no       | Path for the machines list file in the bucket (default: `/broken-machines-list.txt`) |
| `THRESHOLD_RECIPES_RUN`      | no     | no       | Minimum recipes a machine needs to have run to be included (default: 15)             |
| `THRESHOLD_BROKEN`           | no     | no       | Ratio of failed jobs for machines to be considered broken (default: 0.5)             |
| `MIN_DAYS_TO_CHECK`          | no     | no       | Minimum days in the past to be included in the checks (default: 3)                   |
| `MAX_DAYS_TO_CHECK`          | no     | no       | Maximum days in the past to be included in the checks (default: 7)                   |
| `LIMIT_RESULTS`              | no     | no       | Number of machines to be included in the resulting list (default: 50)                |

## CKI_DEPLOYMENT_ENVIRONMENT

On staging developments (`CKI_DEPLOYMENT_ENVIRONMENT != production`), the
changes are only logged but not uploaded to the S3 bucket.
