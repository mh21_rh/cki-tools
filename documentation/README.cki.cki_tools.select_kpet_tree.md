---
title: cki.cki_tools.select_kpet_tree
linkTitle: select_kpet_tree
description: Determine kpet tree from RHEL tree family and kernel NVR
---

```shell
usage: python3 -m cki.cki_tools.select_kpet_tree [-h]
                           [--package-name PACKAGE_NAME]
                           [--kernel-version KERNEL_VERSION]
                           [--nvr-tree-mapping NVR_TREE_MAPPING]
                           [--nvr-tree-mapping-file NVR_TREE_MAPPING_FILE]

options:
  -h, --help            show this help message and exit
  --package-name PACKAGE_NAME
                        kernel binary RPM base name
  --kernel-version KERNEL_VERSION
                        kernel Version-Release information
  --nvr-tree-mapping NVR_TREE_MAPPING
                        nvr-tree mapping data as YAML
  --nvr-tree-mapping-file NVR_TREE_MAPPING_FILE
                        File with nvr-tree mapping data
```

## Configuration

The tree mapping data can be found in [kpet-db].

Example:

```yaml
rhel79-z: '3.10.0-1160..*.el7'
rhel8{minor}-z: '.el8_(?P<minor>[1246789])'
rhel9: ['.el9_[456789]', '.el9(?!_)']
```

The RHEL 7.9 z-stream is matched on both version and release. Any dots not
followed by `*` are escaped before matching the regular expressions. For RHEL 8
z-stream, the match is done solely on the release. For RHEL 9 y-stream, only
one of the multiple expressions has to match.

If a package name contains `-rt`, it is considered an RT kernel. In this case,
`-rt` is appended to the kpet tree.

[kpet-db]: https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db/-/blob/main/nvr-tree-mapping.yml?ref_type=heads
