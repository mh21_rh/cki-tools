---
title: cki_tools.gitrepo_trigger
linkTitle: gitrepo_trigger
description: Trigger CKI pipelines for kernels from git repos
---

## Usage

```shell
usage: python3 -m cki.cki_tools.gitrepo_trigger [-h] [--gitlab-url GITLAB_URL]
                       [--config CONFIG] [--config-path CONFIG_PATH]

options:
  -h, --help            show this help message and exit
  --gitlab-url GITLAB_URL
                        GitLab URL
  --config CONFIG       YAML configuration file to use
  --config-path CONFIG_PATH
                        Path to YAML configuration file
```

## Running as a cron job

```bash
python -m cki_tools.gitrepo_trigger
```

### Configuration via environment variables

The following variables need to be defined:

| Name                          | Type   | Secret | Required | Description                                                                                                |
|-------------------------------|--------|--------|----------|------------------------------------------------------------------------------------------------------------|
| `GITREPO_TRIGGER_CONFIG`      | yaml   | no     | no       | Configuration in YAML. If not present, falls back to `GITREPO_TRIGGER_CONFIG_PATH`                         |
| `GITREPO_TRIGGER_CONFIG_PATH` | path   | no     | no       | Path to the configuration YAML file                                                                        |
| `PIPELINE_DEFINITION_URL`     | url    | no     | yes      | URL of the pipeline-definition repository                                                                  |
| `GITLAB_PARENT_PROJECT`       | string | no     | yes      | Parent group of pipeline projects                                                                          |
| `GITLAB_URL`                  | url    | no     | yes      | URL of GitLab instance                                                                                     |
| `GITLAB_TOKENS`               | json   | no     | yes      | URL/environment variable pairs of GitLab instances and private tokens                                      |
| `GITLAB_TOKEN`                | string | yes    | yes      | GitLab private tokens as configured in `GITLAB_TOKENS`                                                     |
| `CKI_DEPLOYMENT_ENVIRONMENT`  | string | no     | no       | Define the deployment environment (production/staging)                                                     |
| `CKI_LOGGING_LEVEL`           | string | no     | no       | logging level for CKI modules, defaults to WARN; to get meaningful output on the command line, set to INFO |
| `SENTRY_DSN`                  | url    | yes    | no       | Sentry DSN                                                                                                 |

## Pipeline configuration YAML

Basic inheritance and defaults are modeled after the
[default](https://docs.gitlab.com/ce/ci/yaml/#global-defaults) and
[extends](https://docs.gitlab.com/ce/ci/yaml/#extends) keywords in
the [GitLab CI/CD job descriptions](https://docs.gitlab.com/ce/ci/yaml) can be
used.

* trigger configurations with a name starting with a dot (`.`) are not
  processed, but can be used for inheritance
* the `.default` trigger configuration will be inherited by all trigger
  configurations
* the configuration option `.extends` followed by a name or list of names
  specifies trigger configuration(s) to inherit from
* configuration options with a name starting with a dot (`.`) are used to
  configure the trigger, but are NOT passed to the pipeline code as trigger
  variables

### Supported configuration options

Next to variables that are passed through to the pipeline as trigger variables,
certain configuration options are required by the git repo trigger.

| Name                   | Description                                                                             |
|------------------------|-----------------------------------------------------------------------------------------|
| `git_url`              | kernel git repository                                                                   |
| `.branches`            | branches in the kernel git repository                                                   |
| `watch_url`            | instead of the kernel git repository, trigger on changes in this git repository instead |
| `watch_branch`         | branches to watch                                                                       |
| `minimum_interval`     | minimum time interval between created pipelines, e.g. `6 hours` or `1 day`              |
| `concurrency_limit`    | maximum number of concurrently running pipelines                                        |
| `concurrency_group`    | group filter for `minimum_interval` and `concurrency_limit`, see below                  |
| `.report_rules`        | serialized via JSON to `report_rules` trigger variable                                  |
| `cki_pipeline_project` | GitLab pipeline project                                                                 |
| `cki_pipeline_branch`  | target branch in the GitLab pipeline project                                            |

By default, `minimum_interval` and `concurrency_limit` determine the previous
pipelines the same way previously triggered pipelines for a commit are
detected: by using a filter containing `watch_url`, `watch_branch` and
`package_name`. To be able to limit the number of pipelines across multiple
trigger configurations, a custom filter can be specified via
`concurrency_group`. In that case, the default variable filter is not used.
