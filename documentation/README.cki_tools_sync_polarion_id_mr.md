---
title: cki_tools_sync_polarion_id_mr.sh
description: Create MRs for updated Polarion IDs
aliases: [/l/sync-polarion-id-mr-docs]
---

```shell
cki_tools_sync_polarion_id_mr.sh
```

The underlying functionality is implemented in `cki_tools.sync_polarion_id`.

## Environment variables

Next to the variables for the [Python module][sync_polarion_id], the
following variables need to be defined:

| Field                | Type   | Required | Description                                        |
|----------------------|--------|----------|----------------------------------------------------|
| `GITLAB_PROJECT_URL` | url    | yes      | Git repo URL without protocol                      |
| `GITLAB_FORK_URL`    | url    | yes      | URL of the fork of the Git repo without protocol   |
| `GITLAB_TOKEN`       | string | yes      | GitLab private token with access to the Git repo   |
| `BRANCH_NAME`        | string | yes      | Branch name in the fork of the git repo            |
| `SOURCE_BRANCH_NAME` | string | no       | Source branch name to checkout, defaults to `main` |
| `REVIEWERS`          | string | yes      | GitLab users to notify in MR description           |
| `GIT_USER_NAME`      | string | yes      | Git user name                                      |
| `GIT_USER_EMAIL`     | string | yes      | Git user email                                     |

[sync_polarion_id]: README.cki_tools.sync_polarion_id.md
