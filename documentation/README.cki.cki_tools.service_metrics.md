---
title: cki.cki_tools.service_metrics
linkTitle: service_metrics
description: Service metrics exposed via Prometheus
---

The service metrics framework runs collector modules on specific schedules to
generate various metrics about CKI or external services.
