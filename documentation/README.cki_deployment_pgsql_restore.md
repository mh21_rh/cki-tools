---
title: cki_deployment_pgsql_restore.sh
description: Restore a PostgreSQL database from an S3 bucket
---

```shell
cki_deployment_pgsql_restore.sh
```

## Environment variables

| Field                 | Type   | Required | Description                                               |
|-----------------------|--------|----------|-----------------------------------------------------------|
| `BUCKET_CONFIG_NAME`  | string | yes      | Name of an environment variable with bucket configuration |
| `POSTGRESQL_USER`     | no     | yes      | PostgreSQL user name                                      |
| `POSTGRESQL_PASSWORD` | yes    | yes      | PostgreSQL password                                       |
| `POSTGRESQL_HOST`     | no     | yes      | PostgreSQL host                                           |
| `POSTGRESQL_DATABASE` | no     | yes      | PostgreSQL database                                       |
