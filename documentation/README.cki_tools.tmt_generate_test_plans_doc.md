---
title: cki_tools.tmt_generate_test_plans_doc
linkTitle: tmt_generate_test_plans_doc
description: Generate documentation from tmt plans
---

## Usage

```shell
usage: python3 -m cki_tools.tmt_generate_test_plans_doc --plans-dir ../test-plans

Creates the related .md files under ../test-plans/documentation directory

options:
  -h, --help            show this help message and exit
  --plans-dir PLANS_DIR
                        Path to repository with tmt plans
```

## Configuration via environment variables

The following variables need to be defined:

| Name                           | Type   | Secret | Required | Description                                                                                                |
|--------------------------------|--------|--------|----------|------------------------------------------------------------------------------------------------------------|
| `CKI_DEPLOYMENT_ENVIRONMENT`   | string | no     | no       | Define the deployment environment (production/staging)                                                     |
| `CKI_LOGGING_LEVEL`            | string | no     | no       | logging level for CKI modules, defaults to WARN; to get meaningful output on the command line, set to INFO |
| `SENTRY_DSN`                   | url    | yes    | no       | Sentry DSN                                                                                                 |
