---
title: cki_tools.pipeline_data_gen_tree_doc
linkTitle: pipeline_data_gen_tree_doc
description: Generate documentation about onboarded kernel trees defined on pipeline-data
---

## Usage

```shell
usage: python3 -m cki_tools.pipeline_data_gen_tree_doc --pipeline-data ../pipeline-data

Creates the related .md files under ../pipeline-data directory

options:
  -h, --help                       show this help message and exit
  --pipeline-data pipeline-data    path to pipeline-data directory
```

## Configuration via environment variables

The following variables need to be defined:

| Name                           | Type   | Secret | Required | Description                                                                                                |
|--------------------------------|--------|--------|----------|------------------------------------------------------------------------------------------------------------|
| `CKI_DEPLOYMENT_ENVIRONMENT`   | string | no     | no       | Define the deployment environment (production/staging)                                                     |
| `CKI_LOGGING_LEVEL`            | string | no     | no       | logging level for CKI modules, defaults to WARN; to get meaningful output on the command line, set to INFO |
| `SENTRY_DSN`                   | url    | yes    | no       | Sentry DSN                                                                                                 |
