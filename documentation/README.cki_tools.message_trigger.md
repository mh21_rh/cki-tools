---
title: cki_tools.message_trigger
linkTitle: message_trigger
description: Trigger GitLab pipelines from webhook messages
---

## Usage

```text
usage: python3 -m cki_tools.message_trigger.main [-h] [--message MESSAGE] [--validate]

options:
  -h, --help         show this help message and exit
  --message MESSAGE  message data
  --validate         only validate configuration and exit
```

Without `--message`, listens to the AMQP message bus for messages and triggers
GitLab pipelines matching the configuration.

## Configuration

This service determines its configuration according to the first
`MESSAGE_TRIGGER_CONFIG*` environment variable that is set:

- from files in the `MESSAGE_TRIGGER_CONFIG_DIR` directory
- from the contents of `MESSAGE_TRIGGER_CONFIG`
- from the file in `MESSAGE_TRIGGER_CONFIG_PATH`

Example configuration:

```yaml
.schema: cki_tools.message_trigger/schema.yml
service:
  project_url: https://gitlab.com/cki-project/cki-tools
  ref: main
  conditions:
    field: {jmespath: body.field, regex: "[0-9]+"}
    path: {jmespath: body.some.path, regex: "(?P<captured_value>.+)"}
  variables:
    EXTRACTED_VAR: "{captured_value}"
    TRIGGER_VAR: variable-value
```

This will trigger a pipeline for the `main` branch of
<https://gitlab.com/cki-project/cki-tools> for each message that has a body
field corresponding to the `some.path` [JMESPath] and a number at `field`. The
pipeline will have two trigger variables: `EXTRACTED_VAR` is set to the value
at `some.path`, while `TRIGGER_VAR` is set to the static value `variable-value`.

| Field                 | Description                                                                                                                     |
|-----------------------|---------------------------------------------------------------------------------------------------------------------------------|
| `project_url`         | GitLab project URL that contains the deployment pipelines                                                                       |
| `ref`                 | Branch or ref for the triggered pipelines, defaults to main                                                                     |
| `conditions`          | Message filters, the key is ignored                                                                                             |
| `conditions.jmespath` | Limit to messages that match the path                                                                                           |
| `conditions.regex`    | If specified, limit to messages where the value matches the regex                                                               |
| `variables`           | Dictionary of trigger variable name-value pairs, with [named subgroups] from `conditions.regex` provided via [str.format_map()] |

The `jmespath` expressions are matched against a data structure that looks like
this:

```yaml
topic: rabbitmq.topic
headers:
  key1: value1
  ...
body:
  key1: value1
  nested:
    key2: value2
  ...
```

Complex (non-string) fields captured via JMESPath expressions are serialized as
JSON.

The regular expressions are configured with [re.DOTALL] by default to make it
easy to cope with multi-line fields. This can be disabled for part of the
expression by enclosing it in [(?-s:...)].

[re.DOTALL]: https://docs.python.org/3/library/re.html#re.DOTALL
[(?-s:...)]: https://docs.python.org/3/library/re.html#regular-expression-syntax

## Environment variables

| Environment variable          | Secret | Required | Description                                                                  |
|-------------------------------|--------|----------|------------------------------------------------------------------------------|
| `MESSAGE_TRIGGER_CONFIG_DIR`  | no     | no       | Path to the configuration YAML files, falls back to `MESSAGE_TRIGGER_CONFIG` |
| `MESSAGE_TRIGGER_CONFIG`      | no     | yes      | Configuration in YAML, falls back to `MESSAGE_TRIGGER_CONFIG_PATH`           |
| `MESSAGE_TRIGGER_CONFIG_PATH` | no     | no       | Path to a single configuration YAML file                                     |
| `GITLAB_TOKENS`               | no     | yes      | URL/environment variable pairs of GitLab instances and private tokens        |
| `GITLAB_TOKEN`                | yes    | yes      | GitLab private tokens as configured in `GITLAB_TOKENS` above                 |
| `RABBITMQ_HOST`               | no     | yes      | RabbitMQ host                                                                |
| `RABBITMQ_PORT`               | no     | no       | RabbitMQ port, TLS is used for port 443                                      |
| `RABBITMQ_USER`               | no     | yes      | RabbitMQ user                                                                |
| `RABBITMQ_PASSWORD`           | yes    | yes      | RabbitMQ password                                                            |
| `RABBITMQ_CAFILE`             | no     | no       | RabbitMQ CA file path                                                        |
| `RABBITMQ_CERTFILE`           | no     | no       | RabbitMQ certificate + private key file path                                 |
| `CKI_DEPLOYMENT_ENVIRONMENT`  | no     | no       | Define the deployment environment (production/staging)                       |
| `CKI_LOGGING_LEVEL`           | no     | no       | logging level for CKI modules, defaults to WARN                              |
| `SENTRY_SDN`                  | yes    | no       | Sentry SDN                                                                   |

### `CKI_DEPLOYMENT_ENVIRONMENT`

On non-production deployments (`CKI_DEPLOYMENT_ENVIRONMENT != production`), no
pipelines are triggered.

[JMESPath]: https://jmespath.org/
[named subgroups]: https://docs.python.org/3/library/re.html#re.Match.groupdict
[str.format_map()]: https://docs.python.org/3/library/stdtypes.html#str.format_map
