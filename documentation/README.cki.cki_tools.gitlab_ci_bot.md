---
title: cki.cki_tools.gitlab_ci_bot
linkTitle: gitlab_ci_bot
description: CKI CI bot to trigger pipelines from merge requests
aliases: [/l/gitlab-ci-bot-docs]
---

Interact with a user in a merge request for the pipeline definition, and
trigger pipelines for it.

## Environment variables

| Environment variable         | Description                                                                       |
|------------------------------|-----------------------------------------------------------------------------------|
| `CKI_DEPLOYMENT_ENVIRONMENT` | Define the deployment environment (production/staging)                            |
| `GITLAB_CI_BOT_CONFIG`       | Configuration in YAML. If not present, falls back to `GITLAB_CI_BOT_CONFIG_PATH`. |
| `GITLAB_CI_BOT_CONFIG_PATH`  | Path to the configuration YAML file (Default: config.yml)                         |
| `SENTRY_SDN`                 | Sentry SDN                                                                        |
