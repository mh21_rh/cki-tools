---
title: cki.deployment_tools.gitlab_codeowners_config
linkTitle: gitlab_codeowners_config
description: Maintain a CODEOWNERS file
aliases: [/l/gitlab-codeowners-config-docs]
---

## CLI interface

```shell
python3 -m cki.deployment_tools.gitlab_codeowners_config \
    [--config-path CONFIG_PATH]
    [--test-path TEST_PATH]
    [--context-lines CONTEXT_LINES] [--interactive] [--comment-mr-iid MR_ID]
    {generate,update,diff,needs-update}
```

Depending on the action on the command line, a CODEOWNERS file will be either

- generated and printed (`generate`)
- updated in-place (`update`)
- compared to the current version (`diff`)
- check whether the configuration has changed (`needs-update`)

For diffing, `--interactive` will start an editor to interactively explore the
changes, and `--comment-mr-iid` will post the diff as an MR comment.

## Configuration

The CODEOWNERS file will be updated based on the information in a special
comment framed by `# GITLAB-CODEOWNERS-CONFIG-BEGIN/END`.

Example:

```text
[manual-section]
file @user1 @user2

# GITLAB-CODEOWNERS-CONFIG-BEGIN
# general:
#   project_url: https://gitlab.com/group/project
#   access_level: 30
# sections:
#   simple-section:
#     users:
#       - user-or-group
#     paths:
#       - file-path
#   kpet-locations:
#     users:
#       - another-user
#     kpet: locations
#   kpet-maintainers:
#     minimum_count: 2
#     kpet: maintainers
# GITLAB-CODEOWNERS-CONFIG-END
```

The `general` dictionary contains the project URL and a minimum access level.
Only direct members of the project with this access level or higher will be
included in the CODEOWNERS file.

The `sections` list contains the individual sections. For each section, users
or groups can be specified in `users`. Groups will be automatically looked up
and expanded to the member list. Paths for those users can be either specified
directly in `paths` or via `kpet: locations` to read them from the output of
`kpet test list -o json` given via `--test-path`. With `kpet: maintainers`,
both users and paths will be obtained from the kpet data. Rules will only get
created if at least `minimum_count` (default 1) owners are specified.

## Environment variables

| Name            | Secret | Required | Description                                                           |
|-----------------|--------|----------|-----------------------------------------------------------------------|
| `GITLAB_TOKENS` | no     | yes      | URL/environment variable pairs of GitLab instances and private tokens |
| `GITLAB_TOKEN`  | yes    | yes      | GitLab private tokens as configured in `gitlab_tokens` above          |
