"""Tests for generate documentation from tmt plans."""
from importlib import resources
import pathlib
import tempfile
import unittest
from unittest import mock
from unittest.mock import MagicMock

from cki_tools import tmt_generate_test_plans_doc
from tests.assets import tmt_generate_test_plans_doc as assets


class TestGenPlanDoc(unittest.TestCase):
    """Tests to generate output of specific tmt plan."""

    def test_gen_plan_doc(self):
        """Make sure the function return expected output based on plan info."""
        plan_dict = {'name': 'test/test-with-url',
                     'summary': 'test with discover url',
                     'contact': [],
                     'enabled': True,
                     'discover': [{'url': 'https://test.url'}]}
        expected_output = [
            '## test/test-with-url\n',
            'test with discover url\n',
            '- enabled: `true`',
            '- discover:',
            '  - url: <https://test.url>',
            ''
        ]
        output = tmt_generate_test_plans_doc.gen_plan_doc(plan_dict)
        self.assertEqual(output, expected_output)

    @mock.patch('cki_tools.tmt_generate_test_plans_doc.subprocess.run')
    def test_main(self, mock_run):
        """Make sure it can generate the documentation."""
        mock_stdout = MagicMock(stdout=(resources.files(assets) / 'plans.yaml').read_text('utf8'))
        mock_run.return_value = mock_stdout
        with tempfile.TemporaryDirectory() as tempdir:
            tmt_generate_test_plans_doc.main(['--plans-dir', tempdir])
            expected_output = (resources.files(assets) / 'README.md').read_text('utf8')
            output = pathlib.Path(f'{tempdir}/documentation/README.md').read_text('utf8')
            self.assertEqual(output, expected_output)

            expected_output = (resources.files(assets) / 'security.md').read_text('utf8')
            output = pathlib.Path(f'{tempdir}/documentation/security.md').read_text('utf8')
            self.assertEqual(output, expected_output)
