"""Test the matchers."""
import copy
import datetime
import json
import unittest
from unittest import mock

import responses

from cki_tools.pipeline_herder import main
from cki_tools.pipeline_herder import utils


def get_match_result(name: str) -> utils.MatchResult:
    """Return MatchResult for name."""
    return utils.MatchResult(
        name=name,
        description=mock.ANY,
        action=mock.ANY,
        maximum_artifact_size=mock.ANY,
        retry_delays=mock.ANY,
        retry_limit=mock.ANY,
    )


class TestChecker(unittest.TestCase):
    """Base class with Gitlab variables."""

    user = {
        'id': 2,
    }

    project_project = {
        'id': 'project',
        'path_with_namespace': 'project',
    }

    pipeline_524445 = {
        'id': 524445,
        'user': {
            'id': 1,
        },
        'created_at': '2000-01-01T00:00:00',
        'web_url': 'https://host/project/-/pipelines/524445',
    }

    variables_524445 = []

    jobs_524445 = [{
        'id': 764828,
        'name': 'build x86_64',
        'user': {
            'id': 1,
        },
        'pipeline': {
            'id': 524445,
            'project_id': 'project',
            'web_url': 'https://host/project/-/pipelines/524445',
        },
        'created_at': '2000-01-01T00:00:01',
    }]

    job_764828 = {
        'id': 764828,
        'status': 'failed',
        'stage': 'build',
        'name': 'build x86_64',
        'pipeline': {
            'id': 524445,
            'project_id': 'project',
            'web_url': 'https://host/project/-/pipelines/524445',
        },
        'user': {
            'id': 1,
            'username': 'user',
        },
        'web_url': 'https://host/project/-/jobs/764828',
    }

    artifacts_764828 = {}

    base_url = 'https://host/api/v4/projects/project'
    s3_base_url = 'https://s3-host/bucket/job-prefix'

    def mock_responses(self, s3=False):
        responses.add(responses.GET, 'https://host/api/v4/user',
                      json=self.user)
        responses.add(responses.GET, f'{self.base_url}',
                      json=self.project_project)
        responses.add(responses.GET, f'{self.base_url}/jobs/764828',
                      json=self.job_764828)
        joiner = '\n' if self.trace_764828 and isinstance(self.trace_764828[0], str) else b'\n'
        responses.add(responses.GET, f'{self.base_url}/jobs/764828/trace',
                      body=joiner.join(self.trace_764828))
        if s3:
            url = f'{self.base_url}/jobs/764828/artifacts/artifacts-meta.json'
            responses.add(responses.GET, url,
                          json.dumps({'mode': 's3', 's3_url': self.s3_base_url}))
            for name, contents in self.artifacts_764828.items():
                url = f'{self.s3_base_url}/{name}'
                joiner = '\n' if contents and isinstance(contents[0], str) else b'\n'
                responses.add(responses.GET, url, joiner.join(contents))
        else:
            for name, contents in self.artifacts_764828.items():
                url = f'{self.base_url}/jobs/764828/artifacts/{name}'
                joiner = '\n' if contents and isinstance(contents[0], str) else b'\n'
                responses.add(responses.GET, url, joiner.join(contents))
        responses.add(responses.GET, f'{self.base_url}/pipelines/524445',
                      json=self.pipeline_524445)
        responses.add(responses.GET, f'{self.base_url}/pipelines/524445/jobs?include_retried=True',
                      json=self.jobs_524445)
        responses.add(responses.GET,
                      f'{self.base_url}/pipelines/524445/variables',
                      json=self.variables_524445)


@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('cki_tools.pipeline_herder.main.notify_finished', mock.Mock())
class TestCode137(TestChecker):
    """Test the detection of pod resource exhaustion (exit code 137)."""

    trace_764828 = (
        '... snip ...',
        '%_rpmdir /builds/cki-project/cki-pipeline/workdir/rpms',
        'section_end:1586362335:build_script',
        '\x1b[0Ksection_start:1586362335:after_script',
        '\x1b[0K\x1b[0K\x1b[36;1mRunning after_script\x1b[0;m',
        '\x1b[0;msection_end:1586362335:after_script',
        '\x1b[0Ksection_start:1586362335:upload_artifacts_on_failure',
        '\x1b[0K\x1b[0K\x1b[36;1mUploading artifacts for failed job\x1b[0;m',
        '\x1b[0;msection_end:1586362335:upload_artifacts_on_failure',
        '\x1b[0K\x1b[31;1mERROR: Job failed: command terminated with exit code 137',
        '\x1b[0;m',
    )

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    @mock.patch.object(utils, 'CONFIG', {'matchers': [{
        'name': 'code137',
        'description': 'Pod resource exhaustion (exit code 137)',
        'messages': ['ERROR: Job failed: command terminated with exit code 137'],
    }]})
    def test_exit_137(self, submit_retry: mock.MagicMock):
        """Test the detection of pod resource exhaustion (exit code 137)."""
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertEqual(action, 'retry')
        submit_retry.assert_called_once_with(
            'https://host/project/-/jobs/764828', get_match_result('code137'),
            datetime.timedelta(seconds=300))

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    @mock.patch.object(utils, 'CONFIG', {'matchers': [{
        'name': 'code137',
        'description': 'Pod resource exhaustion (exit code 137)',
        'messages': ['ERROR: Job failed: command terminated with exit code 137'],
    }]})
    def test_exit_137_non_utf8(self, submit_retry: mock.MagicMock):
        """Test the detection of pod resource exhaustion (exit code 137) with broken encoding."""
        self.trace_764828 = (
            b'\xff ERROR: Job failed: command terminated with exit code 137',
        )
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertEqual(action, 'retry')
        submit_retry.assert_called_once_with(
            'https://host/project/-/jobs/764828', get_match_result('code137'),
            datetime.timedelta(seconds=300))

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    @mock.patch.object(
        utils,
        'CONFIG',
        {
            'matchers': [
                {
                    'name': 'code137',
                    'description': 'Pod resource exhaustion (exit code 137)',
                    'messages': ['ERROR: Job failed: command terminated with exit code 137'],
                    'variables': {'CKI_RETRIGGER_PIPELINE': [None]},
                }
            ]
        },
    )
    def test_job_retrigger_ignored(self, submit_retry: mock.MagicMock):
        """Test the detection with different environment names."""
        self.variables_524445 = [{'key': 'CKI_RETRIGGER_PIPELINE', 'value': 'true'}]
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertEqual(action, None)
        submit_retry.assert_not_called()

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    @mock.patch.object(
        utils,
        'CONFIG',
        {
            'matchers': [
                {
                    'name': 'code137',
                    'description': 'Pod resource exhaustion (exit code 137)',
                    'messages': ['ERROR: Job failed: command terminated with exit code 137'],
                    'variables': {'CKI_RETRIGGER_PIPELINE': ['true']},
                }
            ]
        },
    )
    def test_job_retrigger_ok(self, submit_retry: mock.MagicMock):
        """Test the detection with different environment names."""
        self.variables_524445 = [{'key': 'CKI_RETRIGGER_PIPELINE', 'value': 'true'}]
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertEqual(action, 'retry')
        submit_retry.assert_called_once_with(
            'https://host/project/-/jobs/764828', get_match_result('code137'),
            datetime.timedelta(seconds=300))


@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('cki_tools.pipeline_herder.main.notify_finished', mock.Mock())
@mock.patch.object(utils, 'CONFIG', {'matchers': [{
    'name': 'force-push',
    'messages': [r'/fatal: reference is not a tree:/'],
    'action': 'alert',
}]})
class TestForcePush(TestChecker):
    """Test the detection of force push on merge jobs."""

    trace_764828 = (
        '... snip ...',
        'See "git help gc" for manual housekeeping.',
        'Successfully rebased and updated refs/heads/queue/5.7.',
        'fatal: reference is not a tree: d6d92c1d',
        '202e1926e06333ee3dee9fe2c1b5ed78',
        'Running after_script',
        '00:01',
        'Running after script...',
    )

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    def test_force_push(self, submit_retry: mock.MagicMock):
        """Test force push checks."""
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertEqual(action, 'alert')
        self.assertFalse(submit_retry.called)


@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('cki_tools.pipeline_herder.main.notify_finished', mock.Mock())
@mock.patch.object(utils, 'CONFIG', {'matchers': [{
    'name': 'error-dummy',
    'description': 'Dummy error',
    'messages': ['Something failed'],
    'action': 'something-unhandled',
}]})
class TestFallback(TestChecker):
    """Test the detection of force push on merge jobs."""

    trace_764828 = (
        'Something failed',
    )

    @responses.activate
    def test_fallback(self):
        """Test fall backing to error action if action is not handled."""
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertEqual(action, 'error')


@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('cki_tools.pipeline_herder.main.notify_finished', mock.Mock())
@mock.patch.object(utils, 'CONFIG', {'matchers': [{
    'name': 'too-large',
    'messages': [r'/ERROR: Uploading artifacts( as "archive")? to coordinator... too large/'],
    'action': 'report',
}]})
class TestTooLarge(TestChecker):
    """Test the detection of artifacts size exceeded."""

    trace_764828 = (
        '... snip ...',
        'Uploading artifacts for failed job',
        'Uploading artifacts...',
        'artifacts: found 2865 matching files',
        'ERROR: Uploading artifacts to coordinator... too large archive  ',
        'id=931289 responseStatus=413 Request Entity Too Large status=413 ',
        'Request Entity Too Large token=s216cQxY',
        'FATAL: too large',
        'ERROR: Job failed: exit code 2',
    )

    @responses.activate
    def test_artifacts_size(self):
        """Test the detection of artifact size exceeded."""
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertEqual(action, 'report')


@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('cki_tools.pipeline_herder.main.notify_finished', mock.Mock())
@mock.patch.object(utils, 'CONFIG', {'matchers': [{
    'name': 'missing-git-cache',
    'messages': [r'/download failed: s3://cki/git-cache/.* error occurred'
                 r' \(NoSuchKey\) when calling the GetObject operation/'],
    'action': 'alert',
}]})
class TestGitCache(TestChecker):
    """Test the detection of missing git-caches."""

    trace_764828 = (
        '... snip ...',
        'download failed: s3://cki/git-cache/torvalds.linux.tar to - An error'
        ' occurred (NoSuchKey) when calling the GetObject operation: Unknown',
        'tar: This does not look like a tar archive',
        'tar: Exiting with failure status due to previous errors',
        '... snip ...',
        'ERROR: Job failed: command terminated with exit code 1',
    )

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    def test_git_cache(self, submit_retry: mock.MagicMock):
        """Test the detection of artifact size exceeded."""
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertEqual(action, 'alert')
        self.assertFalse(submit_retry.called)


@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('cki_tools.pipeline_herder.main.notify_finished', mock.Mock())
class TestArtifactsMatching(TestChecker):
    """Test the detection of missing git-caches."""

    trace_764828 = (
        'Something failed',
    )

    artifacts_764828 = {'artifacts/build.log': (
        '00:01:18 /bin/sh: fork: Resource temporarily unavailable',
        '00:01:19   LD [M]  drivers/gpu/drm/vmwgfx/vmwgfx.o',
        '00:01:19 make[3]: *** [scripts/Makefile.build:500: fs] Error 2',
        '... snip ...',
        '00:01:21 make: *** [Makefile:1523: targz-pkg] Error 2',
    )}

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    @mock.patch.object(utils, 'CONFIG', {'matchers': [{
        'name': 'error-dummy',
        'description': 'Dummy error',
        'messages': ['Something failed'],
        'job_name': 'build',
    }]})
    def test_job_match(self, submit_retry: mock.MagicMock):
        """Test the detection only for specific jobs."""
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertEqual(action, 'retry')
        self.assertTrue(submit_retry.called)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    @mock.patch.object(utils, 'CONFIG', {'matchers': [{
        'name': 'error-dummy',
        'description': 'Dummy error',
        'messages': ['Something failed'],
        'job_name': 'test',
    }]})
    def test_job_nomatch(self, submit_retry: mock.MagicMock):
        """Test the detection if the job name differs."""
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertIsNone(action)
        self.assertFalse(submit_retry.called)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    @mock.patch.object(utils, 'CONFIG', {'matchers': [{
        'name': 'error-dummy',
        'description': 'Dummy error',
        'messages': ['Something failed'],
        'job_name': 'build',
    }]})
    def test_job_artifact(self, submit_retry: mock.MagicMock):
        """Test the detection in the job artifacts."""
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertEqual(action, 'retry')
        self.assertTrue(submit_retry.called)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    @mock.patch.object(utils, 'CONFIG', {'matchers': [{
        'name': 'error-dummy',
        'description': 'Dummy error',
        'messages': ['Something failed'],
        'job_name': 'build',
    }]})
    def test_job_artifact_non_utf8(self, submit_retry: mock.MagicMock):
        """Test the detection in the job artifacts with encoding errors."""
        self.artifacts_764828 = {'artifacts/build.log': (
            b'\xff fork: Resource temporarily unavailable',
        )}
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertEqual(action, 'retry')
        self.assertTrue(submit_retry.called)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    @mock.patch.object(utils, 'CONFIG', {'matchers': [{
        'name': 'error-dummy',
        'description': 'Dummy error',
        'messages': ['Something failed'],
        'job_name': 'build',
    }]})
    def test_job_artifact_s3(self, submit_retry: mock.MagicMock):
        """Test the detection in the S3 job artifacts."""
        self.mock_responses(s3=True)

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertEqual(action, 'retry')
        self.assertTrue(submit_retry.called)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    @mock.patch.object(utils, 'CONFIG', {'matchers': [{
        'name': 'error-dummy',
        'description': 'Dummy error',
        'messages': ['Something failed'],
        'job_status': ['passed'],
    }]})
    def test_job_status(self, submit_retry: mock.MagicMock):
        """Test the detection only for specific job states."""
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertEqual(action, None)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    @mock.patch.object(utils, 'CONFIG', {'matchers': [{
        'name': 'error-dummy',
        'description': 'Dummy error',
        'messages': ['Something failed'],
        'job_status': ['passed', 'failed'],
    }]})
    def test_job_status_multiple(self, submit_retry: mock.MagicMock):
        """Test the detection only for specific job states."""
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertEqual(action, 'retry')
        self.assertTrue(submit_retry.called)


@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
class TestNotifications(TestChecker):
    """Test the notifications for non-failures."""

    trace_764828 = (
        'WARNING XML-RPC connection to beaker.engineering.redhat.com failed',
    )

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry', mock.MagicMock())
    @mock.patch('cki_tools.pipeline_herder.main.notify_finished')
    @mock.patch.object(utils, 'CONFIG', {'matchers': [{
        'name': 'error-dummy',
        'description': 'Dummy error',
        'messages': ['all is ok'],
    }]})
    def test_ok(self, notify_finished):
        """Check that the job is notified when it's all good."""
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertEqual(None, action)
        self.assertTrue(notify_finished.called)
        notify_finished.assert_called_with('https://host/project/-/jobs/764828')

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry', mock.MagicMock())
    @mock.patch('cki_tools.pipeline_herder.main.notify_finished')
    @mock.patch.object(utils, 'CONFIG', {'matchers': [{
        'name': 'error-dummy',
        'description': 'Dummy error',
        'messages': ['WARNING XML-RPC'],
        'action': 'retry',
    }]})
    def test_retry(self, notify_finished):
        """Check that the job is not notified when the matcher retries."""
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertEqual('retry', action)
        self.assertFalse(notify_finished.called)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry', mock.MagicMock())
    @mock.patch('cki_tools.pipeline_herder.main.notify_finished')
    @mock.patch.object(utils, 'CONFIG', {'matchers': [{
        'name': 'error-dummy',
        'description': 'Dummy error',
        'messages': ['WARNING XML-RPC'],
        'action': 'alert',
    }]})
    def test_alert(self, notify_finished):
        """Check that the job is not notified when the matcher alerts."""
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertEqual('alert', action)
        self.assertFalse(notify_finished.called)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry', mock.MagicMock())
    @mock.patch('cki_tools.pipeline_herder.main.notify_finished')
    @mock.patch.object(utils, 'CONFIG', {'matchers': [{
        'name': 'error-dummy',
        'description': 'Dummy error',
        'messages': ['WARNING XML-RPC'],
        'action': 'report',
    }]})
    def test_report(self, notify_finished):
        """Check that the job is notified when the matcher reports."""
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertEqual('report', action)
        self.assertTrue(notify_finished.called)
        notify_finished.assert_called_with('https://host/project/-/jobs/764828')


@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('cki_tools.pipeline_herder.main.notify_finished', mock.Mock())
@mock.patch.object(utils, 'CONFIG', {'matchers': [{
    'builtin': 'no_trace',
    'action': 'report',
}]})
class TestNoTrace(TestChecker):
    """Test the detection of jobs without trace."""

    trace_764828 = ()

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    def test_no_trace(self, submit_retry: mock.MagicMock):
        """Test job without trace."""
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertEqual(action, 'report')
        self.assertFalse(submit_retry.called)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    def test_no_trace_ok(self, submit_retry: mock.MagicMock):
        """Test job with trace."""
        self.trace_764828 = ('foobar',)
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertEqual(action, None)
        self.assertFalse(submit_retry.called)


@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('cki_tools.pipeline_herder.main.notify_finished', mock.Mock())
@mock.patch.object(utils, 'CONFIG', {'matchers': [{
    'builtin': 'missed_tests',
    'action': 'report',
}]})
class TestTestsNotRun(TestChecker):
    """Test the detection of tests not run."""

    trace_764828 = (
        'Everything finished ok.'
    )

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    def test_ok(self, submit_retry: mock.MagicMock):
        """Check that all tests are ok."""
        self.job_764828['stage'] = 'test'
        self.job_764828['name'] = 'test x86_64'
        self.artifacts_764828 = {
            'kcidb_all.json': [
                '{'
                '  "version": {"major": 4, "minor": 0},'
                '  "tests": ['
                '    {"id": "redhat:1", "origin": "redhat", "build_id": "rh:1", "status": "SKIP"},'
                '    {"id": "redhat:2", "origin": "redhat", "build_id": "rh:1", "status": "PASS"}'
                '  ]'
                '}'
            ]
        }
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertEqual(action, None)
        self.assertFalse(submit_retry.called)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    @mock.patch('cki_tools.pipeline_herder.utils.notify')
    def test_not_result(self, notify: mock.MagicMock, submit_retry: mock.MagicMock):
        """Check that at least a test has no results."""
        self.job_764828['stage'] = 'test'
        self.job_764828['name'] = 'test x86_64'
        self.artifacts_764828 = {
            'kcidb_all.json': [
                '{'
                '  "version": {"major": 4, "minor": 0},'
                '  "tests": ['
                '    {"id": "redhat:1", "origin": "redhat", "build_id": "rh:1"},'
                '    {"id": "redhat:2", "origin": "redhat", "build_id": "rh:1"},'
                '    {"id": "redhat:3", "origin": "redhat", "build_id": "rh:1", "status": "PASS"}'
                '  ]'
                '}'
            ]
        }
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertEqual(action, 'report')
        self.assertFalse(submit_retry.called)
        notify.assert_called_with(mock.ANY, 'Detected 2 tests that did not run', True)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    @mock.patch('cki_tools.pipeline_herder.utils.notify')
    def test_no_kcidb(self, notify: mock.MagicMock, submit_retry: mock.MagicMock):
        """Check that kcidb file can be missing."""
        self.job_764828['stage'] = 'test'
        self.job_764828['name'] = 'test x86_64'
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertIsNone(action)
        self.assertFalse(submit_retry.called)
        notify.assert_not_called()

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    @mock.patch('cki_tools.pipeline_herder.utils.notify')
    def test_forced_skip_status(self, notify: mock.MagicMock, submit_retry: mock.MagicMock):
        """Check that at least a test has forced_skip_status."""
        self.job_764828['stage'] = 'test'
        self.job_764828['name'] = 'test x86_64'
        self.artifacts_764828 = {
            'kcidb_all.json': [
                '{'
                '  "version": {"major": 4, "minor": 0},'
                '  "tests": ['
                '    {"id": "redhat:1", "origin": "redhat", "build_id": "rh:1", "status": "SKIP",'
                '     "misc": {"forced_skip_status": true}},'
                '    {"id": "redhat:2", "origin": "redhat", "build_id": "rh:1", "status": "SKIP"},'
                '    {"id": "redhat:3", "origin": "redhat", "build_id": "rh:1", "status": "PASS"}'
                '  ]'
                '}'
            ]
        }
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertEqual(action, 'report')
        self.assertFalse(submit_retry.called)
        notify.assert_called_with(mock.ANY, 'Detected 1 tests that did not run', True)


@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('cki_tools.pipeline_herder.main.notify_finished', mock.Mock())
@mock.patch.object(utils, 'CONFIG', {'matchers': [{
    'failure_reason': 'data_integrity_failure',
}]})
class TestDataIntegrityMatcher(TestChecker):
    """Test the detection of data_integrity_failure."""

    trace_764828 = ()

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    def test_failure(self, submit_retry: mock.MagicMock):
        """Test the detection."""
        self.job_764828 = copy.deepcopy(self.job_764828)
        self.job_764828['failure_reason'] = 'data_integrity_failure'
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertEqual(action, 'retry')
        self.assertTrue(submit_retry.called)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    def test_no_failure_reason(self, submit_retry: mock.MagicMock):
        """Test that a non-existing failure reason is ok."""
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertIsNone(action)
        self.assertFalse(submit_retry.called)


@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('cki_tools.pipeline_herder.main.notify_finished', mock.Mock())
@mock.patch.object(utils, 'CONFIG', {'matchers': [{
    'failure_reason': 'stuck_or_timeout_failure',
}]})
class TestTimeout(TestChecker):
    """Test the detection of pod resource exhaustion (exit code 137)."""

    trace_764828 = ()

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    def test_timeout(self, submit_retry: mock.MagicMock):
        """Test the detection of stuck jobs."""
        self.job_764828 = copy.deepcopy(self.job_764828)
        self.job_764828['failure_reason'] = 'stuck_or_timeout_failure'
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertEqual(action, 'retry')
        self.assertTrue(submit_retry.called)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    def test_no_failure_reason(self, submit_retry: mock.MagicMock):
        """Test that a non-existing failure reason is ok."""
        self.mock_responses()

        action = main.process_job('https://host/project/-/jobs/764828')
        self.assertIsNone(action)
        self.assertFalse(submit_retry.called)
