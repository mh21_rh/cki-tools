"""KCIDB utils tests."""
import unittest
from unittest import mock

import datawarehouse
import responses

from cki.kcidb import utils

from .utils import mock_attrs

CHECKOUT_ID = "redhat:123"
CHECKOUT_IID = "1"


class TestUtils(unittest.TestCase):
    """Test Job methods."""

    @responses.activate
    def test_patchset_hash(self):
        """Test patchset_hash return values."""
        responses.add(responses.GET, 'https://s/p_1', body=b'p_1')
        responses.add(responses.GET, 'https://s/p_2/', body=b'p_2')

        patch_list = [
            'https://s/p_1', 'https://s/p_2/'
        ]

        self.assertEqual('', utils.patch_list_hash([]))

        self.assertEqual(
            'a560b2642c43bb3494b79d364d9371dd4468307443d1fde7b12f51e7ba4c1b33',
            utils.patch_list_hash([patch_list[0]])
        )
        self.assertEqual(
            '8d2e6de35099fc8e1c9f98f6c107c004f414846d30f6315a23ccbe0d9a664328',
            utils.patch_list_hash(patch_list)
        )

    @responses.activate
    @mock.patch.dict('os.environ', {'DATAWAREHOUSE_URL': 'https://host.url'})
    def test_unknown_issues(self):
        """Verify unknown issues are filtered correctly."""
        dw = datawarehouse.Datawarehouse("https://host.url")
        checkout_url = f"https://host.url/api/1/kcidb/checkouts/{CHECKOUT_ID}"
        checkout_all_url = f"{checkout_url}/all"

        responses.get(checkout_url, json={"id": CHECKOUT_ID, "misc": {"iid": CHECKOUT_IID}})

        cases = {
            "fail": (
                {"tests": [mock_attrs(status="FAIL", waived=False)]},
                1,
            ),
            "error": (
                {"tests": [mock_attrs(status="ERROR", waived=False)]},
                1,
            ),
            "pass": (
                {"tests": [mock_attrs(status="PASS", waived=False)]},
                0,
            ),
            "waived fail": (
                {"tests": [mock_attrs(status="FAIL", waived=True)]},
                0,
            ),
            "waived error": (
                {"tests": [mock_attrs(status="ERROR", waived=True)]},
                0,
            ),
            "triaged test fail": (
                {
                    "tests": [mock_attrs(status="FAIL", waived=False)],
                    "issueoccurrences": [{"is_regression": False, "test_id": "redhat:1"}],
                },
                0,
            ),
            "triaged test error": (
                {
                    "tests": [mock_attrs(status="FAIL", waived=False)],
                    "issueoccurrences": [{"is_regression": False, "test_id": "redhat:1"}],
                },
                0,
            ),
            "triaged test fail with testresult": (
                {
                    "tests": [mock_attrs(status="FAIL", waived=False)],
                    "testresults": [{"id": "redhat:1.1", "test_id": "redhat:1", "status": "FAIL"}],
                    "issueoccurrences": [{"is_regression": False, "test_id": "redhat:1"}],
                },
                1,  # Test triaging does not matter when there are results in play
            ),
            "triaged test error with testresult": (
                {
                    "tests": [mock_attrs(status="ERROR", waived=False)],
                    "testresults": [mock_attrs(id="redhat:2", test_id="redhat:1", status="ERROR")],
                    "issueoccurrences": [{"is_regression": False, "test_id": "redhat:1"}],
                },
                1,
            ),
            "triaged some testresult fail": (
                {
                    "tests": [mock_attrs(status="FAIL", waived=False)],
                    "testresults": [
                        mock_attrs(id="redhat:1.1", test_id="redhat:1", status="FAIL"),
                        mock_attrs(id="redhat:1.2", test_id="redhat:1", status="FAIL"),
                    ],
                    "issueoccurrences": [{"is_regression": False, "testresult_id": "redhat:1.1"}],
                },
                1,  # All results need to be triaged
            ),
            "triaged all testresult fail": (
                {
                    "tests": [mock_attrs(status="FAIL", waived=False)],
                    "testresults": [
                        mock_attrs(id="redhat:1.1", test_id="redhat:1", status="FAIL"),
                        mock_attrs(id="redhat:1.2", test_id="redhat:1", status="FAIL"),
                    ],
                    "issueoccurrences": [
                        {"is_regression": False, "testresult_id": "redhat:1.1"},
                        {"is_regression": False, "testresult_id": "redhat:1.2"},
                    ],
                },
                0,
            ),
            "triaged FAIL testresult and untriaged ERROR testresult": (
                {
                    "tests": [mock_attrs(status="FAIL", waived=False)],
                    "testresults": [
                        mock_attrs(id="redhat:1.1", test_id="redhat:1", status="FAIL"),
                        mock_attrs(id="redhat:1.2", test_id="redhat:1", status="ERROR"),
                    ],
                    "issueoccurrences": [
                        {"is_regression": False, "testresult_id": "redhat:1.1"},
                    ],
                },
                1,
            ),
            "triaged some testresult error": (
                {
                    "tests": [mock_attrs(status="ERROR", waived=False)],
                    "testresults": [
                        mock_attrs(id="redhat:1.1", test_id="redhat:1", status="ERROR"),
                        mock_attrs(id="redhat:1.2", test_id="redhat:1", status="ERROR"),
                    ],
                    "issueoccurrences": [{"is_regression": False, "testresult_id": "redhat:1.1"}],
                },
                1,
            ),
            "triaged all testresult error": (
                {
                    "tests": [mock_attrs(status="ERROR", waived=False)],
                    "testresults": [
                        mock_attrs(id="redhat:1.1", test_id="redhat:1", status="ERROR"),
                        mock_attrs(id="redhat:1.2", test_id="redhat:1", status="ERROR"),
                    ],
                    "issueoccurrences": [
                        {"is_regression": False, "testresult_id": "redhat:1.1"},
                        {"is_regression": False, "testresult_id": "redhat:1.2"},
                    ],
                },
                0,
            ),
            "regression fail": (
                {
                    "tests": [mock_attrs(status="FAIL", waived=False)],
                    "issueoccurrences": [{"is_regression": True, "test_id": "redhat:1"}],
                },
                1,
            ),
            "regression error": (
                {
                    "tests": [mock_attrs(status="ERROR", waived=False)],
                    "issueoccurrences": [{"is_regression": True, "test_id": "redhat:1"}],
                },
                1,
            ),
            "regression testresult fail": (
                {
                    "tests": [mock_attrs(status="FAIL", waived=False)],
                    "testresults": [
                        mock_attrs(id="redhat:1.1", test_id="redhat:1", status="FAIL"),
                    ],
                    "issueoccurrences": [
                        # Triaging directly doesn't override
                        {"is_regression": False, "test_id": "redhat:1"},
                        # the testresult regressions
                        {"is_regression": True, "testresult_id": "redhat:1.1"},
                    ],
                },
                1,
            ),
            "regression testresult error": (
                {
                    "tests": [mock_attrs(status="ERROR", waived=False)],
                    "testresults": [
                        mock_attrs(id="redhat:1.1", test_id="redhat:1", status="ERROR"),
                    ],
                    "issueoccurrences": [
                        # Triaging directly doesn't override
                        {"is_regression": False, "test_id": "redhat:1"},
                        # the testresult regressions
                        {"is_regression": True, "testresult_id": "redhat:1.1"},
                    ],
                },
                1,
            ),
        }

        for description, (data, expected) in cases.items():
            with self.subTest(description):
                responses.upsert(responses.GET, checkout_all_url, json=data)
                dw_all = dw.kcidb.checkouts.get(id=CHECKOUT_ID).all.get()

                result = utils.unknown_issues(dw_all)
                self.assertEqual(len(result), expected, f"{result=}")

            if description == "triaged FAIL testresult and untriaged ERROR testresult":
                with self.subTest("Test status is overridden by the worst untriaged subtest"):
                    self.assertEqual(result[0].status, "ERROR")

        with self.subTest("Passing the optional tests parameter reflects on the outcome"):
            data = {
                "tests": [
                    mock_attrs(id="redhat:1-1", status="FAIL", build_id="redhat:1"),
                    mock_attrs(id="redhat:1-2", status="PASS", build_id="redhat:1"),
                    mock_attrs(id="redhat:2-1", status="FAIL", build_id="redhat:2"),
                    mock_attrs(id="redhat:2-2", status="PASS", build_id="redhat:2"),
                ]
            }
            responses.upsert(responses.GET, checkout_all_url, json=data)
            dw_all = dw.kcidb.checkouts.get(id=CHECKOUT_ID).all.get()

            result = utils.unknown_issues(dw_all)
            self.assertEqual(
                {t.id for t in result},
                {"redhat:1-1", "redhat:2-1"},
                "Payload should return both FAIL",
            )

            filtered_tests = [t for t in dw_all.tests if t.build_id == "redhat:1"]
            filtered_result = utils.unknown_issues(dw_all, tests=filtered_tests)
            self.assertEqual({t.id for t in filtered_result}, {"redhat:1-1"})
