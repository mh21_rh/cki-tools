"""Test cki_tools.aws module."""
import base64
from importlib import resources
import os
import pathlib
import tempfile
import unittest
from unittest import mock

import responses

from cki_tools import aws
from tests.assets import aws as assets


class TestModule(unittest.TestCase):
    """Test cki_tools.aws module."""

    @responses.activate
    def test_login(self) -> None:
        """Test logging in via SAML."""
        assertion = base64.b64encode(
            (resources.files(assets) / 'saml-assertion.xml').read_bytes()).decode('utf8')
        responses.get('https://auth.url', body=(resources.files(assets) / 'saml-response.html')
                      .read_text().replace('ASSERTION', assertion))

        cases = (
            ([], {
                'RoleArn': 'arn:aws:iam::account1:role/account1-role1',
                'PrincipalArn': 'arn:aws:iam::account1:saml-provider/provider',
            }),
            (['--account', 'account2'], {
                'RoleArn': 'arn:aws:iam::account2:role/account2-role1',
                'PrincipalArn': 'arn:aws:iam::account2:saml-provider/provider',
            }),
            (['--role', 'role2'], {
                'RoleArn': 'arn:aws:iam::account3:role/account3-role2',
                'PrincipalArn': 'arn:aws:iam::account3:saml-provider/provider',
            }),
            (['--duration', '1234'], {
                'RoleArn': 'arn:aws:iam::account1:role/account1-role1',
                'PrincipalArn': 'arn:aws:iam::account1:saml-provider/provider',
                'DurationSeconds': 1234,
            }),
        )
        for args, expected_args in cases:
            with (self.subTest(args),
                  tempfile.NamedTemporaryFile() as conf_file,
                  mock.patch('boto3.client') as client,
                  mock.patch.dict(os.environ, {'AWS_IDP_URL': 'https://auth.url',
                                               'AWS_SHARED_CREDENTIALS_FILE': conf_file.name})):
                client.return_value.assume_role_with_saml.return_value = {'Credentials': {
                    'AccessKeyId': 'access_key_id',
                    'SecretAccessKey': 'secret_access_key',
                    'SessionToken': 'session_token',
                }}

                aws.main(['login', '--profile', 'saml'] + args)

                client.return_value.assume_role_with_saml.assert_called_with(
                    SAMLAssertion=assertion, **expected_args)
                self.assertEqual(pathlib.Path(conf_file.name).read_text('utf8'), (
                    '[saml]\n'
                    'aws_access_key_id = access_key_id\n'
                    'aws_secret_access_key = secret_access_key\n'
                    'aws_session_token = session_token\n\n'
                ))
