"""Test the kcidb forwarder."""
import copy
import unittest
from unittest import mock

from cki_lib import misc
from cki_lib.kcidb import CONSUMER_KCIDB_SCHEMA
from cki_lib.kcidb import PRODUCER_KCIDB_SCHEMA

from cki_tools import datawarehouse_kcidb_forwarder
from cki_tools.datawarehouse_kcidb_forwarder import LOGGER
from tests.utils import tear_down_registry

tear_down_registry()

DW_KCIDB_SCHEMA_VERSION = {'major': PRODUCER_KCIDB_SCHEMA.major,
                           'minor': PRODUCER_KCIDB_SCHEMA.minor}


class TestKCIDBForwarder(unittest.TestCase):
    """Test the kcidb forwarder."""

    def test_callback_submit(self):
        """Test that messages are submitted."""
        message = datawarehouse_kcidb_forwarder.KCIDBMessage()
        message.submit = mock.Mock()
        message.add = mock.Mock()

        message.checkouts = ['foo']
        message.callback()
        self.assertTrue(message.submit.called)
        self.assertFalse(message.add.called)

    def test_callback_add(self):
        """Test that messages are added to the list."""
        message_ok = {
            "object_type": "test",
            "status": "new",
            "object": {
                "id": "redhat:1",
                "origin": "redhat",
                "misc": {
                    "is_public": True,
                    "kcidb": {"version": DW_KCIDB_SCHEMA_VERSION},
                },
            },
        }
        message = datawarehouse_kcidb_forwarder.KCIDBMessage()
        message.submit = mock.Mock()
        message.add = mock.Mock()

        ack_fn = mock.Mock()

        with self.subTest("Message has status != new"):
            message_not_new = copy.deepcopy(message_ok)
            message_not_new["status"] = "crowbar"
            with self.assertLogs(LOGGER, "INFO") as log_ctx:
                message.callback(message_not_new, ack_fn)
            expected_log = (
                f"INFO:{LOGGER.name}:"
                'Message status "crowbar" is not "new" or "updated", skipping'
            )
            self.assertIn(expected_log, log_ctx.output)
            message.add.assert_not_called()
            ack_fn.assert_called_once()
        ack_fn.reset_mock()

        with self.subTest("Message has is_public == False"):
            message_private = copy.deepcopy(message_ok)
            message_private["object"]["misc"]["is_public"] = False
            with self.assertLogs(LOGGER, "INFO") as log_ctx:
                message.callback(message_private, ack_fn)
            expected_log = f"INFO:{LOGGER.name}:Object is not public, skipping"
            self.assertIn(expected_log, log_ctx.output)
            message.add.assert_not_called()
            ack_fn.assert_called_once()
        ack_fn.reset_mock()

        with self.subTest("Message has retrigger == True"):
            message_private = copy.deepcopy(message_ok)
            message_private["object"]["misc"]["retrigger"] = True
            with self.assertLogs(LOGGER, "INFO") as log_ctx:
                message.callback(message_private, ack_fn)
            expected_log = f"INFO:{LOGGER.name}:Object is not public, skipping"
            self.assertIn(expected_log, log_ctx.output)
            message.add.assert_not_called()
            ack_fn.assert_called_once()
        ack_fn.reset_mock()

        with self.subTest("Message has source_package_name !~ kernel"):
            message_source_package_name = copy.deepcopy(message_ok)
            message_source_package_name["object_type"] = "checkout"
            message_source_package_name["object"]["misc"]["source_package_name"] = "pipewire"
            with self.assertLogs(LOGGER, "INFO") as log_ctx:
                message.callback(message_source_package_name, ack_fn)
            expected_log = (
                f"INFO:{LOGGER.name}:Message is not about the kernel, skipping"
            )
            self.assertIn(expected_log, log_ctx.output)
            message.add.assert_not_called()
            ack_fn.assert_called_once()
        ack_fn.reset_mock()

        with self.subTest("Message has has origin != redhat"):
            message_origin = copy.deepcopy(message_ok)
            message_origin["object"]["origin"] = "foo"
            with self.assertLogs(LOGGER, "INFO") as log_ctx:
                message.callback(message_origin, ack_fn)
            expected_log = f"INFO:{LOGGER.name}:Object not created by CKI, skipping"
            message.add.assert_not_called()
            ack_fn.assert_called_once()
        ack_fn.reset_mock()

        with self.subTest("Message has no KCIDB version"):
            message_missing_version = copy.deepcopy(message_ok)
            misc.set_nested_key(message_missing_version, "object/misc/kcidb/version", None)
            with self.assertLogs(LOGGER, "INFO") as log_ctx:
                message.callback(message_missing_version, ack_fn)
            expected_log = (
                f"INFO:{LOGGER.name}:"
                "Message is missing the KCIDB version, skipping"
            )
            self.assertIn(expected_log, log_ctx.output)
            message.add.assert_not_called()
            ack_fn.assert_called_once()
        ack_fn.reset_mock()

        with self.subTest("Message has scratch == True"):
            message_scratch = copy.deepcopy(message_ok)
            message_scratch["object_type"] = "checkout"
            message_scratch["object"]["misc"]["scratch"] = True
            with self.assertLogs(LOGGER, "INFO") as log_ctx:
                message.callback(message_scratch, ack_fn)
            expected_log = f"INFO:{LOGGER.name}:Object is just a scratch, skipping"
            self.assertIn(expected_log, log_ctx.output)
            message.add.assert_not_called()
            ack_fn.assert_called_once()
        ack_fn.reset_mock()

        with self.subTest("Message is ok"):
            message.callback(message_ok, ack_fn)
            message.add.assert_called_once_with(
                "test",
                {
                    "origin": "redhat",
                    "id": "redhat:1",
                    "misc": {
                        "is_public": True,
                        "kcidb": {"version": DW_KCIDB_SCHEMA_VERSION},
                    },
                },
                ack_fn,
            )
            message.submit.assert_not_called()
            # This message is acked when submitted.
            ack_fn.assert_not_called()

    def test_kcidbmessage_add_valid(self):
        """Test KCIDBMessage.add() with valid inputs."""
        ack_fn = mock.Mock()

        cases = [
            ("checkout", "checkouts"),
            ("build", "builds"),
            ("test", "tests"),
            ("issue", "issues"),
            ("incident", "incidents"),
        ]
        for obj_type, obj_list_name in cases:
            message = datawarehouse_kcidb_forwarder.KCIDBMessage()
            self.assertEqual(getattr(message, obj_list_name), [])

            with self.subTest("Adding in an empty list", obj_type=obj_type):
                message.add(obj_type, {"id": f"r:{obj_type}_1"}, ack_fn)

                self.assertEqual(getattr(message, obj_list_name), [{"id": f"r:{obj_type}_1"}])
                self.assertEqual(message.msg_ack_callbacks, [ack_fn])

            with self.subTest("Adding in a list with one object", obj_type=obj_type):
                message.add(obj_type, {"id": f"r:{obj_type}_2"}, ack_fn)

                self.assertEqual(
                    getattr(message, obj_list_name),
                    [{"id": f"r:{obj_type}_1"}, {"id": f"r:{obj_type}_2"}],
                )
                self.assertEqual(message.msg_ack_callbacks, [ack_fn, ack_fn])

    def test_kcidbmessage_add_invalid(self):
        """Test KCIDBMessage.add() with unhandled input."""
        message = datawarehouse_kcidb_forwarder.KCIDBMessage()
        ack_fn = mock.Mock()

        with self.assertRaisesRegex(KeyError, expected_regex=r"invalid object type"):
            message.add("invalid object type", {'foo': 'bar'}, ack_fn)

        ack_fn.assert_not_called()

    @mock.patch(
        "cki_lib.kcidb.CONSUMER_KCIDB_SCHEMA.validate",
        wraps=CONSUMER_KCIDB_SCHEMA.validate,
    )
    def test_kcidbmessage_callback_encoded(self, validate):
        """Test KCIDBMessage.callback() with a realistic payload results in the expected encoded."""
        ack_fn = mock.Mock()

        checkout_obj = {
            "origin": "redhat",
            "id": "redhat:92b8f402aa964f209772e30190af5de818af996c",
            "misc": {
                "is_public": True,
                "kcidb": {"version": DW_KCIDB_SCHEMA_VERSION},
                "source_package_name": "kernel",
            },
        }
        checkout = {'object_type': 'checkout', 'status': 'updated', 'object': checkout_obj}
        build_obj = {
            "checkout_id": "redhat:92b8f402aa964f209772e30190af5de818af996c",
            "origin": "redhat",
            "id": "redhat:802",
            "misc": {"is_public": True, "kcidb": {"version": DW_KCIDB_SCHEMA_VERSION}},
        }
        build = {'object_type': 'build', 'status': 'updated', 'object': build_obj}
        test_obj = {
            "build_id": "redhat:802",
            "origin": "redhat",
            "id": "redhat:1337",
            "misc": {"is_public": True, "kcidb": {"version": DW_KCIDB_SCHEMA_VERSION}},
        }
        test = {'object_type': 'test', 'status': 'updated', 'object': test_obj}
        issue_obj = {
            "origin": "redhat",
            "id": "redhat:123",
            "version": 1609463420,
            "comment": "[kernel-rt 9.x] vsnprintf calls crng_make_state with IRQs disabled",
            "misc": {"is_public": True, "kcidb": {"version": DW_KCIDB_SCHEMA_VERSION}},
        }
        issue = {'object_type': 'issue', 'status': 'updated', 'object': issue_obj}
        incident_obj = {
            "origin": "redhat",
            "id": "redhat:007",
            "issue_id": "redhat:123",
            "issue_version": 1609463420,
            "test_id": "test:1337",
            "present": True,
            "misc": {"is_public": True, "kcidb": {"version": DW_KCIDB_SCHEMA_VERSION}},
        }
        incident = {'object_type': 'incident', 'status': 'updated', 'object': incident_obj}

        expected_msg = {
            "version": DW_KCIDB_SCHEMA_VERSION,
            "checkouts": [copy.deepcopy(checkout_obj)],
            "builds": [copy.deepcopy(build_obj)],
            "tests": [copy.deepcopy(test_obj)],
            "issues": [copy.deepcopy(issue_obj)],
            "incidents": [copy.deepcopy(incident_obj)],
        }
        # Set misc for each object taking into account the cleaning from KCIDBMessage.add()
        misc.set_nested_key(expected_msg, "checkouts/0/misc", {"source_package_name": "kernel"})
        misc.set_nested_key(expected_msg, "builds/0/misc", {})
        misc.set_nested_key(expected_msg, "tests/0/misc", {})
        misc.set_nested_key(expected_msg, "issues/0/misc", {})
        misc.set_nested_key(expected_msg, "incidents/0/misc", {})

        message = datawarehouse_kcidb_forwarder.KCIDBMessage()
        message.callback(checkout, ack_fn)
        message.callback(build, ack_fn)
        message.callback(test, ack_fn)
        message.callback(issue, ack_fn)
        message.callback(incident, ack_fn)

        self.assertDictEqual(expected_msg, message.encoded)
        validate.assert_called_with(expected_msg)

        ack_fn.assert_not_called()

    def test_kcidbmessage_len(self):
        """Test KCIDBMessage length property."""
        message = datawarehouse_kcidb_forwarder.KCIDBMessage()
        self.assertEqual(0, len(message))

        message.checkouts = [{}] * 1
        message.builds = [{}] * 2
        message.tests = [{}] * 4
        message.issues = [{}] * 8
        message.incidents = [{}] * 16
        self.assertEqual(31, len(message))

    def test_kcidbmessage_clear(self):
        """Test KCIDBMessage clear method."""

        message = datawarehouse_kcidb_forwarder.KCIDBMessage()
        message.version = DW_KCIDB_SCHEMA_VERSION
        message.checkouts.append({"id": "redhat:checkout_1"})
        message.builds.append({"id": "redhat:build_1"})
        message.tests.append({"id": "redhat:test_1"})
        message.issues.append({"id": "redhat:issue_1"})
        message.incidents.append({"id": "redhat:incident_1"})
        message.msg_ack_callbacks.append(mock.Mock())

        properties = (
            'checkouts',
            'builds',
            'tests',
            'issues',
            'incidents',
            'msg_ack_callbacks',
        )

        for prop_name in properties:
            prop = getattr(message, prop_name)
            self.assertEqual(1, len(prop), prop_name)

        message.clear()

        for prop_name in properties:
            prop = getattr(message, prop_name)
            self.assertEqual(0, len(prop), prop_name)
        self.assertIsNone(message.version)

    def test_kcidbmessage_submit(self):
        """Test KCIDBMessage submit method ."""
        for is_production in [True, False]:
            with (
                self.assertLogs(LOGGER, "INFO") as log_ctx,
                self.subTest(is_production=is_production),
                mock.patch("cki_lib.misc.is_production", mock.Mock(return_value=is_production)),
            ):
                message = datawarehouse_kcidb_forwarder.KCIDBMessage()
                message.version = DW_KCIDB_SCHEMA_VERSION
                message.kcidb_client = mock.Mock()
                message.clear = mock.Mock()
                message.ack_messages = mock.Mock()

                message.submit()

                if is_production:
                    message.kcidb_client.submit.assert_called_with(message.encoded)
                else:
                    message.kcidb_client.submit.assert_not_called()
                    expected_log = (
                        f"INFO:{LOGGER.name}:"
                        "production mode would submit 0 elements"
                    )
                    self.assertIn(expected_log, log_ctx.output)

                message.clear.assert_called_once()
                message.ack_messages.assert_called_once()

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch.dict('os.environ', {'MAX_BATCH_SIZE': '0'})
    def test_kcidbmessage_submit_acks_invalid_kcidb_payload(self):
        """Test KCIDBMessage submit acks invalid kcidb bodies logging their error"""
        message_ok = {
            'object_type': 'test',
            'status': 'new',
            'id': 1,
            'iid': 1,
            'object': {
                'origin': 'redhat',
                'status': 'MISS',  # MISS was introduced by KCIDBv4.2
                'misc': {
                    'is_public': True,
                    'kcidb': {'version': {'major': 4, 'minor': 1}},
                }
            },
        }
        message = datawarehouse_kcidb_forwarder.KCIDBMessage()
        ack_fn = mock.Mock()

        with self.assertLogs(LOGGER, "ERROR") as log_ctx:
            message.callback(message_ok, ack_fn)

        self.assertTrue(
            log_ctx.output[0].startswith(
                f"ERROR:{LOGGER.name}:"
                "Tried to submit invalid KCIDB data upstream. The message will be acked."
            )
        )
        ack_fn.assert_called_once()

    def test_kcidbmessage_add_set_version(self):
        """Test KCIDBMessage add sets message version."""
        message = datawarehouse_kcidb_forwarder.KCIDBMessage()

        message.add(
            'checkout', {'misc': {'kcidb': {
                'version': DW_KCIDB_SCHEMA_VERSION}}}, None)
        self.assertDictEqual(
            DW_KCIDB_SCHEMA_VERSION,
            message.version,
        )

        # New message does not replace it.
        message.add(
            'checkout', {'misc': {'kcidb': {
                'version': {'major': 4, 'minor': 1}}}}, None)
        self.assertDictEqual(
            DW_KCIDB_SCHEMA_VERSION,
            message.version,
        )

        # New message replaces it only after clear.
        message.clear()
        message.add(
            'checkout', {'misc': {'kcidb': {
                'version': {'major': 4, 'minor': 1}}}}, None)
        self.assertDictEqual(
            {'major': 4, 'minor': 1},
            message.version,
        )

    def test_ack_messages(self):
        """Test ack_messages does what is expected to do."""
        message = datawarehouse_kcidb_forwarder.KCIDBMessage()
        message.msg_ack_callbacks = [
            mock.Mock(),
            mock.Mock()
        ]
        message.ack_messages()
        for ack_fn in message.msg_ack_callbacks:
            self.assertTrue(ack_fn.called)

    def test_add_remove_misc(self):
        """Test that the some keys are removed from misc."""
        message = datawarehouse_kcidb_forwarder.KCIDBMessage()
        checkouts = [
            {'foo': 'bar', 'misc': {'bar': 'bar', 'kcidb': {'foo': 'foo'}}},
            {'foo': 'bar', 'misc': {'bar': 'bar', 'is_public': True}},
            {'foo': 'bar', 'misc': {
                'bar': 'bar', 'is_public': False, 'kcidb': {'foo': 'foo'}}},
        ]

        for rev in checkouts:
            message.add('checkout', rev, None)

        self.assertListEqual(
            [
                {'foo': 'bar', 'misc': {'bar': 'bar'}},
                {'foo': 'bar', 'misc': {'bar': 'bar'}},
                {'foo': 'bar', 'misc': {'bar': 'bar'}},
            ],
            message.checkouts
        )

    def test_sanitize_tests(self):
        """Test _sanitize_test."""
        message = datawarehouse_kcidb_forwarder.KCIDBMessage()
        message.version = DW_KCIDB_SCHEMA_VERSION
        message.tests = [
            {'path': 'foobar'},
            {'path': 'foo bar'},
            {'path': ' foo bar'},
            {'path': 'foo   bar'},
        ]

        message._sanitize_test()  # pylint: disable=protected-access

        for test in message.tests:
            self.assertNotIn(' ', test['path'])

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=False))
    def test_callback_submit_on_many_messages(self):
        """Test KCIDBMessage add calls submit when there are many queued messages."""
        message_ok = {
            "object_type": "test",
            "status": "new",
            "object": {
                "origin": "redhat",
                "id": "redhat:1",
                "foo": "bar",
                "misc": {
                    "is_public": True,
                    "kcidb": {"version": DW_KCIDB_SCHEMA_VERSION},
                },
            },
        }
        message = datawarehouse_kcidb_forwarder.KCIDBMessage()
        message.submit = mock.Mock()
        message.builds = [{'id': i} for i in range(message.max_batch_size)]

        ack_fn = mock.Mock()
        message.callback(message_ok, ack_fn)
        self.assertTrue(message.submit.called)

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=False))
    def test_callback_delete_git_commit_hash(self):
        """Test git_commit_hash is deleted if no git_repository_url is present."""
        missing_repo_url = {
            "object_type": "checkout",
            "status": "new",
            "object": {
                "origin": "redhat",
                "id": "redhat:1",
                "git_commit_hash": "hash",
                "misc": {
                    "is_public": True,
                    "kcidb": {"version": DW_KCIDB_SCHEMA_VERSION},
                },
            },
        }
        message = datawarehouse_kcidb_forwarder.KCIDBMessage()
        message.add = mock.Mock()
        ack_fn = mock.Mock()

        with self.subTest("git_commit_hash is deleted when missing git_repository_url"):
            message.callback(copy.deepcopy(missing_repo_url), ack_fn)
            message.add.assert_called_with(
                "checkout",
                {
                    "origin": "redhat",
                    "id": "redhat:1",
                    "misc": {
                        "is_public": True,
                        "kcidb": {"version": DW_KCIDB_SCHEMA_VERSION},
                    },
                },
                mock.ANY,
            )

        with self.subTest("git_commit_hash remains when there's git_repository_url"):
            message_ok = copy.deepcopy(missing_repo_url)
            misc.set_nested_key(message_ok, "object/git_repository_url", "https://url")
            message.callback(message_ok, ack_fn)
            message.add.assert_called_with(
                "checkout",
                {
                    "origin": "redhat",
                    "id": "redhat:1",
                    "git_commit_hash": "hash",
                    "git_repository_url": "https://url",
                    "misc": {
                        "is_public": True,
                        "kcidb": {"version": DW_KCIDB_SCHEMA_VERSION},
                    },
                },
                mock.ANY,
            )
