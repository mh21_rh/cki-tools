"""Tests for cki_tools.gitrepo_trigger."""
import os
import unittest
from unittest import mock

from cki_lib import config_tree
from cki_lib.gitlab import get_instance
import freezegun
import responses

from cki_tools import gitrepo_trigger
from tests import fakes


class TestLoadTriggers(unittest.TestCase):
    """Tests for gitrepo_trigger.get_triggers()."""

    @freezegun.freeze_time("2019-01-01")
    @mock.patch('cki_tools.gitrepo_trigger.get_commit_hash',
                mock.Mock(return_value='current_baseline'))
    def test_trigger(self):
        cases = (
            ('pipeline project', {'cki_project': 'username/project'},
             False, False, False, {}, [{}]),
            ('parent project', {'cki_pipeline_project': 'project'}, False, False, False, {
                'GITLAB_PARENT_PROJECT': 'username'
            }, [{}]),
            ('was tested', {'cki_project': 'username/project'}, True, False, False, {}, []),
            ('too soon', {
                'cki_project': 'username/project',
                'minimum_interval': '6h',
            }, False, True, False, {}, []),
            ('too many', {
                'cki_project': 'username/project',
                'concurrency_limit': 1,
            }, False, False, True, {}, []),
            ('variant kernel', {
                'cki_project': 'username/project',
                'package_name': 'kernel-debug',
            }, False, False, False, {}, [{
                'package_name': 'kernel-debug', 'title': 'Baseline - '
                'http://git.test/git/kernel.git - main - current_base - kernel-debug',
            }]),
        )

        for (description, trigger_config, was_tested, too_soon, too_many, environ,
             expected_triggers) in cases:
            with (self.subTest(description),
                  mock.patch.dict(os.environ, environ),
                  mock.patch('cki_tools.gitrepo_trigger.was_tested',
                             mock.Mock(return_value=was_tested)),
                  mock.patch('cki_tools.gitrepo_trigger.too_soon',
                             mock.Mock(return_value=too_soon)),
                  mock.patch('cki_tools.gitrepo_trigger.too_many',
                             mock.Mock(return_value=too_many))):
                config = config_tree.process_config_tree({'test_name': {
                    'git_url': 'http://git.test/git/kernel.git',
                    '.branches': ['main'],
                    'cki_pipeline_branch': 'test_name',
                    '.report_rules': [{'if': 'something',
                                       'send_cc': 'someone',
                                       'send_bcc': 'someone_else'}],
                    **trigger_config,
                }})

                triggers = list(gitrepo_trigger.get_triggers(mock.Mock(), config))

                self.assertEqual(triggers, [{
                    'git_url': 'http://git.test/git/kernel.git',
                    'watch_url': 'http://git.test/git/kernel.git',
                    'commit_hash': 'current_baseline',
                    'watched_repo_commit_hash': 'current_baseline',
                    'branch': 'main',
                    'watch_branch': 'main',
                    'cki_pipeline_branch': 'test_name',
                    'cki_project': 'username/project',
                    'title': 'Baseline - http://git.test/git/kernel.git - main - current_base',
                    'report_rules': (
                        '[{"if": "something", "send_cc": "someone", "send_bcc": "someone_else"}]'
                    ),
                    **e,
                } for e in expected_triggers])


class TestCheckForTested(unittest.TestCase):
    """Tests for gitrepo_trigger.was_tested()."""

    @mock.patch('cki_lib.gitlab.get_variables')
    def test_was_tested_true(self, mock_variables):
        """Check was_tested returns True if the commit was already tested."""
        project = fakes.FakeGitLabProject()
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})
        project.trigger_pipeline('cki_2', 'token', {'ref': 'cki_2'})

        mock_variables.return_value = {'watch_url': 'git_url',
                                       'ref': 'sha',
                                       'watched_repo_commit_hash': 'sha',
                                       'watch_branch': 'git_branch'}

        self.assertTrue(gitrepo_trigger.was_tested(project, 'cki_2', 'sha', {
            'watch_url': 'git_url', 'watch_branch': 'git_branch', 'package_name': None}))
        self.assertEqual(mock_variables.call_count, 2)  # found

    @mock.patch('cki_lib.gitlab.get_variables')
    def test_was_tested_other_commit(self, mock_variables):
        """Check was_tested returns False if the pipeline didn't run."""
        project = fakes.FakeGitLabProject()
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})

        mock_variables.return_value = {'watch_url': 'git_url',
                                       'ref': 'sha',
                                       'watch_url_commit_hash': 'different-sha',
                                       'watch_branch': 'git_branch'}

        self.assertFalse(gitrepo_trigger.was_tested(project, 'cki_branch', 'sha', {
            'watch_url': 'git_url', 'watch_branch': 'git_branch', 'package_name': None}))
        self.assertEqual(mock_variables.call_count, 2)  # short-circuited

    @mock.patch('cki_lib.gitlab.get_variables')
    def test_was_tested_no_commit(self, mock_variables):
        """Check was_tested returns False if the pipeline didn't run."""
        project = fakes.FakeGitLabProject()
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})

        mock_variables.return_value = {'watch_url': 'git_url',
                                       'ref': 'sha',
                                       'watched_repo_commit_hash': 'sha',
                                       'watch_branch': 'different_git_branch'}

        self.assertFalse(gitrepo_trigger.was_tested(project, 'cki_branch', 'sha', {
            'watch_url': 'git_url', 'watch_branch': 'git_branch', 'package_name': None}))
        self.assertEqual(mock_variables.call_count, 3)

    @mock.patch('cki_lib.gitlab.get_variables')
    def test_was_tested_retriggered(self, mock_variables):
        """Check that was_tested skips retriggered pipelines."""
        project = fakes.FakeGitLabProject()
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})

        mock_variables.return_value = {
            "CKI_RETRIGGER_PIPELINE": "true",
            "CKI_DEPLOYMENT_ENVIRONMENT": "staging",
            "watch_url": "git_url",
            "ref": "sha",
            "watched_repo_commit_hash": "sha",
            "watch_branch": "git_branch",
        }

        self.assertFalse(gitrepo_trigger.was_tested(project, 'cki_branch', 'sha', {
            'watch_url': 'git_url', 'watch_branch': 'git_branch', 'package_name': None}))
        self.assertEqual(mock_variables.call_count, 3)

    @mock.patch('cki_lib.gitlab.get_variables')
    def test_was_tested_no_jobs(self, mock_variables):
        """Check was_tested returns True if the commit was already tested."""
        project = fakes.FakeGitLabProject()
        project.trigger_pipeline('cki_2', 'token', {'ref': 'cki_2'})
        project.pipelines[0].jobs.clear()

        mock_variables.return_value = {'watch_url': 'git_url',
                                       'ref': 'sha',
                                       'watched_repo_commit_hash': 'sha',
                                       'watch_branch': 'git_branch'}

        self.assertFalse(gitrepo_trigger.was_tested(project, 'cki_2', 'sha', {
            'watch_url': 'git_url', 'watch_branch': 'git_branch', 'package_name': None}))

    @responses.activate
    @freezegun.freeze_time('2019-01-02T00:00:00Z')
    def test_too_soon(self):
        responses.add(
            responses.GET,
            'https://gitlab.com/api/v4/projects/1/pipelines?'
            'ref=branch&per_page=100',
            json=[{'id': 103, 'created_at': '2019-01-01T00:00:00Z'}])
        responses.add(
            responses.GET,
            'https://gitlab.com/api/v4/projects/1/pipelines/103/variables',
            json=[])

        gl_project = get_instance('https://gitlab.com').projects.get(1, lazy=True)

        cases = (
            ('old enough', '6h', False),
            ('too new', '30h', True),
        )
        for description, minimum_interval, expected in cases:
            with self.subTest(description):
                self.assertEqual(gitrepo_trigger.too_soon(
                    gl_project, 'branch', minimum_interval, {}), expected)

    @responses.activate
    def test_too_many(self):
        responses.add(
            responses.GET,
            'https://gitlab.com/api/v4/projects/1/pipelines?'
            'ref=branch&scope=running&per_page=100',
            json=[{'id': 103}, {'id': 102}])
        responses.add(
            responses.GET,
            'https://gitlab.com/api/v4/projects/1/pipelines/103/variables',
            json=[])
        responses.add(
            responses.GET,
            'https://gitlab.com/api/v4/projects/1/pipelines/102/variables',
            json=[])

        gl_project = get_instance('https://gitlab.com').projects.get(1, lazy=True)

        cases = (
            ('too many', 1, True),
            ('on the limit', 2, True),
            ('more possible', 3, False),
        )
        for description, concurrency_limit, expected in cases:
            with self.subTest(description):
                self.assertEqual(gitrepo_trigger.too_many(
                    gl_project, 'branch', concurrency_limit, {}), expected)


class TestGetCommitHash(unittest.TestCase):
    """Tests for gitrepo_trigger.get_commit_hash()."""

    @mock.patch('subprocess.check_output')
    def test_commit_hash(self, mock_check_output):
        """Verify get_commit_hash returns the right value from git output."""
        mock_check_output.return_value = 'hash\t\trefs/heads/main'.encode()
        self.assertEqual(gitrepo_trigger.get_commit_hash('repo', 'refs/heads/main'),
                         'hash')
