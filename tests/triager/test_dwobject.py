"""Test dwobject.py."""
import unittest
from unittest import mock

from datawarehouse import Datawarehouse
from datawarehouse import objects
import responses

from cki.triager import dwobject

from ..utils import mock_attrs


class TestDWObject(unittest.TestCase):
    """Test DWObject."""

    def setUp(self):
        self.data = {
            "checkouts": [
                {
                    "origin": "redhat",
                    "id": "redhat:1",
                    "valid": True,
                },
                {
                    "origin": "redhat",
                    "id": "redhat:2",
                    "valid": True,
                },
                {
                    "origin": "redhat",
                    "id": "redhat:3",
                    "valid": False,
                },
            ],
            "builds": [
                {
                    "origin": "redhat",
                    "checkout_id": "redhat:1",
                    "id": "redhat:1_x86_64",
                    "valid": True,
                },
                {
                    "origin": "redhat",
                    "checkout_id": "redhat:1",
                    "id": "redhat:1_aarch64",
                    "valid": True,
                },
                {
                    "origin": "redhat",
                    "checkout_id": "redhat:1",
                    "id": "redhat:1_riscv64",
                    "valid": False,
                },
                {
                    "origin": "redhat",
                    "checkout_id": "redhat:2",
                    "id": "redhat:2_x86_64",
                    "valid": True,
                },
                {
                    "origin": "redhat",
                    "checkout_id": "redhat:2",
                    "id": "redhat:2_aarch64",
                    "valid": True,
                },
            ],
            "tests": [
                {
                    "origin": "redhat",
                    "build_id": "redhat:1_x86_64",
                    "id": "redhat:1_x86_64_upt1",
                    "status": "FAIL",
                },
                {
                    "origin": "redhat",
                    "build_id": "redhat:1_x86_64",
                    "id": "redhat:1_x86_64_upt2",
                    "status": "ERROR",
                },
                {
                    "origin": "redhat",
                    "build_id": "redhat:1_aarch64",
                    "id": "redhat:1_aarch64_upt1",
                    "status": "MISS",
                },
                {
                    "origin": "redhat",
                    "build_id": "redhat:1_aarch64",
                    "id": "redhat:1_aarch64_upt2",
                    "status": "SKIP",
                },
                {
                    "origin": "redhat",
                    "build_id": "redhat:2_x86_64",
                    "id": "redhat:2_x86_64_upt1",
                    "status": "PASS",
                },
                {
                    "origin": "redhat",
                    "build_id": "redhat:2_x86_64",
                    "id": "redhat:2_x86_64_upt2",
                    "status": "DONE",
                },
                {
                    "origin": "redhat",
                    "build_id": "redhat:2_aarch64",
                    "id": "redhat:2_aarch64_upt1",
                    "status": "FAIL",
                },
                {
                    "origin": "redhat",
                    "build_id": "redhat:2_aarch64",
                    "id": "redhat:2_aarch64_upt2",
                    "status": None,
                },
            ],
            "testresults": [
                {
                    "origin": "redhat",
                    "test_id": "redhat:1_x86_64_upt1",
                    "id": "redhat:1_x86_64_upt1_1",
                    "status": "FAIL",
                },
                {
                    "origin": "redhat",
                    "test_id": "redhat:1_x86_64_upt1",
                    "id": "redhat:1_x86_64_upt1_2",
                    "status": "FAIL",
                },
                {
                    "origin": "redhat",
                    "test_id": "redhat:1_x86_64_upt2",
                    "id": "redhat:1_x86_64_upt2_1",
                    "status": "ERROR",
                },
                {
                    "origin": "redhat",
                    "test_id": "redhat:1_aarch64_upt1",
                    "id": "redhat:1_aarch64_upt1_1",
                    "status": "MISS",
                },
                {
                    "origin": "redhat",
                    "test_id": "redhat:1_aarch64_upt2",
                    "id": "redhat:1_aarch64_upt2_1",
                    "status": "SKIP",
                },
                {
                    "origin": "redhat",
                    "test_id": "redhat:2_x86_64_upt1",
                    "id": "redhat:2_x86_64_upt1_1",
                    "status": "PASS",
                },
                {
                    "origin": "redhat",
                    "test_id": "redhat:2_x86_64_upt2",
                    "id": "redhat:2_x86_64_upt2_1",
                    "status": "DONE",
                },
            ],
            "issueoccurrences": [
                {
                    "issue": {"id": 100001},
                    "checkout_id": "redhat:3",
                    "build_id": None,
                    "test_id": None,
                    "testresult_id": None,
                },
                {
                    "issue": {"id": 100002},
                    "checkout_id": None,
                    "build_id": "redhat:1_riscv64",
                    "test_id": None,
                    "testresult_id": None,
                },
                {
                    "issue": {"id": 100003},
                    "checkout_id": None,
                    "build_id": None,
                    "test_id": "redhat:2_aarch64_upt1",
                    "testresult_id": None,
                },
                {
                    "issue": {"id": 100004},
                    "checkout_id": None,
                    "build_id": None,
                    "test_id": None,
                    "testresult_id": "redhat:1_x86_64_upt1_1",
                },
            ],
        }

    def test_from_attrs(self):
        """Test constructor from_attrs fills attributes as expected."""
        obj = dwobject.from_attrs('checkout', {
            "id": "redhat:1",
            "origin": "redhat",
            "valid": True,
            "misc": {"iid": 1},
            'a': 1,
        })

        self.assertEqual(1, obj.a)
        self.assertEqual("redhat:1", obj.id)
        self.assertEqual("redhat", obj.origin)
        self.assertEqual(True, obj.valid)
        self.assertEqual({"iid": 1}, obj.misc)
        self.assertIsInstance(obj, objects.KCIDBCheckout)

    @responses.activate
    @mock.patch("cki.triager.settings.DW_CLIENT.kcidb.checkouts.get",
                Datawarehouse('http://datawarehouse').kcidb.checkouts.get)
    def test_from_obj_id(self):
        """Test constructor from_obj_id fills fetches from DW as expected."""
        responses.add(
            responses.GET,
            'http://datawarehouse/api/1/kcidb/checkouts/1',
            json={
                "id": "redhat:1",
                "origin": "redhat",
                "valid": True,
                "misc": {"iid": 1},
            })

        obj = dwobject.from_obj_id('checkout', 1)

        self.assertEqual("redhat:1", obj.id)
        self.assertEqual("redhat", obj.origin)
        self.assertEqual(True, obj.valid)
        self.assertEqual({"iid": 1}, obj.misc)
        self.assertIsInstance(obj, objects.KCIDBCheckout)

        with self.assertRaises(KeyError):
            dwobject.from_obj_id('invalid', 1)

    @mock.patch('cki.triager.dwobject.from_attrs')
    def test_check_with_dict(self, mocked_constructor):
        """Test Triager.check initializes the DWObject as expected when given a dict.

        Most of the behavior is mocked in here, because it's already tested elsewhere.
        """
        mocked_attrs = mock_attrs()

        mocked_constructor.return_value.finished = False

        types = ["checkout", "build", "test", "testresult"]
        for obj_type in types:
            with self.subTest(obj_type=obj_type):
                mocked_constructor.reset_mock()
                dwobject.from_attrs(obj_type, attrs=mocked_attrs)
                mocked_constructor.assert_called_once_with(obj_type, attrs=mocked_attrs)

    @mock.patch('cki.triager.dwobject.from_obj_id')
    def test_check_with_object_id(self, mocked_constructor):
        """Test Triager.check initializes the DWObject as expected when given an ID.

        Most of the behavior is mocked in here, because it's already tested elsewhere.
        """
        mocked_attrs = "1"

        mocked_constructor.return_value.finished = False

        types = ["checkout", "build", "test", "testresult"]
        for obj_type in types:
            with self.subTest(obj_type=obj_type), \
                    mock.patch(f"cki.triager.settings.DW_CLIENT.kcidb.{obj_type}s.get"):
                mocked_constructor.reset_mock()
                dwobject.from_obj_id(obj_type, obj_id=mocked_attrs)
                mocked_constructor.assert_called_once_with(obj_type, obj_id=mocked_attrs)

    @classmethod
    def _get_ids(cls, items):
        return [item["id"] for item in items]

    def test_filter_data_filter_by_checkout_id(self):
        """Test filter_data with checkout_id."""
        with self.subTest("Filter existent checkout"):
            result = dwobject.filter_data(self.data, checkout_id="redhat:1")
            self.assertEqual(self._get_ids(result["checkouts"]), ["redhat:1"])
            self.assertEqual(
                self._get_ids(result["builds"]),
                ["redhat:1_x86_64", "redhat:1_aarch64", "redhat:1_riscv64"],
            )
            self.assertEqual(
                self._get_ids(result["tests"]),
                [
                    "redhat:1_x86_64_upt1",
                    "redhat:1_x86_64_upt2",
                    "redhat:1_aarch64_upt1",
                    "redhat:1_aarch64_upt2",
                ],
            )

        with self.subTest("Filter missing checkout"):
            with self.assertLogs(logger=dwobject.LOGGER, level="WARNING") as log_ctx:
                result = dwobject.filter_data(self.data, checkout_id="redhat:no")

            self.assertIn(
                f"WARNING:{dwobject.LOGGER.name}:There's no checkout with the given ID: redhat:no",
                log_ctx.output,
            )
            self.assertEqual(
                result,
                {
                    "checkouts": [],
                    "builds": [],
                    "tests": [],
                    "testresults": [],
                    "issueoccurrences": [],
                },
            )

    def test_filter_data_filter_by_build_id(self):
        """Test filter_data with build_id."""
        with self.subTest("Filter existent build"):
            result = dwobject.filter_data(self.data, build_id="redhat:1_x86_64")

            self.assertEqual(self._get_ids(result["checkouts"]), ["redhat:1"])
            self.assertEqual(self._get_ids(result["builds"]), ["redhat:1_x86_64"])
            self.assertEqual(
                self._get_ids(result["tests"]), ["redhat:1_x86_64_upt1", "redhat:1_x86_64_upt2"]
            )

            with self.subTest("Filter missing build"):
                with self.assertLogs(logger=dwobject.LOGGER, level="WARNING") as log_ctx:
                    result = dwobject.filter_data(self.data, build_id="redhat:no")

                self.assertIn(
                    f"WARNING:{dwobject.LOGGER.name}:There's no build with the given ID: redhat:no",
                    log_ctx.output,
                )
                self.assertEqual(
                    result,
                    {
                        "checkouts": [],
                        "builds": [],
                        "tests": [],
                        "testresults": [],
                        "issueoccurrences": [],
                    },
                )

    def test_filter_data_filter_by_test_id(self):
        """Test filter_data with test_id."""
        with self.subTest("Filter existent build"):
            result = dwobject.filter_data(self.data, test_id="redhat:2_x86_64_upt1")

            self.assertEqual(self._get_ids(result["checkouts"]), ["redhat:2"])
            self.assertEqual(self._get_ids(result["builds"]), ["redhat:2_x86_64"])
            self.assertEqual(self._get_ids(result["tests"]), ["redhat:2_x86_64_upt1"])
            self.assertEqual(self._get_ids(result["testresults"]), ["redhat:2_x86_64_upt1_1"])

        with self.subTest("Filter missing build"):
            with self.assertLogs(logger=dwobject.LOGGER, level="WARNING") as log_ctx:
                result = dwobject.filter_data(self.data, test_id="redhat:no")

            self.assertIn(
                f"WARNING:{dwobject.LOGGER.name}:There's no test with the given ID: redhat:no",
                log_ctx.output,
            )

            self.assertEqual(
                result,
                {
                    "checkouts": [],
                    "builds": [],
                    "tests": [],
                    "testresults": [],
                    "issueoccurrences": [],
                },
            )

    def test_filter_data_multiple_ids(self):
        """Test filter_data with multiple IDs."""
        checkout_id = self.data["checkouts"][0]["id"]
        build_id = self.data["builds"][0]["id"]
        test_id = self.data["tests"][0]["id"]

        with self.assertRaises(ValueError):
            dwobject.filter_data(self.data, checkout_id=checkout_id, build_id=build_id)

        with self.assertRaises(ValueError):
            dwobject.filter_data(self.data, checkout_id=checkout_id, test_id=test_id)

        with self.assertRaises(ValueError):
            dwobject.filter_data(self.data, build_id=build_id, test_id=test_id)

        with self.assertRaises(ValueError):
            dwobject.filter_data(
                self.data, checkout_id=checkout_id, build_id=build_id, test_id=test_id
            )
