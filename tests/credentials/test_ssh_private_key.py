"""Tests for credential management."""

import json
import os
import unittest
from unittest import mock

from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from freezegun import freeze_time
import responses

from cki_tools.credentials import manager
from cki_tools.credentials import secrets

from .utils import setup_secrets


@mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://vault', 'VAULT_TOKEN': 'vault_token'})
class TestToken(unittest.TestCase):
    """Tests for credential management."""

    @freeze_time('2000-01-01T00:00:00.0+00:00')
    @responses.activate
    def test_create(self) -> None:
        """Test behavior of create."""
        private_key = rsa.generate_private_key(65537, 4096)
        private_bytes = private_key.private_bytes(
            serialization.Encoding.PEM,
            serialization.PrivateFormat.OpenSSH,
            serialization.NoEncryption()
        ).decode('ascii').strip()

        meta = {
            'active': True,
            'deployed': True,
            'comment': 'pooh',
            'token_type': 'ssh_private_key',
        }
        expected = {
            'created_at': '2000-01-01T00:00:00+00:00',
            'key_size': 4096,
        }

        cases = (
            ('default', {'key_size': 4096}, True),
            ('too small', {'key_size': 2048}, False),
        )

        for description, config, expected_result in cases:
            with (self.subTest(description),
                  setup_secrets({'secret_token': {'backend': 'hv', 'meta':  meta | config}}),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                rsps.get('https://vault/v1/apps/data/cki/secret_token',
                         json={'data': {'data': {'private_key': private_bytes}}})
                token_put = rsps.put('https://vault/v1/apps/data/cki/secret_token')

                if expected_result:
                    manager.main(['create', '--token-name', 'secret_token'])
                    self.assertEqual(secrets.secret('secret_token#'), meta | expected)
                    self.assertEqual(json.loads(token_put.calls[0].request.body), {'data': {
                        'private_key': mock.ANY, 'public_key': mock.ANY,
                    }})
                else:
                    with self.assertRaises(Exception):
                        manager.main(['create', '--token-name', 'secret_token'])
                    self.assertEqual(len(token_put.calls), 0)

    @freeze_time('2000-01-01T00:00:00.0+00:00')
    @responses.activate
    def test_rotate(self) -> None:
        """Test behavior of rotate."""
        private_key = rsa.generate_private_key(65537, 4096)
        private_bytes = private_key.private_bytes(
            serialization.Encoding.PEM,
            serialization.PrivateFormat.OpenSSH,
            serialization.NoEncryption()
        ).decode('ascii').strip()

        responses.get('https://vault/v1/apps/data/cki/secret_token',
                      json={'data': {'data': {'private_key': private_bytes}}})
        token_put = responses.put('https://vault/v1/apps/data/cki/secret_token/946684800')

        meta = {
            'active': True,
            'comment': 'pooh',
            'deployed': False,
            'key_size': 4096,
            'token_type': 'ssh_private_key',
        }
        expected = {
            'created_at': '2000-01-01T00:00:00+00:00',
        }

        with setup_secrets({'secret_token': {'meta':  meta}}):
            manager.main(['rotate', '--token-name', 'secret_token'])
            self.assertEqual(secrets.secret('secret_token#active'), False)
            self.assertEqual(secrets.secret('secret_token/946684800#'), meta | expected)
            self.assertEqual(json.loads(token_put.calls[0].request.body), {'data': {
                'private_key': mock.ANY, 'public_key': mock.ANY,
            }})

    @freeze_time('2000-01-01T00:00:00.0+00:00')
    @responses.activate
    def test_update(self) -> None:
        """Test behavior of update."""
        private_key = rsa.generate_private_key(65537, 4096)
        private_bytes = private_key.private_bytes(
            serialization.Encoding.PEM,
            serialization.PrivateFormat.OpenSSH,
            serialization.NoEncryption()
        ).decode('ascii').strip()
        responses.get('https://vault/v1/apps/data/cki/secret_token',
                      json={'data': {'data': {'private_key': private_bytes}}})

        token = 'secret_token'
        meta = {
            'active': True,
            'comment': 'pooh',
            'created_at': '2000-01-01T00:00:00+00:00',
            'key_size': 2048,
            'token_type': 'ssh_private_key',
        }
        expected = {
            'key_size': 4096,
        }

        with setup_secrets({token: {'backend': 'hv', 'meta': meta}}):
            manager.main(['update', '--token-name', token])
            self.assertEqual(secrets.secret(f'{token}#'), meta | expected)

    @responses.activate
    def test_validate(self) -> None:
        """Test behavior of validate."""
        private_key = rsa.generate_private_key(65537, 4096)
        private_bytes = private_key.private_bytes(
            serialization.Encoding.PEM,
            serialization.PrivateFormat.OpenSSH,
            serialization.NoEncryption()
        ).decode('ascii').strip()
        public_bytes = private_key.public_key().public_bytes(
            serialization.Encoding.OpenSSH,
            serialization.PublicFormat.OpenSSH
        ).decode('ascii').strip() + ' pooh'
        private_key2 = rsa.generate_private_key(65537, 4096)
        private_bytes2 = private_key2.private_bytes(
            serialization.Encoding.PEM,
            serialization.PrivateFormat.OpenSSH,
            serialization.NoEncryption()
        ).decode('ascii').strip()

        cases = (
            ('default', private_bytes, public_bytes, {
                'secret_token': {'backend': 'hv', 'meta': {
                    'token_type': 'ssh_private_key',
                    'active': True,
                    'created_at': '2000-01-01T00:00:00+00:00',
                    'deployed': True,
                }},
            }, True),
            ('wrong key', private_bytes2, public_bytes, {
                'secret_token': {'backend': 'hv', 'meta': {
                    'token_type': 'ssh_private_key',
                    'active': True,
                    'created_at': '2000-01-01T00:00:00+00:00',
                    'deployed': True,
                }},
            }, False),
        )

        for description, private, public, variables, expected in cases:
            with (self.subTest(description),
                  setup_secrets(variables),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                rsps.get('https://vault/v1/apps/data/cki/secret_token', json={'data': {'data': {
                    'private_key': private,
                    'public_key': public,
                }}})
                rsps.get('https://vault/v1/apps/data/cki/secret_token/2', json={'data': {'data': {
                    'private_key': private,
                    'public_key': public,
                }}})
                if expected:
                    manager.main(['validate', '--token-name', 'secret_token'])
                else:
                    with self.assertRaises(Exception):
                        manager.main(['validate', '--token-name', 'secret_token'])

    def test_destroy(self) -> None:
        """Test behavior of destroy."""

        token = 'secret_token'
        meta = {
            'deployed': False,
            'token_type': 'ssh_private_key',
        }
        with setup_secrets({token: {'meta': meta}}):
            manager.main(['destroy', '--token-name', token])
            self.assertEqual(secrets.secret(f'{token}#active'), False)
