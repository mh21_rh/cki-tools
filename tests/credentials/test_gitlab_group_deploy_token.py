"""Tests for credential management."""

from http import HTTPStatus
import json
import os
import unittest
from unittest import mock

from freezegun import freeze_time
import responses

from cki_tools.credentials import manager
from cki_tools.credentials import secrets

from .utils import setup_secrets


@mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://vault', 'VAULT_TOKEN': 'vault_token'})
class TestToken(unittest.TestCase):
    """Tests for credential management."""

    @freeze_time('2000-01-01T00:00:00.0+00:00')
    @responses.activate
    def test_create(self) -> None:
        """Test behavior of create."""
        responses.get('https://instance/api/v4/groups/g', json={'id': 4})
        responses.post('https://instance/api/v4/groups/4/deploy_tokens', json={
            'id': 6, 'username': 'user', 'expires_at': '2010-01-01',
            'revoked': False, 'expired': False, 'token': 'secret',
        })

        vault_url = responses.put('https://vault/v1/apps/data/cki/secret_token')
        responses.get('https://vault/v1/apps/data/cki/secret_token',
                      json={'data': {'data': {'value': 'secret'}}})

        token = 'secret_token'
        meta = {
            'deployed': False,
            'group_url': 'https://instance/groups/g',
            'scopes': ['scope'],
            'token_name': 'name',
            'token_type': 'gitlab_group_deploy_token',
        }
        expected = {
            'active': True,
            'created_at': '2000-01-01T00:00:00+00:00',
            'deployed': False,
            'expires_at': '2010-01-01',
            'revoked': False,
            'token_id': 6,
            'user_name': 'user',
        }

        with setup_secrets({token: {'meta': meta}}):
            manager.main(['create', '--token-name', token])
            self.assertEqual(json.loads(vault_url.calls[0].request.body),
                             {'data': {'value': 'secret'}})
            self.assertEqual(secrets.secret(f'{token}#'), meta | expected)

    @responses.activate
    def test_destroy(self) -> None:
        """Test behavior of destroy."""

        responses.get('https://instance/api/v4/groups/g', json={'id': 4})
        delete_url = responses.delete('https://instance/api/v4/groups/4/deploy_tokens/3', json={})

        token = 'secret_token'
        meta = {
            'active': True,
            'deployed': False,
            'group_url': 'https://instance/groups/g',
            'token_id': 3,
            'token_type': 'gitlab_group_deploy_token',
        }
        with setup_secrets({token: {'meta': meta}}):
            manager.main(['destroy', '--token-name', token])
            self.assertEqual(secrets.secret(f'{token}#active'), False)
            self.assertEqual(len(delete_url.calls), 1)

    @freeze_time('2000-01-01T00:00:00.0+00:00')
    @responses.activate
    def test_update(self) -> None:
        """Test behavior of update."""
        responses.get('https://instance/api/v4/groups/4/deploy_tokens/6', json={
            'id': 6,
            'username': 'user',
            'name': 'name',
            'scopes': ['scope'],
            'expires_at': '2010-01-01',
            'revoked': False,
            'expired': False,
        })
        responses.get('https://instance/api/v4/groups/g', json={'id': 4})

        responses.get('https://vault/v1/apps/data/cki/secret_token',
                      json={'data': {'data': {'value': 'token'}}})

        token = 'secret_token'
        meta = {
            'token_type': 'gitlab_group_deploy_token',
            'group_url': 'https://instance/groups/g',
            'token_id': 6,
            'active': True,
        }
        expected = {
            'scopes': ['scope'],
            'token_name': 'name',
            'token_id': 6,
            'expires_at': '2010-01-01',
            'revoked': False,
            'active': True,
            'user_name': 'user',
        }
        with setup_secrets({token: {'meta': meta}}):
            manager.main(['update', '--token-name', token])
            self.assertEqual(secrets.secret(f'{token}#'), meta | expected)

    @responses.activate
    def test_validate(self) -> None:
        """Test behavior of validate."""
        cases = (
            ('group deploy token', {}, {
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'token_type': 'gitlab_group_deploy_token',
                'group_url': 'https://instance/groups/g',
                'token_id': 6,
                'active': True,
                'deployed': True,
            }, [{
                'url': 'https://instance/api/v4/groups/4/deploy_tokens/6',
                'json': {'revoked': False, 'expired': False},
            }], True),
            ('invalid group deploy token', {}, {
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'token_type': 'gitlab_group_deploy_token',
                'group_url': 'https://instance/groups/g',
                'token_id': 6,
                'active': True,
                'deployed': True,
            }, [{
                'url': 'https://instance/api/v4/groups/4/deploy_tokens/6',
                'status': HTTPStatus.NOT_FOUND,
            }], False),
            ('revoked group deploy token', {}, {
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'token_type': 'gitlab_group_deploy_token',
                'group_url': 'https://instance/groups/g',
                'token_id': 6,
                'active': True,
                'deployed': True,
            }, [{
                'url': 'https://instance/api/v4/groups/4/deploy_tokens/6',
                'json': {'revoked': True, 'expired': False},
            }], False),
            ('inactive group deploy token', {}, {
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'token_type': 'gitlab_group_deploy_token',
                'group_url': 'https://instance/groups/g',
                'token_id': 6,
                'active': True,
                'deployed': True,
            }, [{
                'url': 'https://instance/api/v4/groups/4/deploy_tokens/6',
                'json': {'revoked': False, 'expired': True},
            }], False),
        )
        for description, other_data, secrets_data, url_mocks, expected in cases:
            with (self.subTest(description),
                  setup_secrets({
                    'secret_token': {'meta': secrets_data},
                  } | (other_data or {
                    'secret_token/B': {'meta': {**secrets_data, 'deployed': False}},
                  })),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                for url_mock in url_mocks:
                    rsps.add(**{'method': 'GET', **url_mock})
                rsps.get('https://instance/api/v4/groups/g', json={'id': 4})
                rsps.get('https://vault/v1/apps/data/cki/secret_token',
                         json={'data': {'data': {'value': 'token'}}})
                rsps.get('https://vault/v1/apps/data/cki/secret_token/B',
                         json={'data': {'data': {'value': 'token'}}})
                if not expected:
                    with self.assertRaises(Exception):
                        manager.main(['validate', '--token-name', 'secret_token'])
                else:
                    manager.main(['validate', '--token-name', 'secret_token'])
