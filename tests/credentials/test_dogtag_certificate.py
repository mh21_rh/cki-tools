"""Tests for credential management."""

import json
import os
import unittest
from unittest import mock

from cki_lib import misc
from cryptography import x509
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from freezegun import freeze_time
import responses

from cki_tools.credentials import manager
from cki_tools.credentials import secrets

from .utils import setup_secrets


@mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://vault', 'VAULT_TOKEN': 'vault_token'})
class TestToken(unittest.TestCase):
    """Tests for credential management."""

    @responses.activate
    def test_create(self) -> None:
        """Test behavior of create."""
        responses.get('https://vault/v1/apps/data/cki/secret_token',
                      json={'data': {'data': {'value': 'secret'}}})
        responses.get('https://vault/v1/apps/data/cki/ldap',
                      json={'data': {'data': {'value': 'password'}}})
        token_put = responses.put('https://vault/v1/apps/data/cki/secret_token')

        cert_post = responses.post(
            'http://dogtag:8443/ca/rest/certrequests', json={'entries': [{
                'requestURL': 'http://dogtag:8443/ca/rest/certrequests/1',
            }]})
        responses.get('http://dogtag:8443/ca/rest/certrequests/1', json={'certId': 'pooh'})
        responses.get('http://dogtag:8443/ca/rest/certs/pooh', json={
            'Encoded': 'certificate\n',
            'id': 'pooh',
            'IssuerDN': 'issuer',
            'NotAfter': '2010-01-10T00:00:00+00:00',
            'NotBefore': '2010-01-01T00:00:00+00:00',
            'Status': 'VALID',
            'SubjectDN': 'subject',
        })

        with setup_secrets({
            'secret_token': {
                'backend': 'hv',
                'meta': {
                    'active': True,
                    'deployed': True,
                    'token_type': 'dogtag_certificate',
                    'server_url': 'http://dogtag:8443',
                    'SubjectDN': 'uid=subject,something=else',
                },
            },
            'ldap': {'backend': 'hv', 'meta': {'token_type': 'ldap_password', 'uid': 'subject'}},
        }):
            manager.main(['create', '--token-name', 'secret_token'])
            self.assertEqual(secrets.secret('secret_token#'), {
                'active': True,
                'created_at': '2010-01-01T00:00:00+00:00',
                'deployed': True,
                'expires_at': '2010-01-10T00:00:00+00:00',
                'id': 'pooh',
                'IssuerDN': 'issuer',
                'server_url': 'http://dogtag:8443',
                'SubjectDN': 'subject',
                'token_type': 'dogtag_certificate',
            })
            self.assertEqual(len(cert_post.calls), 1)
            self.assertEqual(len(token_put.calls), 1)
            certrequest = json.loads(cert_post.calls[0].request.body)
            misc.set_nested_key(certrequest, 'Input/0/Attribute/1/Value', 'request')
            self.assertEqual(certrequest, {
                'ProfileID': 'caDirAppUserCert',
                'Attributes': {'Attribute': [
                    {'name': 'uid', 'value': 'subject'},
                    {'name': 'pwd', 'value': 'password'},
                ]},
                'Input': [{'Attribute': [
                    {'name': 'cert_request_type', 'Value': 'pkcs10'},
                    {'name': 'cert_request', 'Value': 'request'},
                ]}],
            })

            self.assertEqual(json.loads(token_put.calls[0].request.body), {'data': {
                'certificate': 'certificate',
                'private_key': mock.ANY,
            }})

    @responses.activate
    def test_update(self) -> None:
        """Test behavior of update."""
        responses.get('https://vault/v1/apps/data/cki/secret_token',
                      json={'data': {'data': {'value': 'secret'}}})

        responses.get('http://dogtag:8443/ca/rest/certs/pooh', json={
            'Encoded': 'certificate\n',
            'id': 'bear',
            'IssuerDN': 'issuer',
            'NotAfter': '2010-01-10T00:00:00+00:00',
            'NotBefore': '2010-01-01T00:00:00+00:00',
            'Status': 'VALID',
            'SubjectDN': 'subject',
        })

        with setup_secrets({'secret_token': {
                'backend': 'hv',
                'meta': {
                    'active': True,
                    'token_type': 'dogtag_certificate',
                    'id': 'pooh',
                    'server_url': 'http://dogtag:8443',
                },
        }}):
            manager.main(['update', '--token-name', 'secret_token'])
            self.assertEqual(secrets.secret('secret_token#'), {
                'active': True,
                'created_at': '2010-01-01T00:00:00+00:00',
                'expires_at': '2010-01-10T00:00:00+00:00',
                'id': 'bear',
                'IssuerDN': 'issuer',
                'server_url': 'http://dogtag:8443',
                'SubjectDN': 'subject',
                'token_type': 'dogtag_certificate',
            })

    @responses.activate
    def test_validate(self) -> None:
        """Test behavior of validate."""
        created_at = misc.datetime_fromisoformat_tz_utc('1999-01-01T00:00:00.0+00:00')
        expires_at = misc.datetime_fromisoformat_tz_utc('2001-01-01T00:00:00.0+00:00')

        key = rsa.generate_private_key(65537, 4096)
        key2 = rsa.generate_private_key(65537, 4096)
        cert = x509.CertificateBuilder().subject_name(x509.Name([
        ])).public_key(
            key.public_key()
        ).issuer_name(x509.Name([
        ])).serial_number(1).not_valid_before(
            created_at
        ).not_valid_after(
            expires_at
        ).sign(key, hashes.SHA256())

        cases = (
            ('default', key, cert, '2000-01-01T00:00:00.0+00:00', True),
            ('key mismatch', key2, cert, '2000-01-01T00:00:00.0+00:00', False),
        )

        for description, key, cert, now, expected in cases:
            with (self.subTest(description),
                  setup_secrets({'secret_token': {'backend': 'hv', 'meta': {
                      'active': True,
                      'created_at': '1999-01-01T00:00:00+00:00',
                      'deployed': True,
                      'token_type': 'dogtag_certificate',
                      'expires_at': expires_at.isoformat(),
                  }}}),
                  freeze_time(now),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                rsps.get('https://vault/v1/apps/data/cki/secret_token', json={'data': {'data': {
                    'private_key': key.private_bytes(
                        serialization.Encoding.PEM,
                        serialization.PrivateFormat.PKCS8,
                        serialization.NoEncryption()).decode('ascii').strip(),
                    'certificate': cert.public_bytes(serialization.Encoding.PEM).decode('ascii')
                    .strip()
                }}})
                if expected:
                    manager.main(['validate', '--token-name', 'secret_token'])
                else:
                    with self.assertRaises(Exception):
                        manager.main(['validate', '--token-name', 'secret_token'])
