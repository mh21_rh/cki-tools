"""Tests for credential management."""

import datetime
import json
import os
import unittest
from unittest import mock

import responses

from cki_tools.credentials import aws_tokens
from cki_tools.credentials import manager
from cki_tools.credentials import secrets

from .utils import setup_secrets


@mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://vault', 'VAULT_TOKEN': 'vault_token'})
class TestToken(unittest.TestCase):
    """Tests for credential management."""

    def setUp(self) -> None:
        """Test setup code."""
        aws_tokens._iam.cache_clear()

    @mock.patch('cki_tools.credentials.aws_tokens.BOTO_SESSION')
    @responses.activate
    def test_create(self, boto_session) -> None:
        """Test behavior of create."""
        create_date = datetime.datetime(2010, 1, 1)
        boto_session.client.return_value.create_access_key.return_value = {'AccessKey': {
            'AccessKeyId': 'aki',
            'SecretAccessKey': 'secret',
            'CreateDate': create_date,
        }}

        vault_url = responses.put('https://vault/v1/apps/data/cki/secret_token')
        responses.get('https://vault/v1/apps/data/cki/secret_token',
                      json={'data': {'data': {'value': 'secret'}}})

        token = 'secret_token'
        meta = {
            'account': 'account',
            'token_type': 'aws_secret_access_key',
            'user_name': 'username',
        }
        expected = {
            'active': True,
            'access_key_id': 'aki',
            'arn': 'arn:aws:iam::account:user/username',
            'created_at': '2010-01-01T00:00:00+00:00',
            'deployed': True,
        }

        with setup_secrets({token: {'meta': meta}}):
            manager.main(['create', '--token-name', token])
            boto_session.client.return_value.create_access_key.assert_called_with(
                UserName='username')
            self.assertEqual(json.loads(vault_url.calls[0].request.body),
                             {'data': {'value': 'secret'}})
            self.assertEqual(secrets.secret(f'{token}#'), meta | expected)

    @mock.patch('cki_tools.credentials.aws_tokens.BOTO_SESSION')
    def test_destroy(self, boto_session) -> None:
        """Test behavior of destroy."""

        token = 'secret_token'
        meta = {
            'access_key_id': 'aki',
            'account': 'account',
            'active': True,
            'arn': 'arn',
            'created_at': '2010-01-01T00:00:00+00:00',
            'deployed': False,
            'token_type': 'aws_secret_access_key',
            'user_name': 'username',
        }
        with setup_secrets({token: {'meta': meta}}):
            manager.main(['destroy', '--token-name', token])
            boto_session.client.return_value.delete_access_key.assert_called_with(
                UserName='username', AccessKeyId='aki')

    @mock.patch('cki_tools.credentials.aws_tokens.BOTO_SESSION')
    @responses.activate
    def test_update(self, boto_session) -> None:
        """Test behavior of update."""
        create_date = datetime.datetime(2010, 1, 1)
        responses.get('https://vault/v1/apps/data/cki/secret_token',
                      json={'data': {'data': {'value': 'secret'}}})
        boto_session.client.return_value.get_access_key_last_used.return_value = {
            'UserName': 'username',
        }
        boto_session.client.return_value.list_access_keys.return_value = {'AccessKeyMetadata': [
            {'AccessKeyId': 'aki', 'Status': 'Active', 'CreateDate': create_date},
        ]}

        cases = (
            ('default', 'account', {
            }, {
                'account': 'account',
                'active': True,
                'arn': 'arn',
                'created_at': '2010-01-01T00:00:00+00:00',
                'deployed': True,
                'user_name': 'username',
            }),
            ('known account, arn', 'other-account', {
                'account': 'other-account',
                'arn': 'other-arn',
            }, {
                'account': 'other-account',
                'active': True,
                'arn': 'other-arn',
                'created_at': '2010-01-01T00:00:00+00:00',
                'deployed': True,
                'user_name': 'username',
            }),
            ('mismatched account ignored', 'account', {
                'account': 'other-account',
                'arn': 'other-arn',
            }, {
                'account': 'other-account',
                'active': True,
                'arn': 'other-arn',
                'deployed': True,
            }),
            ('known account, arn, user_name', 'other-account', {
                'account': 'other-account',
                'arn': 'other-arn',
                'user_name': 'other-username',
            }, {
                'account': 'other-account',
                'active': True,
                'arn': 'other-arn',
                'created_at': '2010-01-01T00:00:00+00:00',
                'deployed': True,
                'user_name': 'other-username',
            }),
            ('custom endpoint', 'account', {
                'endpoint_url': 'endpoint',
            }, {
                'active': True,
                'deployed': True,
                'endpoint_url': 'endpoint',
            }),
        )

        for description, account, meta, expected in cases:
            with self.subTest(description), setup_secrets({'secret_token': {
                    'backend': 'hv',
                    'meta': {
                        'access_key_id': 'aki',
                        'active': True,
                        'deployed': True,
                        'token_type': 'aws_secret_access_key',
                    } | meta,
            }}), mock.patch('cki_tools.credentials.aws_tokens._current_account',
                            return_value=account):
                boto_session.client.return_value.get_caller_identity.return_value = {
                    'Account': account,
                    'Arn': 'arn',
                }
                manager.main(['update', '--token-name', 'secret_token'])
                self.assertEqual(secrets.secret('secret_token#'), {
                    'token_type': 'aws_secret_access_key',
                    'access_key_id': 'aki',
                    **expected,
                })
