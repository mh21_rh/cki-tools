"""Tests for credential management."""

import os
import typing
import unittest
from unittest import mock

import freezegun
import responses

from cki_tools.credentials import gitlab_tokens
from cki_tools.credentials import manager
from cki_tools.credentials import secrets
from cki_tools.credentials import token

from .utils import setup_secrets


@mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://vault', 'VAULT_TOKEN': 'vault_token'})
class TestToken(unittest.TestCase):
    """Tests the base token class."""

    def test_get_token(self) -> None:
        """Test behavior of token.get_token."""
        cases = (
                ('gitlab_group_deploy_token', True),
                ('pooh_bear', False),
                ('pooh_bear', None),
        )
        for token_type, expected in cases:
            with self.subTest(token_type), setup_secrets({'secret_token/a': {'meta': {
                'token_type': token_type,
            }}}):
                if expected is None:
                    instance = token.get_token('secret_token/a', False, False)
                    self.assertIs(instance, None)
                elif expected is False:
                    with self.assertRaises(Exception):
                        token.get_token('secret_token/a', False)
                else:
                    instance = token.get_token('secret_token/a', False)
                    self.assertIsInstance(instance, gitlab_tokens.GitLabGroupDeployToken)
                    self.assertEqual(instance.token_group, 'secret_token')

    def test_full_token_name(self) -> None:
        """Test behavior of token.full_token_name."""
        with setup_secrets({'secret_token/a': {'meta': {
            'token_type': 'gitlab_group_deploy_token',
        }}}):
            instance = token.get_token('secret_token/a', False)
            self.assertEqual(instance.full_token_name('b'), 'secret_token/b')
            self.assertEqual(instance.full_token_name(''), 'secret_token')

    def test_get_active_token_data(self) -> None:
        """Test behavior of token.get_active_token_data."""
        with setup_secrets({
            'other/a': {'meta': {'active': True, 'token_type': 'gitlab_group_deploy_token'}},
            'name': {'meta': {'active': True, 'token_type': 'gitlab_group_deploy_token'}},
            'name/a': {'meta': {'active': True, 'token_type': 'gitlab_group_deploy_token'}},
            'name/b': {'meta': {'active': True, 'token_type': 'gitlab_group_deploy_token'}},
            'name/c': {'meta': {'active': False, 'token_type': 'gitlab_group_deploy_token'}},
        }):
            instance = token.get_token('name', False)
            self.assertCountEqual(instance.active_token_data.keys(),
                                  ['name', 'name/a', 'name/b'])

    def test_check_invariants(self) -> None:
        """Test behavior of token.check_invariants."""
        cases = (
            ('none deployed', {
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'token_type': 'gitlab_project_token',
                'active': True,
                'deployed': False,
            }, {}, 'No deployed'),
            ('both deployed', {
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'token_type': 'gitlab_project_token',
                'active': True,
                'deployed': True,
            }, {
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'token_type': 'gitlab_project_token',
                'active': True,
                'deployed': True,
            }, 'More than one'),
            ('wrong active field', {
                'active': 'pooh',
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'token_type': 'gitlab_project_token',
                'deployed': True,
            }, {}, 'active'),
            ('missing deployed field', {
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'token_type': 'gitlab_project_token',
                'active': True,
            }, {}, 'deployed'),
            ('missing created_at field', {
                'token_type': 'gitlab_project_token',
                'active': True,
                'deployed': True,
            }, {}, 'created_at'),
            ('inconsistent type', {
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'token_type': 'gitlab_project_token',
                'active': True,
                'deployed': True,
            }, {
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'token_type': 'gitlab_project_deploy_token',
                'active': True,
                'deployed': False,
            }, 'Inconsistent'),
        )
        for description, meta, other_meta, expected_exception in cases:
            with self.subTest(description), setup_secrets({
                'secret_token': {'meta': meta},
                'secret_token/B': {'meta': meta | (other_meta or {'deployed': False})},
            }):
                instance = token.get_token('secret_token', False)
                with self.assertRaises(Exception) as ex:
                    instance.check_invariants()
                self.assertIn(expected_exception, str(ex.exception))

    # create and update are thoroughly tested on the individual token types

    def test_destroy(self) -> None:
        """Test behavior of token.destroy."""
        with setup_secrets({
            'name/a': {'meta': {'deployed': True, 'token_type': 'gitlab_group_deploy_token'}},
        }):
            with self.assertRaises(Exception) as ex:
                manager.main(['destroy', '--token-name', 'name/a'])
            self.assertIn('deployed', str(ex.exception))

    def test_rotate(self) -> None:
        """Test behavior of token.rotate."""
        with setup_secrets({
            'name/a': {'meta': {'deployed': True, 'token_type': 'gitlab_group_deploy_token'}},
        }):
            with self.assertRaises(Exception) as ex:
                manager.main(['rotate', '--token-name', 'name/a'])
            self.assertIn('deployed', str(ex.exception))

    def test_purge(self) -> None:
        """Test behavior of token.purge."""
        cases = (
            (None, {
                'active': False,
                'deployed': False,
                'token_type': 'password',
            }),
            ('active', {
                'active': True,
                'deployed': False,
                'token_type': 'password',
            }),
            ('deployed', {
                'active': False,
                'deployed': True,
                'token_type': 'password',
            }),
        )

        for description, meta in cases:
            with (self.subTest(description),
                  setup_secrets({'secret_token': {'backend': 'hv', 'meta': meta}})):
                instance = token.get_token('secret_token', False)
                if description:
                    with self.assertRaises(ValueError) as ex:
                        instance.purge('')
                    self.assertIn(description, str(ex.exception))
                else:
                    instance.purge('')
                    self.assertEqual(secrets.secret('secret_token[]:'), [])
                    self.assertEqual(secrets.secret('secret_token[]#'), [])

    @freezegun.freeze_time('2000-01-03T00:00:00.0+00:00')
    @responses.activate
    def test_prepare(self) -> None:
        """Test behavior of token.prepare."""
        responses.get('https://instance/api/v4/projects/g%2Fp', json={'id': 1})
        responses.get('https://instance/api/v4/groups/g', json={'id': 4})
        responses.post('https://instance/api/v4/projects/1/access_tokens', json={
            'id': 3, 'user_id': 2, 'created_at': '2000-01-01', 'expires_at': '2010-01-01',
            'revoked': False, 'active': True, 'token': 'secret',
        })
        responses.post('https://instance/api/v4/projects/1/deploy_tokens', json={
            'id': 5, 'username': 'user', 'expires_at': '2010-01-01',
            'revoked': False, 'expired': False, 'token': 'secret',
        })
        responses.get('https://instance/api/v4/users/2', json={
            'username': 'user'
        })
        responses.get('https://instance/api/v4/personal_access_tokens/3', json={
            'id': 3,
            'user_id': 2,
            'name': 'name',
                    'scopes': ['api'],
                    'created_at': '2000-01-01',
                    'expires_at': '2010-01-01',
                    'revoked': False,
                    'active': True,
        })
        responses.get('https://instance/api/v4/personal_access_tokens/4', json={
            'id': 4,
            'user_id': 2,
            'name': 'name',
                    'scopes': ['api'],
                    'created_at': '2000-01-01',
                    'expires_at': '2010-01-01',
                    'revoked': False,
                    'active': True,
        })

        responses.get('https://vault/v1/apps/data/cki/secret_token/1',
                      json={'data': {'data': {'value': 'secret'}}})
        responses.get('https://vault/v1/apps/data/cki/secret_token/946857600',
                      json={'data': {'data': {'value': 'secret'}}})

        cases = (
            ('not needed', [{
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }, {
                'active': True,
                'created_at': '1999-01-02T00:00:00.0+00:00',
                'deployed': False,
                'expires_at': '2000-01-16T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }], 0, 0, 0),
            ('missing', [{
                'access_level': 10,
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'project_url': 'https://instance/g/p',
                'scopes': ['scope'],
                'token_name': 'name',
                'token_type': 'gitlab_project_token',
            }], 1, 0, 0),
            ('manual', [{
                'active': True,
                'created_at': '2000-01-01T00:00:00+00:00',
                'deployed': False,
                'project_url': 'https://instance/g/p',
                'revoked': False,
                'scopes': ['scope'],
                'token_id': 5,
                'token_name': 'name',
                'token_type': 'gitlab_project_deploy_token',
                'user_name': 'user',
            }, {
                'active': True,
                'created_at': '2000-01-02T00:00:00+00:00',
                'deployed': True,
                'project_url': 'https://instance/g/p',
                'revoked': False,
                'scopes': ['scope'],
                'token_id': 5,
                'token_name': 'name',
                'token_type': 'gitlab_project_deploy_token',
                'user_name': 'user',
            }], 1, 0, 1),
        )
        for description, secrets_data, s2_expected, p_expected, d_expected in cases:
            with (self.subTest(description),
                  setup_secrets({f'secret_token/{i}': {'backend': 'hv', 'meta': d}
                                 for i, d in enumerate(secrets_data)})):
                s2_put = responses.upsert(
                    'PUT', 'https://vault/v1/apps/data/cki/secret_token/946857600')
                p_rotate = responses.upsert(
                    'POST', 'https://instance/api/v4/personal_access_tokens/3/rotate',
                    json={'id': 4, 'token': 'new-secret'})
                d_delete = responses.upsert(
                    'DELETE', 'https://instance/api/v4/projects/1/deploy_tokens/5', json={})
                manager.main(['prepare'])
                self.assertEqual(len(s2_put.calls), s2_expected)
                self.assertEqual(len(p_rotate.calls), p_expected)
                self.assertEqual(len(d_delete.calls), d_expected)

    @freezegun.freeze_time('2000-01-03T00:00:00.0+00:00')
    def test_switch(self) -> None:
        """Test behavior of token.switch."""
        cases = (
            ('default', [], [{
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }, {
                'active': True,
                'created_at': '1999-01-02T00:00:00.0+00:00',
                'deployed': False,
                'expires_at': '2000-01-16T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }], 1),
            ('created wrong order', [], [{
                'active': True,
                'created_at': '1999-01-02T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }, {
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': False,
                'expires_at': '2000-01-16T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }], 0),
            ('not needed', [], [{
                'active': True,
                'created_at': '1999-11-11T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2001-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }, {
                'active': True,
                'created_at': '1999-11-12T00:00:00.0+00:00',
                'deployed': False,
                'expires_at': '2001-01-16T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }], 0),
            ('named token', ['--token-name', 'secret_token'], [{
                'active': True,
                'created_at': '1999-11-11T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2001-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }, {
                'active': True,
                'created_at': '1999-11-12T00:00:00.0+00:00',
                'deployed': False,
                'expires_at': '2001-01-16T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }], 0),
            ('forced named token', ['--force', '--token-name', 'secret_token'], [{
                'active': True,
                'created_at': '1999-11-11T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2001-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }, {
                'active': True,
                'created_at': '1999-11-12T00:00:00.0+00:00',
                'deployed': False,
                'expires_at': '2001-01-16T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }], 1),
        )

        for description, args, secrets_data, expected in cases:
            with (self.subTest(description),
                  setup_secrets({f'secret_token/{i}': {'backend': 'hv', 'meta': d}
                                 for i, d in enumerate(secrets_data)})):
                self.assertEqual(manager.main(['switch'] + args), 0)
                for i in range(2):
                    self.assertEqual(secrets.secret(f'secret_token/{i}#deployed'), expected == i)

    @freezegun.freeze_time('2000-01-03T00:00:00.0+00:00')
    @responses.activate
    def test_clean(self) -> None:
        """Test behavior of token.clean."""
        responses.get('https://instance/api/v4/projects/g%2Fp', json={'id': 1})
        responses.delete('https://instance/api/v4/personal_access_tokens/self', json={})
        responses.delete('https://instance/api/v4/projects/1/deploy_tokens/3', json={})

        responses.get('https://vault/v1/apps/data/cki/secret_token/1',
                      json={'data': {'data': {'value': 'secret'}}})
        responses.get('https://vault/v1/apps/data/cki/secret_token/2',
                      json={'data': {'data': {'value': 'secret'}}})

        cases = (
            ('1 < 2', [], [{
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }], 1),
            ('2 == 2', [], [{
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }, {
                'active': True,
                'created_at': '1999-01-02T00:00:00.0+00:00',
                'deployed': False,
                'expires_at': '2000-01-16T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }], 2),
            ('3 > 2', [], [{
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }, {
                'active': True,
                'created_at': '1999-01-02T00:00:00.0+00:00',
                'deployed': False,
                'expires_at': '2000-01-16T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }, {
                'active': True,
                'created_at': '1999-01-02T00:00:00.0+00:00',
                'deployed': False,
                'expires_at': '2000-01-16T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }], 2),
            ('1 == 1', [], [{
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'project_url': 'https://instance/g/p',
                'token_id': 3,
                'token_type': 'gitlab_project_deploy_token',
            }], 1),
            ('2 > 1', [], [{
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'project_url': 'https://instance/g/p',
                'token_id': 3,
                'token_type': 'gitlab_project_deploy_token',
            }, {
                'active': True,
                'created_at': '1999-01-02T00:00:00.0+00:00',
                'deployed': False,
                'expires_at': '2000-01-16T00:00:00.0+00:00',
                'project_url': 'https://instance/g/p',
                'token_id': 3,
                'token_type': 'gitlab_project_deploy_token',
            }], 1),
            ('3 > 0', [], [{
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'token_type': 'splunk_hec_token',
            }, {
                'active': True,
                'created_at': '1999-01-02T00:00:00.0+00:00',
                'deployed': False,
                'expires_at': '2000-01-16T00:00:00.0+00:00',
                'token_type': 'splunk_hec_token',
            }, {
                'active': True,
                'created_at': '1999-01-02T00:00:00.0+00:00',
                'deployed': False,
                'expires_at': '2000-01-16T00:00:00.0+00:00',
                'token_type': 'splunk_hec_token',
            }], 3),
        )

        for description, args, secrets_data, expected in cases:
            with (self.subTest(description),
                  setup_secrets({f'secret_token/{i}': {'backend': 'hv', 'meta': d}
                                 for i, d in enumerate(secrets_data)})):
                self.assertEqual(manager.main(['clean'] + args), 0)
                self.assertEqual(len([
                    a for a in secrets.secret('secret_token[]#active') if a
                ]), expected)

    @freezegun.freeze_time('2000-01-01T00:00:00.0+00:00')
    @responses.activate
    def test_check_needs_prepare(self) -> None:
        """Test behavior of token.check_needs_prepare."""
        cases = (
            ('1 version', [{
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }]),
            ('Deployed version is newer', [{
                'active': True,
                'created_at': '1999-01-02T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }, {
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': False,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }]),
            ('Deployed version is newer', [{  # different order
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': False,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }, {
                'active': True,
                'created_at': '1999-01-02T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }]),
        )

        for description, meta in cases:
            with (self.subTest(description),
                  setup_secrets({f'secret_token/{i}': {'backend': 'hv', 'meta': d}
                                 for i, d in enumerate(meta)})):
                instance = token.get_token('secret_token', False)
                result = instance.check_needs_prepare()
                if description is None:
                    self.assertIsNone(result)
                else:
                    self.assertIsNotNone(result)
                    self.assertIn(description, typing.cast(str, result))

    @freezegun.freeze_time('2000-01-01T00:00:00.0+00:00')
    @responses.activate
    def test_check_needs_rotate(self) -> None:
        """Test behavior of token.check_needs_rotate."""
        cases = (
            (None, {
                'token_type': 'password',
                'active': True,
                'deployed': True,
                'created_at': '2000-01-01T00:00:00.0+00:00',
            }),
            ('too old', {
                'token_type': 'password',
                'active': True,
                'deployed': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
            }),
            ('expiry', {
                'token_type': 'password',
                'active': True,
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
            }),
        )

        for description, meta in cases:
            with (self.subTest(description),
                  setup_secrets({'secret_token': {'backend': 'hv', 'meta': meta}})):
                instance = token.get_token('secret_token', False)
                result = instance.check_needs_rotate()
                if description is None:
                    self.assertIsNone(result)
                else:
                    self.assertIsNotNone(result)
                    self.assertIn(description, typing.cast(str, result))

    @freezegun.freeze_time('2000-01-01T00:00:00.0+00:00')
    @responses.activate
    def test_check_needs_clean(self) -> None:
        """Test behavior of token.check_needs_clean."""
        cases = (
            (None, [{
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': False,
                'token_type': 'gitlab_project_deploy_token',
            }]),
            ('2 active versions instead of 1', [{
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': True,
                'token_type': 'gitlab_project_deploy_token',
            }, {
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': False,
                'token_type': 'gitlab_project_deploy_token',
            }]),
            (None, [{
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': True,
                'token_type': 'gitlab_personal_token',
            }, {
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': False,
                'token_type': 'gitlab_personal_token',
            }]),
            ('3 active versions instead of 2', [{
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': True,
                'token_type': 'gitlab_personal_token',
            }, {
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': True,
                'token_type': 'gitlab_personal_token',
            }, {
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': False,
                'token_type': 'gitlab_personal_token',
            }]),
        )

        for description, meta in cases:
            with (self.subTest(description),
                  setup_secrets({f'secret_token/{i}': {'meta': d} for i, d in enumerate(meta)})):
                instance = token.get_token('secret_token', False)
                result = instance.check_needs_clean()
                if description is None:
                    self.assertIsNone(result)
                else:
                    self.assertIsNotNone(result)
                    self.assertIn(description, typing.cast(str, result))
