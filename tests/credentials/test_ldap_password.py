"""Tests for credential management."""

import os
import unittest
from unittest import mock

from ldap import INVALID_CREDENTIALS
import responses

from cki_tools.credentials import ldap_helpers
from cki_tools.credentials import manager
from cki_tools.credentials import secrets

from .utils import setup_secrets


@mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://vault', 'VAULT_TOKEN': 'vault_token'})
class TestToken(unittest.TestCase):
    """Tests for credential management."""

    def setUp(self) -> None:
        """Set up test environment."""
        ldap_helpers.ldap_search.cache_clear()

    @mock.patch('ldap.initialize')
    def test_update(self, ldap_initialize) -> None:
        """Test behavior of update."""
        ldap_initialize().search_s.side_effect = lambda dn, *_, **__: [(dn, {'cn': [b'common']})]

        token = 'secret_token'
        meta = {
            'active': True,
            'dn': 'some=dn',
            'ldap_server': 'ldap.server',
            'token_type': 'ldap_password',
        }
        expected = {
            'cn': 'common',
        }

        with setup_secrets({'secret_token': {'meta': meta}}):
            manager.main(['update', '--token-name', token])
            self.assertEqual(secrets.secret(f'{token}#'), meta | expected)

    @responses.activate
    @mock.patch('ldap.initialize')
    def test_validate(self, ldap_initialize) -> None:
        """Test behavior of validate."""
        def mock_bind(dn, *_, **__):
            match dn:
                case 'dn=working':
                    return
                case 'dn=failing':
                    raise INVALID_CREDENTIALS(dn)
        ldap_initialize.return_value.simple_bind_s.side_effect = mock_bind

        responses.get('https://vault/v1/apps/data/cki/secret_token',
                      json={'data': {'data': {'value': 'secret'}}})

        cases = (
            ('dn=working', True),
            ('dn=failing', False),
        )

        for dn, expected in cases:
            with self.subTest(dn), setup_secrets({'secret_token': {'meta': {
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': True,
                'dn': dn,
                'ldap_server': 'ldap.server',
                'token_type': 'ldap_password',
            }}}):
                if expected:
                    manager.main(['validate', '--token-name', 'secret_token'])
                else:
                    with self.assertRaises(INVALID_CREDENTIALS):
                        manager.main(['validate', '--token-name', 'secret_token'])
