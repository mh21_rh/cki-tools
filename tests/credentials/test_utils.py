"""Tests for credential management."""

import unittest

from cki_tools.credentials import utils

from .utils import setup_secrets


class TestCredentialManager(unittest.TestCase):
    """Tests for credential management."""

    def test_all_tokens(self) -> None:
        """Test behavior of all_tokens."""
        cases = (
            ('default', ['t1', 't2'], {'t1'}, ['0']),
            ('multiple', ['t1'], {'t1', 't2'}, ['0']),
        )
        for description, tokens, types, expected in cases:
            with self.subTest(description), setup_secrets({
                str(i): {'meta': {'token_type': t}} for i, t in enumerate(tokens)
            }):
                self.assertEqual(utils.all_tokens(types), expected)
