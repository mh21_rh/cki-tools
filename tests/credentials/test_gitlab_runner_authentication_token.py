"""Tests for credential management."""

from http import HTTPStatus
import os
import unittest
from unittest import mock

from freezegun import freeze_time
import responses

from cki_tools.credentials import manager
from cki_tools.credentials import secrets

from .utils import setup_secrets


@mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://vault', 'VAULT_TOKEN': 'vault_token'})
class TestToken(unittest.TestCase):
    """Tests for credential management."""

    @freeze_time('2000-01-01T00:00:00.0+00:00')
    @responses.activate
    def test_update(self) -> None:
        """Test behavior of update."""
        responses.post('https://instance/api/v4/runners/verify', json={
            'id': 5,
            'token_expires_at': '2020-01-01',
        })

        responses.get('https://vault/v1/apps/data/cki/secret_token',
                      json={'data': {'data': {'value': 'token'}}})

        token = 'secret_token'
        meta = {
            'token_type': 'gitlab_runner_authentication_token',
            'instance_url': 'https://instance',
            'active': True,
        }
        expected = {
            'token_id': 5,
            'expires_at': '2020-01-01',
            'active': True,
        }
        with setup_secrets({token: {'meta': meta}}):
            manager.main(['update', '--token-name', token])
            self.assertEqual(secrets.secret(f'{token}#'), meta | expected)

    @responses.activate
    def test_validate(self) -> None:
        """Test behavior of validate."""
        cases = (
            ('runner authentication token', {}, {
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'token_type': 'gitlab_runner_authentication_token',
                'instance_url': 'https://instance',
                'active': True,
                'deployed': True,
            }, [{
                'method': 'POST',
                'url': 'https://instance/api/v4/runners/verify',
                'json': {'id': 5, 'token_expires_at': '2020-01-01'},
            }], True),
            ('invalid runner authentication token', {}, {
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'token_type': 'gitlab_runner_authentication_token',
                'instance_url': 'https://instance',
                'active': True,
                'deployed': True,
            }, [{
                'method': 'POST',
                'url': 'https://instance/api/v4/runners/verify',
                'status': HTTPStatus.UNAUTHORIZED,
            }], False),
        )
        for description, other_data, secrets_data, url_mocks, expected in cases:
            with (self.subTest(description),
                  setup_secrets({
                    'secret_token': {'meta': secrets_data},
                  } | (other_data or {
                    'secret_token/B': {'meta': {**secrets_data, 'deployed': False}},
                  })),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                for url_mock in url_mocks:
                    rsps.add(**{'method': 'GET', **url_mock})
                rsps.get('https://vault/v1/apps/data/cki/secret_token',
                         json={'data': {'data': {'value': 'token'}}})
                rsps.get('https://vault/v1/apps/data/cki/secret_token/B',
                         json={'data': {'data': {'value': 'token'}}})
                if not expected:
                    with self.assertRaises(Exception):
                        manager.main(['validate', '--token-name', 'secret_token'])
                else:
                    manager.main(['validate', '--token-name', 'secret_token'])
