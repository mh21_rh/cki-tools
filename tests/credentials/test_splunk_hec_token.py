"""Tests for credential management."""

import os
import unittest
from unittest import mock

import responses

from cki_tools.credentials import manager

from .utils import setup_secrets


@mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://vault', 'VAULT_TOKEN': 'vault_token'})
class TestToken(unittest.TestCase):
    """Tests for credential management."""

    @responses.activate
    def test_validate(self) -> None:
        """Test behavior of validate."""
        cases = (
            (400, {'text': 'No data', 'code': 5}, True),
            (400, {'text': 'Event field is required', 'code': 12, 'invalid-event-number': 0}, True),
            (400, {'text': 'Incorrect index', 'code': 7, 'invalid-event-number': 1}, False),
            (401, {'text': 'Invalid authorization', 'code': 3}, False),
            (403, {'text': 'Invalid token', 'code': 4}, False),
            (404, {'text': 'The requested URL was not found on this server.', 'code': 404}, False),
        )
        for status, contents, expected in cases:
            with (self.subTest(status), setup_secrets({'secret_token': {'meta': {
                'active': True,
                'created_at': '2000-01-01T00:00:00+00:00',
                'deployed': True,
                'endpoint_url': 'https://endpoint-url',
                'token_type': 'splunk_hec_token',
            }}}), responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                rsps.get('https://vault/v1/apps/data/cki/secret_token',
                         json={'data': {'data': {'value': 'secret'}}})
                rsps.post('https://endpoint-url/services/collector', status=status,
                          json=contents)

                if expected:
                    manager.main(['validate', '--token-name', 'secret_token'])
                else:
                    with self.assertRaises(Exception):
                        manager.main(['validate', '--token-name', 'secret_token'])
