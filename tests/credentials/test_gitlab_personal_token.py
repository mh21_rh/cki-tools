"""Tests for credential management."""

from http import HTTPStatus
import os
import unittest
from unittest import mock

from freezegun import freeze_time
import responses

from cki_tools.credentials import manager
from cki_tools.credentials import secrets

from .utils import setup_secrets


@mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://vault', 'VAULT_TOKEN': 'vault_token'})
class TestToken(unittest.TestCase):
    """Tests for credential management."""

    @responses.activate
    def test_destroy(self) -> None:
        """Test behavior of destroy."""

        responses.get('https://instance/api/v4/projects/g%2Fp', json={'id': 1})
        delete_url = responses.delete(
            'https://instance/api/v4/personal_access_tokens/self', json={})

        responses.get('https://vault/v1/apps/data/cki/secret_token',
                      json={'data': {'data': {'value': 'token'}}})

        token = 'secret_token'
        meta = {
            'active': True,
            'deployed': False,
            'instance_url': 'https://instance',
            'token_id': 3,
            'token_type': 'gitlab_personal_token',
        }
        with setup_secrets({token: {'meta': meta}}):
            manager.main(['destroy', '--token-name', token])
            self.assertEqual(secrets.secret(f'{token}#active'), False)
            self.assertEqual(len(delete_url.calls), 1)

    @freeze_time('2000-01-01T00:00:00.0+00:00')
    def test_update(self) -> None:
        """Test behavior of update."""
        cases = (
            ('personal token', 'secret_token', {
                'token_type': 'gitlab_personal_token',
                'instance_url': 'https://instance',
                'scopes': ['api'],
                'token_id': 3,
                'user_id': 2,
                'active': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/3',
                'json': {
                    'id': 3,
                    'user_id': 2,
                    'name': 'name',
                    'scopes': ['api'],
                    'created_at': '2000-01-01',
                    'expires_at': '2010-01-01',
                    'revoked': False,
                    'active': True,
                },
            }, {
                'url': 'https://instance/api/v4/users/2',
                'json': {'username': 'user'},
            }], {
                'scopes': ['api'],
                'token_name': 'name',
                'token_id': 3,
                'created_at': '2000-01-01',
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_id': 2,
                'user_name': 'user',
            }),
            ('personal token without token_id', 'secret_token', {
                'token_type': 'gitlab_personal_token',
                'instance_url': 'https://instance',
                'scopes': ['api'],
                'active': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'id': 3, 'user_id': 2, 'scopes': ['api']},
            }, {
                'url': 'https://instance/api/v4/personal_access_tokens/3',
                'json': {
                    'id': 3,
                    'user_id': 2,
                    'name': 'name',
                    'scopes': ['api'],
                    'created_at': '2000-01-01',
                    'expires_at': '2010-01-01',
                    'revoked': False,
                    'active': True,
                },
            }, {
                'url': 'https://instance/api/v4/users/2',
                'json': {'username': 'user'},
            }], {
                'scopes': ['api'],
                'token_name': 'name',
                'token_id': 3,
                'created_at': '2000-01-01',
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_id': 2,
                'user_name': 'user',
            }),
        )
        for description, token, meta, url_mocks, expected in cases:
            with (self.subTest(description), setup_secrets({token: {'meta': meta}}),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                for url_mock in url_mocks:
                    rsps.add(**{'method': 'GET', **url_mock})
                rsps.get('https://instance/api/v4/projects/g%2Fp', json={'id': 1})
                rsps.get('https://instance/api/v4/groups/g', json={'id': 4})
                rsps.get('https://vault/v1/apps/data/cki/secret_token',
                         json={'data': {'data': {'value': 'token'}}})
                if expected is None:
                    with self.assertRaises(Exception):
                        manager.main(['update', '--token-name', token])
                else:
                    manager.main(['update', '--token-name', token])
                    self.assertEqual(secrets.secret(f'{token}#'), meta | expected)

    def test_validate(self) -> None:
        """Test behavior of validate."""
        cases = (
            ('personal token', {}, {
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'token_type': 'gitlab_personal_token',
                'instance_url': 'https://instance',
                'active': True,
                'deployed': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': True},
            }], True),
            ('invalid personal token', {}, {
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'token_type': 'gitlab_personal_token',
                'instance_url': 'https://instance',
                'active': True,
                'deployed': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'status': HTTPStatus.UNAUTHORIZED,
            }], False),
            ('revoked personal token', {}, {
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'token_type': 'gitlab_personal_token',
                'instance_url': 'https://instance',
                'active': True,
                'deployed': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': True, 'active': True},
            }], False),
            ('inactive personal token', {}, {
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'token_type': 'gitlab_personal_token',
                'instance_url': 'https://instance',
                'active': True,
                'deployed': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': False},
            }], False),
        )
        for description, other_data, secrets_data, url_mocks, expected in cases:
            with (self.subTest(description),
                  setup_secrets({
                    'secret_token': {'meta': secrets_data},
                  } | (other_data or {
                    'secret_token/B': {'meta': {**secrets_data, 'deployed': False}},
                  })),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                for url_mock in url_mocks:
                    rsps.add(**{'method': 'GET', **url_mock})
                rsps.get('https://instance/api/v4/projects/g%2Fp', json={'id': 1})
                rsps.get('https://vault/v1/apps/data/cki/secret_token',
                         json={'data': {'data': {'value': 'token'}}})
                rsps.get('https://vault/v1/apps/data/cki/secret_token/B',
                         json={'data': {'data': {'value': 'token'}}})
                if not expected:
                    with self.assertRaises(Exception):
                        manager.main(['validate', '--token-name', 'secret_token'])
                else:
                    manager.main(['validate', '--token-name', 'secret_token'])

    @freeze_time('2000-01-03T00:00:00.0+00:00')
    @responses.activate
    def test_rotate(self) -> None:
        """Test behavior of rotate."""
        responses.get('https://instance/api/v4/projects/g%2Fp', json={'id': 1})
        responses.post('https://instance/api/v4/projects/1/access_tokens', json={
            'id': 3, 'user_id': 2, 'created_at': '2000-01-01', 'expires_at': '2010-01-01',
            'revoked': False, 'active': True, 'token': 'secret',
        })
        responses.get('https://instance/api/v4/users/2', json={'username': 'user'})
        responses.get('https://instance/api/v4/personal_access_tokens/3', json={
            'id': 3,
            'user_id': 2,
            'name': 'name',
            'scopes': ['api'],
            'created_at': '2000-01-01',
            'expires_at': '2010-01-01',
            'revoked': False,
            'active': True,
        })
        responses.get('https://instance/api/v4/personal_access_tokens/4', json={
            'id': 4,
            'user_id': 2,
            'name': 'name',
            'scopes': ['api'],
            'created_at': '2000-01-01',
            'expires_at': '2010-01-01',
            'revoked': False,
            'active': True,
        })

        responses.get('https://vault/v1/apps/data/cki/secret_token/1',
                      json={'data': {'data': {'value': 'secret'}}})
        responses.get('https://vault/v1/apps/data/cki/secret_token/946857600',
                      json={'data': {'data': {'value': 'secret'}}})

        secrets_put_url = responses.put('https://vault/v1/apps/data/cki/secret_token/946857600')
        rotate_url = responses.post('https://instance/api/v4/personal_access_tokens/3/rotate',
                                    json={'id': 4, 'token': 'new-secret'})

        meta = [{
            'active': True,
            'created_at': '1999-01-01T00:00:00.0+00:00',
            'deployed': False,
            'expires_at': '2000-01-15T00:00:00.0+00:00',
            'instance_url': 'https://instance',
            'revoked': False,
            'scopes': ['api'],
            'token_id': 3,
            'token_name': 'name',
            'token_type': 'gitlab_personal_token',
            'user_id': 2,
            'user_name': 'user',
        }, {
            'active': True,
            'created_at': '1999-01-02T00:00:00.0+00:00',
            'deployed': True,
            'expires_at': '2000-01-16T00:00:00.0+00:00',
            'instance_url': 'https://instance',
            'revoked': False,
            'scopes': ['api'],
            'token_id': 3,
            'token_name': 'name',
            'token_type': 'gitlab_personal_token',
            'user_id': 2,
            'user_name': 'user',
        }]
        with setup_secrets({f'secret_token/{i}': {'backend': 'hv', 'meta': d}
                            for i, d in enumerate(meta)}):
            manager.main(['prepare'])
            self.assertEqual(len(secrets_put_url.calls), 1)
            self.assertEqual(len(rotate_url.calls), 1)
            self.assertEqual(secrets.secret('secret_token/0#'),
                             meta[0] | {'revoked': True, 'active': False})
            self.assertEqual(secrets.secret('secret_token/1#'), meta[1])
            self.assertEqual(secrets.secret('secret_token/946857600#'), meta[0] | {
                'token_id': 4, 'created_at': '2000-01-01', 'expires_at': '2010-01-01'})
