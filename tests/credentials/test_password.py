"""Tests for credential management."""

import json
import os
import unittest
from unittest import mock

from freezegun import freeze_time
import responses

from cki_tools.credentials import manager
from cki_tools.credentials import secrets

from .utils import setup_secrets


@mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://vault', 'VAULT_TOKEN': 'vault_token'})
class TestToken(unittest.TestCase):
    """Tests for credential management."""
    @freeze_time('2000-01-01T00:00:00.0+00:00')
    @responses.activate
    def test_create(self) -> None:
        """Test behavior of create."""
        responses.get('https://vault/v1/apps/data/cki/secret_token',
                      json={'data': {'data': {}}})
        token_put = responses.put('https://vault/v1/apps/data/cki/secret_token')

        token = 'secret_token'
        meta = {
            'token_type': 'password',
        }
        expected = {
            'active': True,
            'deployed': True,
            'created_at': '2000-01-01T00:00:00+00:00',
        }

        with setup_secrets({token: {'backend': 'hv', 'meta': meta}}):
            manager.main(['create', '--token-name', token])
            self.assertEqual(secrets.secret(f'{token}#'), meta | expected)
            self.assertEqual(json.loads(token_put.calls[0].request.body), {
                'data': {'value': mock.ANY}
            })

    def test_destroy(self) -> None:
        """Test behavior of destroy."""

        token = 'secret_token'
        meta = {
            'deployed': False,
            'token_type': 'password',
        }
        with setup_secrets({token: {'meta': meta}}):
            manager.main(['destroy', '--token-name', token])
            self.assertEqual(secrets.secret(f'{token}#active'), False)
