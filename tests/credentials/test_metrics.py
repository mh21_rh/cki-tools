"""Tests for credential management."""

import unittest

from cki_tools.credentials import metrics

from .utils import setup_secrets


class TestCredentialMetrics(unittest.TestCase):
    """Tests for credential metrics."""

    def test_metrics(self) -> None:
        """Test behavior of metrics.process."""
        c = 'cki_token_created_at'
        e = 'cki_token_expires_at'
        cases = (
            ('default', [
                {'token_type': 'password', 'created_at': '2001-01-01'},
                {'token_type': 'password', 'expires_at': '2000-01-01'},
            ], {
                c + '{active="true",deployed="true",name="0",token_type="password"} 9.783072e+08',
                e + '{active="true",deployed="true",name="1",token_type="password"} 9.466848e+08',
            }),
            ('not deployed', [
                {'token_type': 'password', 'created_at': '2001-01-01', 'deployed': False},
                {'token_type': 'password', 'expires_at': '2000-01-01', 'deployed': False},
            ], {
                c + '{active="true",deployed="false",name="0",token_type="password"} 9.783072e+08',
                e + '{active="true",deployed="false",name="1",token_type="password"} 9.466848e+08',
            }),
            ('not active', [
                {'token_type': 'password', 'created_at': '2001-01-01', 'active': False},
                {'token_type': 'password', 'expires_at': '2000-01-01', 'active': False},
            ], {
                c + '{active="false",deployed="true",name="0",token_type="password"} 9.783072e+08',
                e + '{active="false",deployed="true",name="1",token_type="password"} 9.466848e+08',
            }),
            ('no field', [
                {'token_type': 'password'},
                {'token_type': 'password', 'created_at': '2001-01-01'},
                {'token_type': 'password', 'expires_at': '2000-01-01'},
            ], {
                c + '{active="true",deployed="true",name="1",token_type="password"} 9.783072e+08',
                e + '{active="true",deployed="true",name="2",token_type="password"} 9.466848e+08',
            }),
            ('wrong type', [
                {'token_type': 'wrong_type', 'created_at': '2011-01-01'},
                {'token_type': 'password', 'created_at': '2001-01-01'},
                {'token_type': 'wrong_type', 'expires_at': '2010-01-01'},
                {'token_type': 'password', 'expires_at': '2000-01-01'},
            ], {
                c + '{active="true",deployed="true",name="1",token_type="password"} 9.783072e+08',
                e + '{active="true",deployed="true",name="2",token_type="wrong_type"} 1.262304e+09',
                e + '{active="true",deployed="true",name="3",token_type="password"} 9.466848e+08',
            }),
        )
        for description, metas, expected in cases:
            with self.subTest(description), setup_secrets({
                str(i): {'meta': m} for i, m in enumerate(metas)
            }):
                self.assertEqual({
                    m for m in metrics.process().split('\n') if m.startswith('cki_')
                }, expected)
