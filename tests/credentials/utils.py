"""Utils for credential tests."""

import collections
import contextlib
import os
import tempfile
import typing
from unittest import mock

from cki_tools.credentials import secrets


@contextlib.contextmanager
def setup_secrets(
    variables: dict[str, typing.Any],
) -> collections.abc.Iterator[None]:
    """Context manager for setting up secrets."""
    with tempfile.TemporaryDirectory() as directory, mock.patch.dict(os.environ, {
        'CKI_SECRETS_FILE': f'{directory}/secrets.yml',
    }):
        secrets.write_secrets_file(variables)
        yield
