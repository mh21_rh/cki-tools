"""Test the S3 Bucket helpers."""
import unittest
from unittest import mock

import boto3
import botocore

from cki.cki_tools import _utils


class TestS3Client(unittest.TestCase):
    """Test the S3 client."""

    def test_init(self):
        """Test init sets all attributes."""
        spec = _utils.BucketSpec('https://endpoint', 'access_key',
                                 'secret_key', 'bucket', 'prefix')
        bucket = _utils.S3Bucket(spec)

        self.assertEqual(spec, bucket.spec)

    @mock.patch('boto3.setup_default_session')
    @mock.patch('boto3.DEFAULT_SESSION', mock.Mock())
    def test_init_has_session(self, setup_default_session):
        # pylint: disable=no-self-use
        """Test setup_default_session is not called if there is already a session."""
        spec = _utils.BucketSpec('https://endpoint', '', '', 'bucket', 'prefix')
        _utils.S3Bucket(spec)
        setup_default_session.assert_not_called()

    @staticmethod
    def _setup_default_session():
        boto3.DEFAULT_SESSION = mock.Mock()

    @mock.patch('boto3.setup_default_session')
    @mock.patch('boto3.DEFAULT_SESSION', None)
    def test_init_no_session(self, setup_default_session):
        # pylint: disable=no-self-use
        """Test setup_default_session is called if there is no session."""
        setup_default_session.side_effect = self._setup_default_session
        spec = _utils.BucketSpec('https://endpoint', '', '', 'bucket', 'prefix')
        _utils.S3Bucket(spec)
        setup_default_session.assert_called()

    def test_init_no_access_key(self):
        """Test unsigned requests if no access key is specified."""
        spec = _utils.BucketSpec('https://endpoint', '', '', 'bucket', 'prefix')
        bucket = _utils.S3Bucket(spec)
        self.assertEqual(bucket.kwargs['config'].signature_version, botocore.UNSIGNED)

    @mock.patch('boto3.Session.get_credentials',
                mock.Mock(return_value=mock.Mock(method='iam-role')))
    def test_init_iam(self):
        """Test signed requests if no access key is specified, but an iam role."""
        spec = _utils.BucketSpec('https://endpoint', '', '', 'bucket', 'prefix')
        bucket = _utils.S3Bucket(spec)
        self.assertIsNone(bucket.kwargs['config'].signature_version)

    @mock.patch('boto3.Session.get_credentials',
                mock.Mock(return_value=mock.Mock(method='something-else')))
    def test_init_no_iam(self):
        """Test unsigned requests if no access key is specified and some other creds."""
        spec = _utils.BucketSpec('https://endpoint', '', '', 'bucket', 'prefix')
        bucket = _utils.S3Bucket(spec)
        self.assertEqual(bucket.kwargs['config'].signature_version, botocore.UNSIGNED)

    def test_client(self):
        """Test client."""
        spec = _utils.BucketSpec('https://endpoint', 'access_key',
                                 'secret_key', 'bucket', 'prefix')
        bucket = _utils.S3Bucket(spec)

        self.assertIsInstance(bucket.client, botocore.client.BaseClient)

    def test_bucket(self):
        """Test bucket."""
        spec = _utils.BucketSpec('https://endpoint', 'access_key',
                                 'secret_key', 'bucket', 'prefix')
        bucket = _utils.S3Bucket(spec)
        with mock.patch('boto3.Session.resource') as resource:
            self.assertIsNotNone(bucket.bucket)
            resource.assert_called()

    def test_init_from_string(self):
        """Test from_bucket_string classmethod."""
        spec = _utils.BucketSpec('https://endpoint', 'access_key',
                                 'secret_key', 'bucket', 'prefix')
        bucket = _utils.S3Bucket.from_bucket_string(
            'https://endpoint|access_key|secret_key|bucket|prefix')

        self.assertEqual(spec.endpoint, bucket.spec.endpoint)


class TestParseBucketSpec(unittest.TestCase):
    def test_endpoint(self):
        """Test endpoint parameter."""
        test_cases = (
            ('https://endpoint||||', 'https://endpoint'),
            ('https://endpoint/||||', 'https://endpoint'),
            ('||||', 'http://s3.amazonaws.com'),
        )
        for case, expected in test_cases:
            self.assertEqual(_utils.parse_bucket_spec(case).endpoint, expected)

    def test_prefix(self):
        """Test prefix parameter."""
        test_cases = (
            ('||||path', 'path/'),
            ('||||path/', 'path/'),
            ('||||', ''),
        )
        for case, expected in test_cases:
            self.assertEqual(_utils.parse_bucket_spec(case).prefix, expected)

    def test_attributes(self):
        """Test all BucketSpec attributes."""
        spec = _utils.BucketSpec('https://endpoint', 'access_key',
                                 'secret_key', 'bucket', 'prefix/')
        bucket = _utils.parse_bucket_spec(
            'https://endpoint|access_key|secret_key|bucket|prefix')

        for attribute in ('endpoint', 'access_key', 'secret_key', 'bucket',
                          'prefix'):
            self.assertEqual(
                getattr(spec, attribute),
                getattr(bucket, attribute),
            )
