"""Tests for generate documentation from pipeline-data files."""
from importlib import resources
import pathlib
import tempfile
import unittest
from unittest import mock

from cki_tools import pipeline_data_gen_tree_doc
from tests.assets import pipeline_data_gen_tree_doc as assets


class TestGen_trees_doc(unittest.TestCase):
    """Tests to generate documentation for thee trees configured on specific file."""

    def test_gen_trees_doc(self):
        """Make sure it can generate the documentation from an specific file."""
        tree_file = resources.files(assets) / 'baseline.yaml'
        expected_output = (resources.files(assets) / 'baseline.md').read_text('utf8')
        with tempfile.TemporaryDirectory() as tempdir:
            self.assertTrue(
                pipeline_data_gen_tree_doc.gen_trees_doc(tree_file, f'{tempdir}/baseline.md')
            )
            output = pathlib.Path(f'{tempdir}/baseline.md').read_text()
        self.assertEqual(output, expected_output)

    @mock.patch('cki_tools.pipeline_data_gen_tree_doc.gen_trees_doc')
    def test_main(self, mock_gen_trees_doc):
        """Make sure it can generate the main documentation."""
        mock_gen_trees_doc.return_value = True
        basepath = 'tests/assets/pipeline_data_gen_tree_doc'
        pipeline_data_gen_tree_doc.main(['--pipeline-data', basepath])
        mock_gen_trees_doc.assert_called_once_with(f'{basepath}/baseline.yaml',
                                                   f'{basepath}/documentation/baseline.md')
