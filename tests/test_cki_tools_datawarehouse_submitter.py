"""Tests for cki_tools.datawarehouse_submitter."""
import contextlib
import copy
from http import HTTPStatus
from importlib import resources
import json
import os
import unittest
from unittest import mock

import requests
from requests import exceptions
import responses

from cki_tools import datawarehouse_submitter

from . import assets

DW_API = 'https://dw-url'

EMPTY_BODY = {
    "version": {"major": 4, "minor": 2},
    "checkouts": [],
    "builds": [],
    "tests": [],
}


CALLBACK_KWARGS = {
    "body": EMPTY_BODY,
    "routing_key": "routing_key",
    "headers": {'message-type': 'amqp-bridge'},
    "ack_fn": None,
    "timeout": False,
}


@mock.patch.dict('os.environ', {'DATAWAREHOUSE_URL': DW_API,
                                'DATAWAREHOUSE_TOKEN_SUBMITTER': 'token'})
class TestDataWarehouseSubmitter(unittest.TestCase):
    """Tests for cki_tools.datawarehouse_submitter."""

    @classmethod
    def _load_kcidb_file(cls):
        path_to_file = resources.files(assets) / "kcidb_all.json"
        with path_to_file.open(encoding="utf-8") as f:
            return json.load(f)

    def test_callback(self) -> None:
        """Test callback."""

        cases = [
            ('all-ok', 1, False),
            ('allow-list-no', 0, False),
            ('allow-list-yes', 1, False),
            ('no-file', 0, False),
            ('empty', 0, False),
            ('invalid-json', 0, False),
            ('invalid-schema', 0, False),
            ('devel-env', 0, False),
            ('bad-request', 1, True),
            ('internal-error', 1, True),
        ]

        for description, expected, raises in cases:
            with (self.subTest(description),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                data = {'json': self._load_kcidb_file()}
                dw_status = HTTPStatus.OK
                web_url = []
                is_production_or_staging = True
                ctx_check = self.assertRaises(Exception) if raises else contextlib.nullcontext()
                log_assertion = self.assertLogs if 'invalid' in description else self.assertNoLogs

                if 'allow-list-no' in description:
                    web_url = ['https://gl/p2/']
                if 'allow-list-yes' in description:
                    web_url = ['https://gl/p/']
                if 'no-file' in description:
                    data = {'status': 404}
                if 'empty' in description:
                    data = {}
                if 'invalid-json' in description:
                    data = {'body': json.dumps(data['json'])[:-1]}
                if 'invalid-schema' in description:
                    del data['json']['checkouts'][0]['origin']
                if 'devel-env' in description:
                    is_production_or_staging = False
                if 'bad-request' in description:
                    dw_status = HTTPStatus.BAD_REQUEST
                if 'internal-error' in description:
                    dw_status = HTTPStatus.INTERNAL_SERVER_ERROR

                rsps.get('https://gl/api/v4/projects/p/jobs/1', json={'id': 1, 'name': 'name'})
                rsps.get('https://gl/api/v4/projects/p/jobs/1/artifacts/kcidb_all.json', **data)
                submit_request = rsps.post('https://dw-url/api/1/kcidb/submit', status=dw_status)

                with (
                    mock.patch(
                        'cki_lib.misc.is_production_or_staging',
                        return_value=is_production_or_staging,
                    ),
                    ctx_check,
                    log_assertion(datawarehouse_submitter.LOGGER, "ERROR") as log_ctx,
                ):
                    datawarehouse_submitter.Submitter(web_url=web_url).callback(
                        body={'web_url': 'https://gl/p/-/jobs/1'},
                        headers={'message-type': 'herder'},
                    )

                if log_assertion is self.assertLogs:
                    self.assertEqual(
                        f"ERROR:{datawarehouse_submitter.LOGGER.name}:Discarding message!",
                        log_ctx.output[0].splitlines()[0],
                    )

                rsps.assert_call_count(submit_request.url, expected)

    @mock.patch('cki_lib.messagequeue.MessageQueue')
    @mock.patch.dict('os.environ', {'CKI_METRICS_ENABLED': 'false',
                                    'DATAWAREHOUSE_SUBMITTER_ROUTING_KEYS': 'key',
                                    'DATAWAREHOUSE_SUBMITTER_QUEUE': 'queue'})
    def test_main_queue(self, msgqueue) -> None:
        """Test queue processing via the main function."""
        datawarehouse_submitter.main([])
        msgqueue.return_value.consume_messages.assert_called_with(
            'cki.exchange.webhooks', ['key'], mock.ANY, queue_name='queue')

    def test_main_job(self) -> None:
        """Test job processing via the main function."""
        for environment in ('production', 'staging', 'development'):
            with (self.subTest(environment),
                  mock.patch.dict(os.environ, {'CKI_DEPLOYMENT_ENVIRONMENT': environment}),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                data = {'json': self._load_kcidb_file()}

                rsps.get('https://gl/api/v4/projects/p/jobs/1', json={'id': 1, 'name': 'name'})
                rsps.get('https://gl/api/v4/projects/p/jobs/1/artifacts/kcidb_all.json', **data)
                submit_request = rsps.post('https://dw-url/api/1/kcidb/submit')

                datawarehouse_submitter.main(['--job-url', 'https://gl/p/-/jobs/1'])
                rsps.assert_call_count(submit_request.url, 1)

    def test_main_kcidb_file(self) -> None:
        """Test KCIDB file processing via the main function."""
        for environment in ('production', 'staging', 'development'):
            with (self.subTest(environment),
                  mock.patch.dict(os.environ, {'CKI_DEPLOYMENT_ENVIRONMENT': environment}),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps,
                    resources.as_file(resources.files(assets) / "kcidb_all.json") as data_path,
                  ):
                submit_request = rsps.post('https://dw-url/api/1/kcidb/submit')

                datawarehouse_submitter.main(['--kcidb-file', str(data_path)])
                rsps.assert_call_count(submit_request.url, 1)

    def test_batched(self):
        """Test datawarehouse_submitter.batched()."""
        with (
            self.subTest("Bad batch size: 0"),
            self.assertRaisesRegex(ValueError, r"n must be at least one"),
        ):
            next(datawarehouse_submitter.batched([1, 2, 3, 4], 0))

        batch_size = 3
        cases = {
            "Empty data": ([], []),
            "Data smaller than batch size": ([1, 2], [(1, 2)]),
            "Data same size than batch size": ([1, 2, 3], [(1, 2, 3)]),
            "Data bigger than batch size": ([1, 2, 3, 4, 5, 6, 7], [(1, 2, 3), (4, 5, 6), (7,)]),
        }
        for description, (data, expected) in cases.items():
            with self.subTest(description):
                result = list(datawarehouse_submitter.batched(data, batch_size))
                self.assertEqual(result, expected)

    @responses.activate
    @mock.patch('cki_lib.misc.is_production_or_staging', mock.Mock(return_value=True))
    def test_callback_empty_lists(self):
        """Ensure callback does not submit payloads consisting of empty lists."""
        submitter = datawarehouse_submitter.Submitter()
        callback_kwargs = copy.deepcopy(CALLBACK_KWARGS)
        create_request = responses.post(DW_API + "/api/1/kcidb/submit")
        with self.subTest("All lists are empty -> No requests"):
            callback_kwargs["body"] = copy.deepcopy(EMPTY_BODY)
            submitter.callback(**callback_kwargs)
            self.assertEqual(create_request.call_count, 0, "Should not submit empty lists")

    @mock.patch('cki_lib.misc.is_production_or_staging', mock.Mock(return_value=True))
    def test_callback_empty(self):
        """Test callback when passed empty KCIDB data."""
        submitter = datawarehouse_submitter.Submitter()
        with (
            mock.patch.object(submitter.dw_api.kcidb.submit, "create") as mock_create,
            self.assertLogs(datawarehouse_submitter.LOGGER, level="INFO") as log_ctx,
        ):
            submitter.callback(body={}, headers={'message-type': 'amqp-bridge'})
            self.assertFalse(mock_create.called)
        self.assertIn('empty', log_ctx.output[-1])

    @mock.patch('cki_lib.misc.is_production_or_staging', mock.Mock(return_value=True))
    def test_callback_invalid(self):
        """Test callback when passed invalid KCIDB data."""
        submitter = datawarehouse_submitter.Submitter()
        with (
            mock.patch.object(submitter.dw_api.kcidb.submit, "create") as mock_create,
            self.assertLogs(datawarehouse_submitter.LOGGER, level="ERROR") as log_ctx,
        ):
            submitter.callback(body={'pooh': 'bear'}, headers={'message-type': 'amqp-bridge'})
            self.assertFalse(mock_create.called)
        self.assertIn('validate', log_ctx.output[-1])

    @responses.activate
    @mock.patch('cki_lib.misc.is_production_or_staging', mock.Mock(return_value=True))
    def test_callback_valid(self):
        """Test callback when passed valid KCIDB data."""
        submitter = datawarehouse_submitter.Submitter()
        callback_kwargs = copy.deepcopy(CALLBACK_KWARGS)
        body = self._load_kcidb_file()
        callback_kwargs["body"] = body

        with self.subTest("Validate successful calls to submit work as expected"):
            create_request = responses.post(DW_API + '/api/1/kcidb/submit')
            submitter.callback(**callback_kwargs)

            responses.assert_call_count(create_request.url, 1)
            request: requests.PreparedRequest = responses.calls[0].request
            self.assertEqual(
                request.body,
                json.dumps({"data": body}),
                f"Expected {create_request.url!r} to be called with {EMPTY_BODY!r}",
            )

        with self.subTest("Validate BAD REQUEST error raises"):
            responses.calls.reset()
            create_request = responses.post(DW_API + '/api/1/kcidb/submit',
                                            status=HTTPStatus.BAD_REQUEST)

            with self.assertRaises(Exception):
                submitter.callback(**callback_kwargs)

            responses.assert_call_count(create_request.url, 1)
            request: requests.PreparedRequest = responses.calls[0].request
            self.assertEqual(
                request.body,
                json.dumps({"data": body}),
                f"Expected {create_request.url!r} to be called with {body!r}",
            )

        with self.subTest("Validate any other HTTP error raises"):
            responses.calls.reset()
            create_request = responses.post(DW_API + '/api/1/kcidb/submit',
                                            status=HTTPStatus.BAD_GATEWAY)

            with self.assertRaises(exceptions.HTTPError):
                submitter.callback(**callback_kwargs)

            responses.assert_call_count(create_request.url, 1)
            request: requests.PreparedRequest = responses.calls[0].request
            self.assertEqual(
                request.body,
                json.dumps({"data": body}),
                f"Expected {create_request.url!r} to be called with {body!r}",
            )

    @responses.activate
    @mock.patch('cki_lib.misc.is_production_or_staging', mock.Mock(return_value=True))
    def test_callback_huge_message(self):
        """Ensure callback will split huge messages into reasonable payloads."""
        create_request = responses.post(DW_API + "/api/1/kcidb/submit")

        # Prepare body with tests able to extrapolate the overridden batch_size
        callback_kwargs = copy.deepcopy(CALLBACK_KWARGS)
        full_body = self._load_kcidb_file()
        callback_kwargs["body"] = full_body

        submitter = datawarehouse_submitter.Submitter(batch_size=4)
        submitter.callback(**callback_kwargs)

        self.assertEqual(
            create_request.call_count,
            4,
            (
                "There should be at least one call for object type,"
                " plus one for each list extrapolating the batch size"
            ),
        )

        combined_submitted_body = copy.deepcopy(EMPTY_BODY)
        for call in responses.calls:
            body = json.loads(call.request.body)
            self.assertEqual(combined_submitted_body["version"], body["data"]["version"])
            for key in ["checkouts", "builds", "tests"]:
                combined_submitted_body[key].extend(body["data"].get(key, []))

        for key in ["checkouts", "builds", "tests"]:
            self.assertCountEqual(
                combined_submitted_body[key],
                full_body[key],
                f"Expected {create_request.url!r} to be called with {full_body[key]!r}",
            )

        create_request.calls.reset()

        partial_body = {"version": full_body["version"], "tests": full_body["tests"]}
        partial_body_callback_kwargs = copy.deepcopy(CALLBACK_KWARGS)
        partial_body_callback_kwargs["body"] = partial_body
        with self.subTest("Submits partial bodies, skipping empty lists"):
            submitter.callback(**partial_body_callback_kwargs)

            self.assertEqual(create_request.call_count, 2, "Should submit twice with tests only")

            combined_submitted_body = copy.deepcopy(EMPTY_BODY)
            for call in responses.calls:
                body = json.loads(call.request.body)
                self.assertEqual(combined_submitted_body["version"], body["data"]["version"])
                for key in ["checkouts", "builds", "tests"]:
                    combined_submitted_body[key].extend(body["data"].get(key, []))

            self.assertCountEqual(
                combined_submitted_body,
                partial_body,
                f"Expected {create_request.url!r} to be called with {partial_body!r}",
            )
