---
title: Onboarded baseline trees
---

## [mainline.kernel.org](https://datawarehouse.cki-project.org/kcidb/checkouts?filter_gittrees=mainline.kernel.org)

Run tests on mainline.kernel.org kernel tree.

- repo: <https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git>
- branches: master
- responsible: <contact1@email.com>
- onboarding request: <https://url.com/request/1>
- notifications: test maintainer and reponsible contact (see above)
- test set: `kt1|kself`
- architectures: `x86_64 ppc64le aarch64 s390x`

## [mainline.kernel.org-clang](https://datawarehouse.cki-project.org/kcidb/checkouts?filter_gittrees=mainline.kernel.org-clang)

Run tests on mainline.kernel.org-clang kernel tree.

- repo: <https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git>
- branches: master
- responsible: <contact1@email.com>, <contact2@email.com>
- onboarding request: <N/A>
- notifications: test maintainer and reponsible contact (see above)
- test set: `kt1|kself`
- architectures: `x86_64 ppc64le aarch64 s390x`

## [net-next](https://datawarehouse.cki-project.org/kcidb/checkouts?filter_gittrees=net-next)

test net-next tree

- repo: <https://git.kernel.org/pub/scm/linux/kernel/git/davem/net-next.git>
- branches: master
- onboarding request: <N/A>
- notifications: test maintainer
- test set: `net`
- tests regex: `Network.*`
- architectures: `x86_64 ppc64le aarch64 s390x`
