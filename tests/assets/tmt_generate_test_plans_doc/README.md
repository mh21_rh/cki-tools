<!-- markdownlint-disable-next-line MD041 -->
## kernel-tier1

Upstream kernel tier1 tests

- enabled: `true`
- provision:
  - how: beaker
  - image: `RHEL-9%`
  - hardware:
`{
    "hostname": "machine1.test.com"
}`
- discover:
  - url: <https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests>
  - filter: `tag: KernelTier1`
  - exclude:
    - /security/audit/2028871

More test plans can be found at:

- [security](documentation/security.md)
