<!-- markdownlint-disable-next-line MD041 -->
## security/audit

Audit security test suite

- enabled: `true`
- discover:
  - url: <https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests.git>
  - test:
    - /security/audit/2028871
    - /security/audit/2035123
    - /security/audit/testsuite
