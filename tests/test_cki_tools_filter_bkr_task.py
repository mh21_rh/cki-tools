"""Test for cki_bkr_filter """
from importlib.resources import files
import os
import tempfile
import unittest
from unittest.mock import patch

from cki.cki_tools import filter_bkr_task

test_files = files(__package__)

valid_xml_file = f"{test_files}/test_cki_tools_filter_bkr_task.xml"
invalid_xml_file = "invalid_file.xml"


class Test(unittest.TestCase):
    bkrClass = filter_bkr_task.BeakerXML()

    def test_filter_task(self):
        # Make sure we can filter per CKI test id
        self.assertEqual(self.bkrClass.load_xml(valid_xml_file), None)
        self.assertEqual(self.bkrClass.filter_task(test_id=6), True)
        recipeSets = self.bkrClass.root.findall('recipeSet')
        self.assertEqual(len(recipeSets), 1)
        recipes = recipeSets[0].findall('recipe')
        self.assertEqual(len(recipes), 1)
        self.assertEqual(len(recipes[0].findall('task')), 7)

        # Make sure it can select recipes from multihost recipeSet
        self.assertEqual(self.bkrClass.load_xml(valid_xml_file), None)
        self.assertEqual(self.bkrClass.filter_task(test_id=82), True)
        recipeSets = self.bkrClass.root.findall('recipeSet')
        self.assertEqual(len(recipeSets), 1)
        # it should hvae 2 recipes in the recipeSet
        recipes = recipeSets[0].findall('recipe')
        self.assertEqual(len(recipes), 2)
        self.assertEqual(len(recipes[0].findall('task')), 6)
        self.assertEqual(len(recipes[1].findall('task')), 6)

    def test_cli(self):
        # basic task test id
        with tempfile.TemporaryDirectory() as tempdir:
            output_xml_file = f"{tempdir}/beaker.xml"

            parameter = f"""--beaker-xml-path {valid_xml_file} --output-xml {output_xml_file}
                         --test-id 6""".split()
            self.assertEqual(filter_bkr_task.main(parameter), 0)
            self.assertEqual(os.path.exists(output_xml_file), True)

        # try to save file to a directory that doesn't exist
        self.assertEqual(filter_bkr_task.main(parameter), 0)
        self.assertEqual(os.path.exists(output_xml_file), True)

        # invalid test id
        parameter = f"""--beaker-xml-path {valid_xml_file} --output-xml {output_xml_file}
                     --test-id 9999""".split()
        self.assertEqual(filter_bkr_task.main(parameter), 1)

    @patch('cki.cki_tools.filter_bkr_task.BeakerXML.load_xml')
    def test_cli_filter_task_fails(self, load_xml_mock):
        load_xml_mock.return_value = True
        with tempfile.TemporaryDirectory() as tempdir:
            output_xml_file = f"{tempdir}/beaker.xml"

            parameter = f"""--beaker-xml-path {valid_xml_file} --output-xml {output_xml_file}
                         --test-id 6""".split()
            self.assertEqual(filter_bkr_task.main(parameter), 1)
