"""Utility functions for testcases."""
import base64
import gzip

from prometheus_client.registry import REGISTRY


def tear_down_registry():
    # pylint: disable=protected-access
    """Unregister collectors in prometheus registry."""
    collectors = list(REGISTRY._collector_to_names.keys())
    for collector in collectors:
        REGISTRY.unregister(collector)


def b64decode_and_decompress(data: str) -> str:
    """Decode and uncompress base64-encoded and gzip-compressed data."""
    return gzip.decompress(base64.b64decode(data)).decode('utf-8')


def mock_attrs(extra_args=None, **kwargs):
    """Return a dict with the basic attrs of a DW-API lib object, overwritten by the given args."""
    if extra_args is None:
        extra_args = kwargs
    attrs = {"id": "redhat:1", "origin": "redhat", "misc": {"iid": 1}}
    attrs.update(**extra_args)
    return attrs
